/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.util;

import az.neuron.dev.rs.caremed.domain.DictionaryWrapper;
import az.neuron.dev.rs.caremed.domain.FileWrapper;
import az.neuron.dev.rs.caremed.domain.User;
import az.neuron.dev.rs.caremed.domain.UserAccount;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Bayram
 */
public class RowFetcher {
  
@Autowired
    
    public static final User fetchUser(ResultSet resultSet) throws SQLException {
        return new User(0, 
                        new UserAccount(resultSet.getInt("id"),
                                        resultSet.getString("user_name"), 
                                        "",
                                        new DictionaryWrapper(resultSet.getInt("role_id")),
                                        resultSet.getInt("is_blocked") == 1), 
                        resultSet.getString("fname"), 
                        resultSet.getString("lname"), 
                        resultSet.getString("mname"), 
                        resultSet.getString("birthdate"), 
                        new DictionaryWrapper(resultSet.getInt("gender_id")), 
                        resultSet.getString("pincode"), 
                        new FileWrapper(resultSet.getInt("file_id"), null, null),
                        resultSet.getInt("active_sessions") > 0);
    }  
}
