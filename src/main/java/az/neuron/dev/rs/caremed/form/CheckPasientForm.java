/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

import az.neuron.dev.rs.caremed.domain.FileWrapper;
import java.util.List;

/**
 *
 * @author Bayram
 */
public class CheckPasientForm {
    private Integer patientServiceOperationId;
    private CheckUpForm[] checkUp;

    public CheckPasientForm() {
    }

    public Integer getPatientServiceOperationId() {
        return patientServiceOperationId;
    }

    public void setPatientServiceOperationId(Integer patientServiceOperationId) {
        this.patientServiceOperationId = patientServiceOperationId;
    }

    public CheckUpForm[] getCheckUp() {
        return checkUp;
    }

    public void setCheckUp(CheckUpForm[] checkUp) {
        this.checkUp = checkUp;
    }

    @Override
    public String toString() {
        return "CheckPasientForm{" + "patientServiceOperationId=" + patientServiceOperationId + ", checkUp=" + checkUp + '}';
    }     
    
}
