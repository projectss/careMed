/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.domain.AbroadInfo;
import az.neuron.dev.rs.caremed.domain.AcademicInfo;
import az.neuron.dev.rs.caremed.domain.AwardInfo;
import az.neuron.dev.rs.caremed.domain.CaseRecord;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.Education;
import az.neuron.dev.rs.caremed.domain.ElectronalOrganization;
import az.neuron.dev.rs.caremed.domain.Employee;
import az.neuron.dev.rs.caremed.domain.EmployeeFullList;
import az.neuron.dev.rs.caremed.domain.EmployeeList;
import az.neuron.dev.rs.caremed.domain.Employment;
import az.neuron.dev.rs.caremed.domain.FileWrapper;
import az.neuron.dev.rs.caremed.domain.MilitaryInfo;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.Person;
import az.neuron.dev.rs.caremed.domain.PersonAddress;
import az.neuron.dev.rs.caremed.domain.PersonContact;
import az.neuron.dev.rs.caremed.domain.PersonDocument;
import az.neuron.dev.rs.caremed.domain.PersonLanguage;
import az.neuron.dev.rs.caremed.domain.PersonRelationship;
import az.neuron.dev.rs.caremed.domain.PersonTest;
import az.neuron.dev.rs.caremed.domain.ResearchInfo;
import az.neuron.dev.rs.caremed.domain.Vacation;
import az.neuron.dev.rs.caremed.form.AbroadForm;
import az.neuron.dev.rs.caremed.form.AcademicInfoForm;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.AwardForm;
import az.neuron.dev.rs.caremed.form.CaseRecordForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.DocumentForm;
import az.neuron.dev.rs.caremed.form.EducationForm;
import az.neuron.dev.rs.caremed.form.ElectronalOrgForm;
import az.neuron.dev.rs.caremed.form.EmployeeForm;
import az.neuron.dev.rs.caremed.form.EmployeeHistoryForm;
import az.neuron.dev.rs.caremed.form.EmployementForm;
import az.neuron.dev.rs.caremed.form.MembershipForm;
import az.neuron.dev.rs.caremed.form.MilitaryInfoForm;
import az.neuron.dev.rs.caremed.form.PFile;
import az.neuron.dev.rs.caremed.form.PersonForm;
import az.neuron.dev.rs.caremed.form.PersonLanguageForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import az.neuron.dev.rs.caremed.form.ResearchForm;
import az.neuron.dev.rs.caremed.form.TextUC;
import az.neuron.dev.rs.caremed.form.VacationForm;
import az.neuron.dev.rs.caremed.search.EmployeeSearchForm;
import java.util.List;

/**
 *
 * @author Bayram
 */
public interface IEmployeeDao {

    // get
    public DataTable getEmployeeList(EmployeeSearchForm form);  // orta panel 5 melumat ucun

    public Employee getEmployeeDetails(String token, int personId); // person id-ye gore employeenin melumatlari 

    public Employee getEmployeePersonalInfo(String token, int personId); // sol panel employee ifnomaciyasi

    public List<PersonLanguage> getPersonLanguage(String token, int personId); // employee  dili qabuliyetleri 

    public List<PersonContact> getPersonContact(String token, int personId); // employee kontakti

    public List<PersonAddress> getPersonAddress(String token, int personId); // employee adressi

    public List<AcademicInfo> getPersonAcademicInfo(String token, int personId); // employeenin elmi isleri 

    public List<Education> getPersonEducation(String token, int personId); // employeenin tehsili

    public List<ElectronalOrganization> getPersonElectronalOrganization(String token, int personId);  // employenin secki mentegesinde istiraki 

    public List<Employment> getEmploymentList(String token, int personId);  // employement kohne isi tarixcesi

    public List<MilitaryInfo> getMillitaryList(String token, int personId); // eskerlik dovruyesi

    public List<ResearchInfo> getResearchList(String token, int personId); // kesf etmesi, inkisafi

    public List<AbroadInfo> getAbroadInfo(String token, int personId); // xarice ezamiyeti

    public List<AwardInfo> getAwardInfo(String token, int personId); // mukafatlari

    public List<PersonDocument> getPersonDocument(String token, int personId); // dokumentleri

    public List<FileWrapper> getPersonFile(String token, int documentId); // fillari

    public List<PersonRelationship> getPersonRealtionship(String token, int personId); // gohumluq elaqesi

    public FileWrapper getEmployeeImage(int id); // sekili

    public OperationResponse checkPincode(String pincode); // pinkodu yoxlama

    public List<Vacation> getVacationList(String token, int perosonId); // mezuniyet

    public List<CaseRecord> getCaseList(String token, int personId); // xestelik vereqisi

    // edit ---------------------
    public OperationResponse editPersons(String token, PersonForm form, String imageFullpath, String imageOriginalName);

    public OperationResponse editPersonLanguage(String token, int id, PersonLanguageForm language);

    public OperationResponse editPesonContact(String token, int id, ContactForm form);

    public OperationResponse editPersonAddress(String token, AddressForm form);

    public OperationResponse editPersonRelationship(String token, int id, PersonRelationForm form);

    public OperationResponse editPersonDocument(String token, int id, DocumentForm form);

    public OperationResponse editAbroadInfo(String token, int id, AbroadForm form);

    public OperationResponse editAcademicInfo(String token, int id, AcademicInfoForm form);

    public OperationResponse editAward(String token, int id, AwardForm form);

    public OperationResponse editCaseRecord(String token, int id, CaseRecordForm form);

    public OperationResponse editEducation(String token, int id, EducationForm form);

    public OperationResponse editElectronalOrganization(String token, ElectronalOrgForm form);

    public OperationResponse editEmployement(String token, int personId, EmployementForm form);

    public OperationResponse editDocFile(String token, DocumentForm form); // -- tam bitirmek 

    public OperationResponse editMembership(String token, MembershipForm form);

    public OperationResponse editMillitaryInfo(String token, int id, MilitaryInfoForm form);

    public OperationResponse editResearchInfo(String token, int id, ResearchForm form);

    public OperationResponse editVacationRecord(String token, int id, VacationForm form);

    public OperationResponse editEmployee(EmployeeForm form);

    /// ----------------------------------------------------------------------------------
    /// add ------------------------------------------------------------------------------
    public OperationResponse addEmployeeFile(String token, int id, int docId, PFile file);

    public OperationResponse addEmployee(EmployeeForm form, String imageFullpath, String imageOriginalName);

    public OperationResponse addPersonContact(String token, int personId, ContactForm form);

    public OperationResponse addDocument(DocumentForm form, String token, int personId);

    public OperationResponse addDocFile(DocumentForm form, String token);

    public OperationResponse addAward(String token, int personId, AwardForm form);

    public OperationResponse addAbroadInfo(String token, int personId, AbroadForm form);

    public OperationResponse addAcademicInfo(String token, int personId, AcademicInfoForm form);

    public OperationResponse addCaseRecord(String token, int personId, CaseRecordForm form);

    public OperationResponse addEducation(String token, int personId, EducationForm form);

    public OperationResponse addElectronalOrgaznization(String token, int personId, ElectronalOrgForm form);

    public OperationResponse addEmployement(String token, int personId, EmployementForm form);

    public OperationResponse addMemberShip(String token, int personId, MembershipForm form);

    public OperationResponse addMillitaryInfo(String token, MilitaryInfoForm form);

    public OperationResponse addPersonAddress(String token, int personId, AddressForm form);

    public OperationResponse addPersonLanguage(String token, int personId, PersonLanguageForm form);

    public OperationResponse addPersonRelationShip(String token, int personId, PersonRelationForm form);

    public OperationResponse addPersonResearchInfo(String token, int personId, ResearchForm form);

    public OperationResponse addVacationRecord(String token, int personId, VacationForm form);

    public OperationResponse addTest(test t);

    //---------------------------------
    // remove ---------------------------
    public OperationResponse removePersonContact(String token, int id, int contactId);

    public OperationResponse removeAbroadInfo(String token, int id, int abroadId);

    public OperationResponse removeAcademicInfo(String token, int id, int academicId);

    public OperationResponse removeAward(String token, int id, int awardId);

    public OperationResponse removeCaseRecord(String token, int id, int caseId);

    public OperationResponse removeEducation(String token, int id, int educationId);

    public OperationResponse removeElectronalOrganization(String token, int id, int electoralId);

    public OperationResponse removeEmployment(String token, int id, int employmentId);

    public OperationResponse removeMembership(String token, int id, int memebershipId);

    public OperationResponse removeMillitaryInfo(String token, int id, int militaryId);

    public OperationResponse removePersonAddress(String token, int id, int addressId);

    public OperationResponse removePersonDocument(String token, int id, int documentId);

    public OperationResponse removePersonLanguage(String token, int id, int languageId);

    public OperationResponse removePersonRelationship(String token, int id, int relationshipId);

    public OperationResponse removeResearchInfo(String token, int id, int researchId);

    public OperationResponse removeVacationRecord(String token, int id, int vacationId);

    public List<FileWrapper> getDocumentFiles(int docId);

    public FileWrapper getFileByFileId(int fileId);

    public OperationResponse removeDocFile(String token, int docId, int fileId);

    public OperationResponse removeEmployee(int personId, EmployeeHistoryForm form);

}
