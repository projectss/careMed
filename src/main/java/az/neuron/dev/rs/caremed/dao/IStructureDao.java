/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.domain.User;

/**
 *
 * @author Bayram
 */
public interface IStructureDao {
    public User checkToken(String token);
    
}
