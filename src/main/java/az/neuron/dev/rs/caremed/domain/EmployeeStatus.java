/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class EmployeeStatus  extends Person{
    private DictionaryWrapper positionId;
    private DictionaryWrapper status;

    public EmployeeStatus() {
    }

    public EmployeeStatus(DictionaryWrapper positionId, DictionaryWrapper status, int id, String firstname, String lastname, String middlename, DictionaryWrapper genderId) {
        super(id, firstname, lastname, middlename, genderId);
        this.positionId = positionId;
        this.status = status;
    }

    public DictionaryWrapper getPositionId() {
        return positionId;
    }

    public void setPositionId(DictionaryWrapper positionId) {
        this.positionId = positionId;
    }

    public DictionaryWrapper getStatus() {
        return status;
    }

    public void setStatus(DictionaryWrapper status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EmployeeStatus{" + "positionId=" + positionId + ", status=" + status + '}';
    }

   
    
    
}
