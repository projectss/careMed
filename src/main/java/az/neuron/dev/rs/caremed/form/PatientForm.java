/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class PatientForm extends PersonForm{

    
    private String patientDocNum;
    private String registrationDate;
    private String note;
    private int typeId;
    private int companyNameId;
    private int positionId;
    private int orgId;

    public PatientForm() {
    }

    public String getPatientDocNum() {
        return patientDocNum;
    }

    public void setPatientDocNum(String patientDocNum) {
        this.patientDocNum = patientDocNum;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getCompanyNameId() {
        return companyNameId;
    }

    public void setCompanyNameId(int companyNameId) {
        this.companyNameId = companyNameId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    @Override
    public String toString() {
        return "PatientForm{" + "patientDocNum=" + patientDocNum + ", registrationDate=" + registrationDate + ", note=" + note + ", typeId=" + typeId + ", companyNameId=" + companyNameId + ", positionId=" + positionId + ", orgId=" + orgId + '}';
    }
}
