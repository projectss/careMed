/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Admin
 */
public class OperationForm extends BaseForm{
    private ServiceOperationForm [] serviceOperationForm;

    public OperationForm() {
    }

    
    public ServiceOperationForm[] getServiceOperationForm() {
        return serviceOperationForm;
    }

    public void setServiceOperationForm(ServiceOperationForm[] serviceOperationForm) {
        this.serviceOperationForm = serviceOperationForm;
    }

    @Override
    public String toString() {
        return "OperationForm{" + "serviceOperationForm=" + serviceOperationForm + '}';
    }
    
}
