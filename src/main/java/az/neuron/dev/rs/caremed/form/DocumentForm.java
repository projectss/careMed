/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class DocumentForm {
    private Integer id;
    private Integer typeId;
    private String serial;
    private String number;
    private String startDate;
    private String endDate;
    private String note;
    private FileWrapperForm[] uploads;

    public DocumentForm() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public FileWrapperForm[] getUploads() {
        return uploads;
    }

    public void setUploads(FileWrapperForm[] uploads) {
        this.uploads = uploads;
    }

    @Override
    public String toString() {
        return "DocumentForm{" + "id=" + id + ", typeId=" + typeId + ", serial=" + serial + ", number=" + number + ", startDate=" + startDate + ", endDate=" + endDate + ", note=" + note + ", uploads=" + uploads + '}';
    }
}