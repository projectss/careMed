/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class EducationForm {
private int id;
private int eduLevelId;
private String institutionName;
private String institutionAddress;
private int institutionType;
private String facultyName;
private String startDate;
private String endDate;
private String diplomNumber;
private String note;

EducationForm(){
}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEduLevelId() {
        return eduLevelId;
    }

    public void setEduLevelId(int eduLevelId) {
        this.eduLevelId = eduLevelId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getInstitutionAddress() {
        return institutionAddress;
    }

    public void setInstitutionAddress(String institutionAddress) {
        this.institutionAddress = institutionAddress;
    }

    public int getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(int institutionType) {
        this.institutionType = institutionType;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDiplomNumber() {
        return diplomNumber;
    }

    public void setDiplomNumber(String diplomNumber) {
        this.diplomNumber = diplomNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "EducationForm{" + "id=" + id + ", eduLevelId=" + eduLevelId + ", institutionName=" + institutionName + ", institutionAddress=" + institutionAddress + ", institutionType=" + institutionType + ", facultyName=" + facultyName + ", startDate=" + startDate + ", endDate=" + endDate + ", diplomNumber=" + diplomNumber + ", note=" + note + '}';
    }



}
