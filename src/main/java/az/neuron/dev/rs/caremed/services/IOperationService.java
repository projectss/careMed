/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.services;

import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.EmployeeStatus;
import az.neuron.dev.rs.caremed.domain.FormElement;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.PatientHistory;
import az.neuron.dev.rs.caremed.domain.Services;
import az.neuron.dev.rs.caremed.form.ConsuptionForm;
import az.neuron.dev.rs.caremed.form.EmpStatusForm;
import az.neuron.dev.rs.caremed.form.EmployeeStatusForm;
import az.neuron.dev.rs.caremed.form.MedicalCommitionForm;
import az.neuron.dev.rs.caremed.form.OperationForm;
import az.neuron.dev.rs.caremed.form.ServiceOperationForm;
import az.neuron.dev.rs.caremed.form.ServicesForm;
import az.neuron.dev.rs.caremed.search.ConsuptionSearchForm;
import az.neuron.dev.rs.caremed.search.PatientCommitionHistory;
import az.neuron.dev.rs.caremed.search.PatientSearchForm;
import az.neuron.dev.rs.caremed.search.ServiceSearchForm;
import java.util.List;

/**
 *
 * @author Bayram
 */
public interface IOperationService {

    public List<EmployeeStatus> getEmployeeStatus(String token, EmployeeStatusForm form);

    public OperationResponse addPatientServices(String token, ServicesForm form);

    public OperationResponse changeEmployeeStatus(String token, EmpStatusForm empStatusForm);

    public OperationResponse addServices(String token, ServicesForm form);

    public OperationResponse editServices(String token, ServicesForm form);

    public DataTable getServices(ServiceSearchForm searchForm);

    public OperationResponse removeServices(String token, int id);

    public Services getServicesDetails(String token, int id);

    public List<Services> getSetvicesList(String token, int servicesId);

    public OperationResponse addPatientServiceOperation(String token, OperationForm operationForm);

    public DataTable getPatientForCommition(String token, PatientSearchForm form);

    public OperationResponse addPatientMedicalCommition(String token, int patientId, OperationForm operationForm);

    public DataTable getCommitionHistory(String token, int patientId, PatientCommitionHistory patientCommitionHistory);

    public List<FormElement> getFormElementList(String token, int servicesId);

    public List<PatientHistory> getServicesForMedicalCommition(String token, int commitionId);

    public OperationResponse addConsuption(String token, ConsuptionForm form);

    public DataTable getConsuption(String token, ConsuptionSearchForm consuptionSearchForm);

    public OperationResponse editConsuption(String token, ConsuptionForm form);

    public OperationResponse removeConsuption(String token, int id, String note);

    public OperationResponse checkCodeConsuption(String token, String code);
}
