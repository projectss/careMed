/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class Consuption {
    private int id;
    private String code;
    private String name;
    private DictionaryWrapper type;
    private DictionaryWrapper unit;
    private int cost;
    private DictionaryWrapper currency;
    private String barcode;
    private String note;

    public Consuption() {
    }

    public Consuption(int id, String code, String name, DictionaryWrapper type, DictionaryWrapper unit, int cost, DictionaryWrapper currency, String barcode, String note) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.type = type;
        this.unit = unit;
        this.cost = cost;
        this.currency = currency;
        this.barcode = barcode;
        this.note = note;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DictionaryWrapper getType() {
        return type;
    }

    public void setType(DictionaryWrapper type) {
        this.type = type;
    }

    public DictionaryWrapper getUnit() {
        return unit;
    }

    public void setUnit(DictionaryWrapper unit) {
        this.unit = unit;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public DictionaryWrapper getCurrency() {
        return currency;
    }

    public void setCurrency(DictionaryWrapper currency) {
        this.currency = currency;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Consuption{" + "id=" + id + ", code=" + code + ", name=" + name + ", type=" + type + ", unit=" + unit + ", cost=" + cost + ", currency=" + currency + ", barcode=" + barcode + ", note=" + note + '}';
    }

    
    
    
}
