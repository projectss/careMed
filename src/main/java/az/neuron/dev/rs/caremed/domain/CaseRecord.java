/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class CaseRecord {
    private int id;
   
    private String caseNo;
    private String startDate;
    private String endDate;
    private int dayCount;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserId;

    public CaseRecord() {
    }

    public CaseRecord(int id, String caseNo, String startDate, String endDate, int dayCount) {
        this.id = id;
        this.caseNo = caseNo;
        this.startDate = startDate;
        this.endDate = endDate;
        this.dayCount = dayCount;
    }

    
    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

  

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getDayCount() {
        return dayCount;
    }

    public void setDayCount(int dayCount) {
        this.dayCount = dayCount;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Override
    public String toString() {
        return "caseRecord{" + "id=" + id + ",  caseNo=" + caseNo + ", startDate=" + startDate + ", endDate=" + endDate + ", dayCount=" + dayCount + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }
    
    
     
    
}
