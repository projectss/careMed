/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

import lombok.Data;

/**
 *
 * @author Admin
 */
public @Data class PatientProphylaxlsesVaccinationForm {

    private int id;
    private int patientId;
    private int vaccinationId;
    private String dateOfVaccination;
    private String againstWhichInfaction;
    private String doze;
    private String numberOfSeries;
    private String result;
    private String note;

}
