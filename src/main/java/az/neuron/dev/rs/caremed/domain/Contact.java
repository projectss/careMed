/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

import java.util.Date;

/**
 *
 * @author Bayram
 */
public class Contact {

    private int id;
    private DictionaryWrapper type;
    private String contact;
    private Date createDate;
    private Date lastUpdate;
    private boolean active;
    
    Contact(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getType() {
        return type;
    }

    public void setType(DictionaryWrapper type) {
        this.type = type;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Contact{" + "id=" + id + ", type=" + type + ", contact=" + contact + ", createDate=" + createDate + ", lastUpdate=" + lastUpdate + ", active=" + active + '}';
    }
    
    
}
