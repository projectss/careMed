/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

import java.util.List;

/**
 *
 * @author Bayram
 */
public class PersonDocument {
    private int id;
    private DictionaryWrapper type;
    private String docNum;
    private String docSerial;
    private String startDate;
    private String endDate;
    private String note;
    private List<FileWrapper> file;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserId;

    public PersonDocument() {
    }

    public PersonDocument(int id, DictionaryWrapper type, String docNum, String docSerial, String startDate, String endDate, String note, List<FileWrapper> file) {
        this.id = id;
        this.type = type;
        this.docNum = docNum;
        this.docSerial = docSerial;
        this.startDate = startDate;
        this.endDate = endDate;
        this.note = note;
        this.file = file;
    }

    public List<FileWrapper> getFile() {
        return file;
    }

    public void setFile(List<FileWrapper> file) {
        this.file = file;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getType() {
        return type;
    }

    public void setType(DictionaryWrapper type) {
        this.type = type;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getDocSerial() {
        return docSerial;
    }

    public void setDocSerial(String docSerial) {
        this.docSerial = docSerial;
    }

    @Override
    public String toString() {
        return "PersonDocument{" + "id=" + id + ", type=" + type + ", docNum=" + docNum + ", docSerial=" + docSerial + ", startDate=" + startDate + ", endDate=" + endDate + ", note=" + note + ", file=" + file + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }
}
