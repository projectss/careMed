/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class ServiceOperationForm {
    private Integer patientId;
    private Integer employeeId;
    private Integer serviceId;
    private String startDate;
    private String endDate;
    private Integer statusTypeId;
    private Integer queueTypeId;
    private Integer medicalCommitionId;
    private Integer paymentStatusId;
    private Integer price ;
    private String note;

    
    
    public ServiceOperationForm() {
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getStatusTypeId() {
        return statusTypeId;
    }

    public void setStatusTypeId(Integer statusTypeId) {
        this.statusTypeId = statusTypeId;
    }

    public Integer getQueueTypeId() {
        return queueTypeId;
    }

    public void setQueueTypeId(Integer queueTypeId) {
        this.queueTypeId = queueTypeId;
    }

    public Integer getMedicalCommitionId() {
        return medicalCommitionId;
    }

    public void setMedicalCommitionId(Integer medicalCommitionId) {
        this.medicalCommitionId = medicalCommitionId;
    }

    public Integer getPaymentStatusId() {
        return paymentStatusId;
    }

    public void setPaymentStatusId(Integer paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ServiceOperationForm{" + "patientId=" + patientId + ", employeeId=" + employeeId + ", serviceId=" + serviceId + ", startDate=" + startDate + ", endDate=" + endDate + ", statusTypeId=" + statusTypeId + ", queueTypeId=" + queueTypeId + ", medicalCommitionId=" + medicalCommitionId + ", paymentStatusId=" + paymentStatusId + ", price=" + price + ", note=" + note + '}';
    }
 
}
