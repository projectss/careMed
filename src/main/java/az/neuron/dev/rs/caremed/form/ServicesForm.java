/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

import lombok.Data;
import lombok.ToString;

/**
 *
 * @author Bayram
 */
@Data
@ToString
public class ServicesForm extends BaseForm {

    private int id;
    private int serviceTypeId;
    private int typeId;
    private int orgId;
    private String name;
    private String duration;
    private int cost;
    private int discountedCost;
    private int currencyId;
    private String serviceCode;
    private String note;

}
