/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.db.DbConnect;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.DictionaryWrapper;
import az.neuron.dev.rs.caremed.domain.MultilanguageString;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.PatienForwardByDoctor;
import az.neuron.dev.rs.caremed.domain.Patient;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.form.CheckPasientForm;
import az.neuron.dev.rs.caremed.form.OperationForm;
import az.neuron.dev.rs.caremed.search.PatientSearchByDoctor;
import az.neuron.dev.rs.caremed.util.Converter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bayram
 */
@Repository
@Log4j
public class DoctorDao implements IDoctorDao {

    @Autowired
    private DbConnect dbConnect;

    @Override
    public DataTable getPatientListByDoctor(String token, PatientSearchByDoctor searchByDoctor) {
        DataTable dataTable = new DataTable();
        // List<Patient> list = new  java.util.ArrayList<>();
        List<Patient> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.get_patient_list_by_emp(?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, token);
                if (searchByDoctor.getPaymentType() != null && searchByDoctor.getPaymentType() > 0) {
                    callableStatement.setInt(2, searchByDoctor.getPaymentType());
                } else {
                    callableStatement.setInt(2, 0);
                }

                if (searchByDoctor.getQueueId() != null && searchByDoctor.getQueueId() > 0) {
                    callableStatement.setInt(3, searchByDoctor.getQueueId());
                } else {
                    callableStatement.setInt(3, 0);
                }

                callableStatement.setInt(4, searchByDoctor.getStart());
                callableStatement.setInt(5, searchByDoctor.getLength());
                callableStatement.registerOutParameter(6, Types.INTEGER);
                callableStatement.registerOutParameter(7, Types.OTHER);

                callableStatement.execute();

                try (ResultSet resultSet = (ResultSet) callableStatement.getObject(7)) {

                    while (resultSet.next()) {
                        list.add(new Patient(
                                resultSet.getInt("patient_id"),
                                resultSet.getInt("id"), resultSet.getInt("r"), resultSet.getString("patient_doc_num"),
                                resultSet.getString("start_date"),
                                new DictionaryWrapper(resultSet.getInt("payment_status"),
                                        new MultilanguageString(resultSet.getString("payment_status_name_az"),
                                                resultSet.getString("payment_status_name_en"),
                                                resultSet.getString("payment_status_name_ru"))),
                                new DictionaryWrapper(resultSet.getInt("status_type_id"),
                                        new MultilanguageString(resultSet.getString("status_name_az"),
                                                resultSet.getString("status_name_en"),
                                                resultSet.getString("status_name_ru"))),
                                resultSet.getInt("patient_person_id"),
                                resultSet.getString("patient_first_name"),
                                resultSet.getString("patient_last_name"),
                                resultSet.getString("patient_middle_name"),
                                new DictionaryWrapper(resultSet.getInt("patient_gender"),
                                        new MultilanguageString(resultSet.getString("patient_gender_az"),
                                                resultSet.getString("patient_gender_en"),
                                                resultSet.getString("patient_gender_ru"))),
                                resultSet.getString("patient_birthdate"),
                                new DictionaryWrapper(resultSet.getInt("patient_service"),
                                        resultSet.getString("service_name_az"))));
                    }
                    dataTable.setDataList(list);
                    dataTable.setTotalCount(callableStatement.getInt(6));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return dataTable;
    }

    @Override
    public OperationResponse addCheckUpPatient(String token, CheckPasientForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.add_check_up(?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getPatientServiceOperationId());
                callableStatement.setArray(3, connection.createArrayOf("text", Converter.toPlpgCheckUp(form.getCheckUp())));

                callableStatement.execute();
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse addSubServicesByDoctor(String token, OperationForm operationForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            int serviceId = 0;
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.add_patient_service_operation (?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setArray(3, connection.createArrayOf("text", Converter.toPlpgServiceOperation(operationForm.getServiceOperationForm())));
                callableStatement.execute();

                serviceId = callableStatement.getInt(1);
                operationResponse.setData(serviceId);
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public List<PatienForwardByDoctor> getPatientForwardByDoctor(String token, int id) {
        List<PatienForwardByDoctor> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareCall("select * from cm_caremed.v_patient_service_operations s where s.to_employee_id is null and patient_id= ?")) {
                preparedStatement.setInt(1, id);

                try (ResultSet rs = (ResultSet) preparedStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new PatienForwardByDoctor(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("org_id"),
                                        new MultilanguageString(rs.getString("org_name_az"),
                                                rs.getString("org_name_en"),
                                                rs.getString("org_name_ru"))),
                                new DictionaryWrapper(rs.getInt("patient_service"), rs.getString("service_name_az")),
                                new DictionaryWrapper(rs.getInt("service_type"),
                                        new MultilanguageString(rs.getString("service_type_name_az"),
                                                rs.getString("service_type_name_en"),
                                                rs.getString("service_type_name_ru"))),
                                new DictionaryWrapper(rs.getInt("queue_type_id"),
                                        new MultilanguageString(rs.getString("queue_name_az"),
                                                rs.getString("queue_name_en"),
                                                rs.getString("queue_name_ru"))),
                                new DictionaryWrapper(rs.getInt("payment_status"),
                                        new MultilanguageString(rs.getString("payment_status_name_az"),
                                                rs.getString("payment_status_name_en"),
                                                rs.getString("payment_status_name_ru")))));

                    }

                }

            }
        } catch (Exception e) {
        }

        return list;
    }

}
