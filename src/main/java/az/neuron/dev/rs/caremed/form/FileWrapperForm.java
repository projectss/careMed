/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Bayram
 */
public class FileWrapperForm {
    private int id;
    private String originalName;
    private String path;
    private MultipartFile file;

    public FileWrapperForm() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "FileWrapperForm{" + "id=" + id + ", originalName=" + originalName + ", path=" + path + ", file=" + file + '}';
    }

}
