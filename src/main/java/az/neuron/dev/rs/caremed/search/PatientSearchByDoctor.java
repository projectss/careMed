/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.search;

/**
 *
 * @author Bayram
 */
public class PatientSearchByDoctor {

    private String orderColumn = "id";
    private String orderType = "asc";
    private Integer start = 0;
    private Integer length = 10;
    private Integer queueId;
    private Integer paymentType;

    public PatientSearchByDoctor() {
    }

    
    
    
    public String getOrderColumn() {
        return orderColumn;
    }

    
    
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getQueueId() {
        return queueId;
    }

    public void setQueueId(Integer queueId) {
        this.queueId = queueId;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public String toString() {
        return "PatientSearchByDoctor{" + "orderColumn=" + orderColumn + ", orderType=" + orderType + ", start=" + start + ", length=" + length + ", queueId=" + queueId + ", paymentType=" + paymentType + '}';
    }
    

}
