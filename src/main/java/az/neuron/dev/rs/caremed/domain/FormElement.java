/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class FormElement {
    private int id;
    private Services service;
    private DictionaryWrapper htmlType;
    private String i18n;
    private DictionaryWrapper selectType;
    private String attrClass;
    private String inputType;

    public FormElement() {
    }

    public FormElement(int id, Services service, DictionaryWrapper htmlType, String i18n, DictionaryWrapper selectType, String attrClass, String inputType) {
        this.id = id;
        this.service = service;
        this.htmlType = htmlType;
        this.i18n = i18n;
        this.selectType = selectType;
        this.attrClass = attrClass;
        this.inputType = inputType;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Services getService() {
        return service;
    }

    public void setService(Services service) {
        this.service = service;
    }

    public DictionaryWrapper getHtmlType() {
        return htmlType;
    }

    public void setHtmlType(DictionaryWrapper htmlType) {
        this.htmlType = htmlType;
    }

    public String getI18n() {
        return i18n;
    }

    public void setI18n(String i18n) {
        this.i18n = i18n;
    }

    public DictionaryWrapper getSelectType() {
        return selectType;
    }

    public void setSelectType(DictionaryWrapper selectType) {
        this.selectType = selectType;
    }

    public String getAttrClass() {
        return attrClass;
    }

    public void setAttrClass(String attrClass) {
        this.attrClass = attrClass;
    }

    @Override
    public String toString() {
        return "FormElement{" + "id=" + id + ", service=" + service + ", htmlType=" + htmlType + ", i18n=" + i18n + ", selectType=" + selectType + ", attrClass=" + attrClass + ", inputType=" + inputType + '}';
    }
}
