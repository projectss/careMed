/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class WorklifeCycleForm extends BaseForm{

    private int id;
    private String code;
    private Integer actionTypeId;
    private String actionDate;
    private Integer orgId;
    private String orgName;
    private String cardNo;
    private Integer staffTypeId;
    private String workBlock;
    private Integer positionId;
    private Integer status;
    
    WorklifeCycleForm(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getActionTypeId() {
        return actionTypeId;
    }

    public void setActionTypeId(Integer actionTypeId) {
        this.actionTypeId = actionTypeId;
    }

    public String getActionDate() {
        return actionDate;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public Integer getStaffTypeId() {
        return staffTypeId;
    }

    public void setStaffTypeId(Integer staffTypeId) {
        this.staffTypeId = staffTypeId;
    }

    public String getWorkBlock() {
        return workBlock;
    }

    public void setWorkBlock(String workBlock) {
        this.workBlock = workBlock;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "WorklifeCycleForm{" + "id=" + id + ", code=" + code + ", actionTypeId=" + actionTypeId + ", actionDate=" + actionDate + ", orgId=" + orgId + ", orgName=" + orgName + ", cardNo=" + cardNo + ", staffTypeId=" + staffTypeId + ", workBlock=" + workBlock + ", positionId=" + positionId + ", status=" + status + '}';
    }
    
    

}
