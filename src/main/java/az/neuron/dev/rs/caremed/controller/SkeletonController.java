/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.controller;

import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.User;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.exception.AuthenticationException;
import az.neuron.dev.rs.caremed.exception.CareMedException;
import az.neuron.dev.rs.caremed.exception.InvalidParametersException;
import az.neuron.dev.rs.caremed.services.CareMedSevice;
import az.neuron.dev.rs.caremed.services.DoctorService;
import az.neuron.dev.rs.caremed.services.FtpService;
import az.neuron.dev.rs.caremed.services.OperationService;
import az.neuron.dev.rs.caremed.services.PatientService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author Bayram
 */
@Controller
public class SkeletonController extends AbstractController{
 
    @Value("${ftp.root.directory}")
    protected String rootDirectory;
    
    @Autowired
    protected FtpService ftpService;
    
    @Autowired
    protected HttpServletRequest request;
    
    @Autowired
    protected CareMedSevice service;
    
    @Autowired
    protected PatientService patient;
    
    @Autowired
    protected OperationService operation;
    
    @Autowired
    protected DoctorService doctor;
    
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    protected User checkToken(OperationResponse operationResponse,String token,String... params) throws CareMedException{
    
        if(token==null || token.trim().isEmpty()){
        operationResponse.setCode(ResultCode.UNAUTHORIZED);
       throw new InvalidParametersException("Token is null");
        
        }
        
        if(params!=null){
            for(String s: params){
              if(s == null || s.trim().isEmpty()) {
                    operationResponse.setCode(ResultCode.INVALID_PARAMS);
                    throw new InvalidParametersException("Required Parameter is null");
                }
            }
        }
       
       User user = service.checkToken(token);
       
       if(user == null) {
           operationResponse.setCode(ResultCode.UNAUTHORIZED);
           throw new AuthenticationException("Invalid token!");
       }
       
       return user;
    }
    
    
    protected String getSecurityToken() {
        return request.getHeader("Token");
    }
     
    protected User checkToken(String token) throws CareMedException {
        if(token == null || token.trim().isEmpty()) {
            throw new InvalidParametersException("Token is null");
        } 
        
        User user = service.checkToken(token);
       
       if(user == null) {
           throw new AuthenticationException("Invalid token!");
       }
       
       return user;
    }
    
    
}
