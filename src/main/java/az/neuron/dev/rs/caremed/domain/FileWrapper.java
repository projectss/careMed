/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class FileWrapper {
    private int id;
    private String originalName;
    private String path;
    private long size;
    private String contentType;
    private byte[] file;

    public FileWrapper() {
    }

    public FileWrapper(int id) {
        this.id = id;
    }

    public FileWrapper(int id, String originalName, String path) {
        this.id = id;
        this.originalName = originalName;
        this.path = path;
    }

    public FileWrapper(String path) {
        this.path = path;
    }
    

    public FileWrapper(int id, String originalName, String path, byte[] file) {
        this.id = id;
        this.originalName = originalName;
        this.path = path;
        this.file = file;
    }

    public FileWrapper(String originalName, String path, long size, String contentType) {
        this.originalName = originalName;
        this.path = path;
        this.size = size;
        this.contentType = contentType;
    }
    
    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public String toString() {
        return "FileWrapper{" + "id=" + id + ", originalName=" + originalName + ", path=" + path + ", size=" + size + ", contentType=" + contentType + ", file=" + file + '}';
    }
}
