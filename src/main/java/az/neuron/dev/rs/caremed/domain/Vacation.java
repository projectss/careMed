/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class Vacation {
    private int id;
    private DictionaryWrapper vacationTypeId;
    private String datePeriod;
    private int dayCount;
    private String startDate;
    private String endDate;
    private String orderNo;
    private String nextVacationDate;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserId;

    public Vacation() {
    }

    public Vacation(int id, DictionaryWrapper vacationTypeId, String datePeriod, int dayCount, String startDate, String endDate, String orderNo,String nextVacationDate) {
        this.id = id;
        this.vacationTypeId = vacationTypeId;
        this.datePeriod = datePeriod;
        this.dayCount = dayCount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.orderNo = orderNo;
        this.nextVacationDate=nextVacationDate;
    }

    
    

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   


    public DictionaryWrapper getVacationTypeId() {
        return vacationTypeId;
    }

    public void setVacationTypeId(DictionaryWrapper vacationTypeId) {
        this.vacationTypeId = vacationTypeId;
    }

    public String getDatePeriod() {
        return datePeriod;
    }

    public void setDatePeriod(String datePeriod) {
        this.datePeriod = datePeriod;
    }

    public int getDayCount() {
        return dayCount;
    }

    public void setDayCount(int dayCount) {
        this.dayCount = dayCount;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getNextVacationDate() {
        return nextVacationDate;
    }

    public void setNextVacationDate(String nextVacationDate) {
        this.nextVacationDate = nextVacationDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }
    
    
     @Override
    public String toString() {
        return "Vacation{" + "id=" + id + ", vacationTypeId=" + vacationTypeId + ", datePeriod=" + datePeriod + ", dayCount=" + dayCount + ", startDate=" + startDate + ", endDate=" + endDate + ", orderNo=" + orderNo + ", nextVacationDate=" + nextVacationDate + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }
}
