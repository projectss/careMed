/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.search;

import az.neuron.dev.rs.caremed.form.BaseForm;

/**
 *
 * @author Bayram
 */
public class ServiceSearchForm  extends BaseForm{
    
    private String orderColumn = "id";
    private String orderType = "asc";
    private Integer start = 0;
    private Integer length = 10;
    private String name;
    private Integer serviceTypeId;

    public ServiceSearchForm() {
    }

    public String getOrderColumn() {
        return orderColumn;
    }

    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    @Override
    public String toString() {
        return "ServiceSearchForm{" + "orderColumn=" + orderColumn + ", orderType=" + orderType + ", start=" + start + ", length=" + length + ", name=" + name + ", serviceTypeId=" + serviceTypeId + '}';
    }

    
    
    
}
