/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class DictionaryWrapper {
    private int id;
    private MultilanguageString value;
    private int typeId;
    private int parentId;
    private String value1;

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public DictionaryWrapper() {
    }

    public DictionaryWrapper(int id) {
        this.id = id;
    }
    
    public DictionaryWrapper(int id, MultilanguageString value, int typeId, int parentId) {
        this.id = id;
        this.value = value;
        this.typeId = typeId;
        this.parentId = parentId;
    }

    public DictionaryWrapper(int id, MultilanguageString value, int typeId) {
        this.id = id;
        this.value = value;
        this.typeId = typeId;
    }
    
    public DictionaryWrapper(int id, MultilanguageString value) {
        this.id = id;
        this.value = value;
    }

    public MultilanguageString getValue() {
        return value;
    }

    public void setValue(MultilanguageString value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTypeId() {
        return typeId;
    }

    public DictionaryWrapper(int id, String value1) {
        this.id = id;
        this.value1 = value1;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "DictionaryWrapper{" + "id=" + id + ", value=" + value + ", typeId=" + typeId + ", parentId=" + parentId + ", value1=" + value1 + '}';
    }

}
