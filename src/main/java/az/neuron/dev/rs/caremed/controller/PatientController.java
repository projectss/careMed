/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.controller;

import az.neuron.dev.rs.caremed.domain.FileWrapper;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.User;
import az.neuron.dev.rs.caremed.enums.Constants;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.exception.InvalidParametersException;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.BaseForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.DocumentForm;
import az.neuron.dev.rs.caremed.form.EmployeeForm;
import az.neuron.dev.rs.caremed.form.EmployeeHistoryForm;
import az.neuron.dev.rs.caremed.form.FileWrapperForm;
import az.neuron.dev.rs.caremed.form.HamfullStuffForm;
import az.neuron.dev.rs.caremed.form.PatientForm;
import az.neuron.dev.rs.caremed.form.PatientHamfullStuffForm;
import az.neuron.dev.rs.caremed.form.PatientHistoryForm;
import az.neuron.dev.rs.caremed.form.PatientProphylaxlsesVaccinationForm;
import az.neuron.dev.rs.caremed.form.PatientSpecialNotesForm;
import az.neuron.dev.rs.caremed.form.PersonForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import az.neuron.dev.rs.caremed.search.PatientHistorySearchForm;
import az.neuron.dev.rs.caremed.search.PatientSearchForm;
import az.neuron.dev.rs.caremed.util.Crypto;
import az.neuron.dev.rs.caremed.validator.ContactFormValidator;
import az.neuron.dev.rs.caremed.validator.EmployeeFormValidator;
import az.neuron.dev.rs.caremed.validator.FileWrapperFormValidator;
import az.neuron.dev.rs.caremed.validator.PatientFormValidator;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import static org.apache.log4j.LogMF.log;
import static org.apache.log4j.LogMF.log;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 *
 * @author Bayram
 */
@RestController
@RequestMapping(value = "/patients")
public class PatientController extends SkeletonController {
    
    private static final Logger log = Logger.getLogger(PatientController.class);

// ----------------------Xestelerin geydiyatii ------------------------------------------------------------
    @PostMapping(value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse addPatient(@RequestPart(name = "patient", required = false) PatientForm form,
            @RequestPart(name = "image", required = false) MultipartFile image,
            MultipartHttpServletRequest multipartHttpServletRequest,
            BindingResult result) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, form.getToken());
// validation 
//
//            PatientFormValidator validator = new PatientFormValidator(Constants.VALIDATOR_ADD);
//            validator.validate(form, result);
//
//            if (result.hasErrors()) {
//                operationResponse.setData(result.getAllErrors());
//                operationResponse.setCode(ResultCode.INVALID_PARAMS);
//                throw new InvalidParametersException("Invalid patientform params. PatientForm :" + form + ",errors: " + result.getAllErrors());
//            }

            MultiValueMap<String, MultipartFile> requestParts = multipartHttpServletRequest.getMultiFileMap();
            DocumentForm[] documents = form.getDocuments();
            List<MultipartFile> files;
            FileWrapperForm[] fileWrappers;
            String docFullPath;
            
            FileWrapperFormValidator fileWrapperFormValidator = new FileWrapperFormValidator();

//            for (DocumentForm documentForm : documents) {
//                files = requestParts.get("doc_" + documentForm.getId());
//
//                if (files != null) {
//                    if (files.size() > 5) {
//                        throw new InvalidParametersException("invalid file count in document");
//                    }
//                    fileWrappers = new FileWrapperForm[files.size()];
//                    int i = 0;
//
//                    for (MultipartFile file : files) {
//                        if (file != null && file.getSize() > 0) {
//                            fileWrappers[i] = new FileWrapperForm();
//                            fileWrappers[i].setFile(file);
////                            fileWrapperFormValidator.validate(fileWrappers[i], result);
////
////                            if (result.hasErrors()) {
////                                operationResponse.setData(result.getAllErrors());
////                                operationResponse.setCode(ResultCode.ERROR);
////                                throw new InvalidParametersException("Invalid file content type in teacher form. Form:  " + employeeForm + ", errors: " + result.getAllErrors());
////                            }
//
//                            OperationResponse saveFileResponse = ftpService.saveFtpFile(rootDirectory + "/patient_docs", file, Crypto.getGuid());
//
//                            if (saveFileResponse.getCode() == ResultCode.OK) {
//                                docFullPath = (String) saveFileResponse.getData();
//                                fileWrappers[i].setOriginalName(file.getOriginalFilename());
//                                fileWrappers[i].setPath(docFullPath);
//                                i++;
//                            }
//                        }
//                    }
//
//                    documentForm.setUploads(fileWrappers);
//                }
//            }
//
//            form.setDocuments(documents);
            String fullPath = null;
            
            if (image != null && image.getSize() > 0) {
                OperationResponse saveImageResponse = ftpService.saveFtpFile(rootDirectory + "/patients", image, Crypto.getGuid());
                
                if (saveImageResponse.getCode() == ResultCode.OK) {
                    fullPath = (String) saveImageResponse.getData();
                }
            }
            operationResponse = patient.addPatient(form.getToken(), form, null, image != null ? image.getOriginalFilename() : "");
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
            
        }
        
        return operationResponse;
    }
    // --------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------- orta panelin Xestelerin listi --------------------------------------------------------
    @PostMapping
    protected OperationResponse getPatientList(BaseForm form, PatientSearchForm searchForm, BindingResult result) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, form.getToken());
            System.out.println("lenggh " + searchForm.getLength() + "stat " + searchForm.getStart() + " order by " + searchForm.getOrderType() + "dsds");
            operationResponse.setData(patient.getPatientList(form.getToken(), searchForm));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }

    //----------------------------------------------------------------------
    //// -------- saq panele gore  patient gore ,xestenin  melumatlari cixardir -----------------------------
    @GetMapping(value = "/{id:\\d+}/info")
    protected OperationResponse getPatientDetails(BaseForm form,
            @PathVariable("id") int patientId) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, form.getToken());
            
            operationResponse.setData(patient.getPatientDetails(form.getToken(), patientId));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/patientInfo/edit")
    protected OperationResponse editPersonalInfo(@RequestPart(name = "employee1", required = false) PersonForm form,
            @RequestPart(name = "image", required = false) MultipartFile image,
            MultipartHttpServletRequest multipartHttpServletRequest,
            @PathVariable("id") int id) {
        System.out.println("sdsd");
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(form.getToken());
            log.info("/{id:\\d+}/patientInfo/edit [POST]. Form: " + form + ", patient id: " + id + ", User: " + user);
            String fullPath = null;
            
            if (image != null && image.getSize() > 0) {
                OperationResponse saveImageResponse = ftpService.saveFtpFile(rootDirectory + "/employees", image, Crypto.getGuid());
                
                if (saveImageResponse.getCode() == ResultCode.OK) {
                    fullPath = (String) saveImageResponse.getData();
                }
            }
            operationResponse.setData(service.editPersonalInfo(form.getToken(), form, fullPath, image != null ? image.getOriginalFilename() : ""));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
        
    }

    // sekiller
    @GetMapping(value = "/{id:\\d+}/image", produces = MediaType.IMAGE_JPEG_VALUE) //OK
    protected byte[] getEmployeeImage(@PathVariable int id,
            BaseForm form) {
        
        try {
            User user = checkToken(form.getToken());
            log.info("/patient/{id}/image [GET]. Id: " + id + ", User: " + user + ", Form: " + form);
            
            FileWrapper image = service.getEmployeeImage(id);
            
            if (image != null) {
                return ftpService.downloadFtpFile(image.getPath());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return null;
    }
    // -----------------------------------------------------------------------------------------------------

    // --------------check patient pincode -------------------------------------------
    @GetMapping(value = "/patientPincode/{pincode}")
    protected OperationResponse checkePatientPincode(BaseForm form,
            @PathVariable("pincode") String pincode) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, form.getToken());
            operationResponse.setData(patient.checkPatientPincode(form.getToken(), pincode));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }

    //--------------------------------------------------------------------------------
    // ---------------- add , edit , remove patient
    @PostMapping(value = "/{id:\\d+}/patient/edit")
    protected OperationResponse editPatient(PatientForm form,
            @PathVariable("id") int id) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(form.getToken());
            form.setId(id);
            operationResponse = patient.editPatient(form.getToken(), form);
            log.info("employee /{id:\\d+}/job/edit [POST]. Form: " + form + ", patient id: " + id + ", User: " + user);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/remove")
    protected OperationResponse removePatient(@PathVariable("id") int id,
            PatientHistoryForm historyForm,
            BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(operationResponse, historyForm.getToken());
            
            log.info("{id:\\d+}/remove/patient/ [POST]. Form: " + historyForm + ", patient id: " + id + ", User: " + user);
            
            operationResponse = patient.removePatient(historyForm.getToken(), id, historyForm);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
        
    }

    // --------------------------------------------------------------------------------------------------------------------
    //------ add,edit,remove cotnact ,address, realationship , document  --------------------------------------------------
    @GetMapping(value = "/{id:\\d+}/contact")
    protected OperationResponse getPatientContact(BaseForm baseForm,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(baseForm.getToken());
            
            log.info("/{id:\\d+}/contact [GET]. Form: " + baseForm + ", patient id: " + id + ", User: " + user);
            operationResponse.setData(patient.getPatientContact(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/contact/{contactId:\\d+}/edit")
    protected OperationResponse editContact(BaseForm baseForm,
            ContactForm contactForm,
            @PathVariable("id") int id,
            @PathVariable("contactId") int contactId,
            BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            
            ContactFormValidator validator = new ContactFormValidator(Constants.VALIDATOR_EDIT);
            validator.validate(contactForm, result);
            
            if (result.hasErrors()) {
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                throw new InvalidParametersException("Invalid Patient form params. Form:  " + contactForm + ", errors: " + result.getAllErrors());
            }
            
            contactForm.setId(contactId);
            
            operationResponse = patient.editPatientContact(baseForm.getToken(), id, contactForm);
            log.info("patient /{id:\\d+}/contact/edit [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }

    // ADD
    @PostMapping(value = "/{id:\\d+}/contact/add")
    protected OperationResponse addCotanct(BaseForm baseForm,
            ContactForm form, @PathVariable("id") int id,
            BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            
            ContactFormValidator validator = new ContactFormValidator(Constants.VALIDATOR_ADD);
            validator.validate(form, result);
            
            if (result.hasErrors()) {
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                throw new InvalidParametersException("Invalid Patient form params. Form:  " + form + ", errors: " + result.getAllErrors());
                
            }
            
            form.setId(id);
            
            operationResponse = patient.addPatientContact(baseForm.getToken(), id, form);
            log.info("patient /{id:\\d+}/contact/add [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/contact/{contactId:\\d+}/remove")
    protected OperationResponse removePatientContact(BaseForm baseform,
            ContactForm form, @PathVariable("id") int id, @PathVariable("contactId") int contactId,
            BindingResult result) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseform.getToken());
            
            ContactFormValidator validator = new ContactFormValidator(Constants.VALIDATOR_REMOVE);
            validator.validate(form, result);
            
            if (result.hasErrors()) {
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                throw new InvalidParametersException("Invalid Patient form params. Form:  " + form + ", errors: " + result.getAllErrors());
                
            }
            
            form.setId(id);
            operationResponse = service.removePersonContact(baseform.getToken(), id, contactId);
            log.info("patient /{id:\\d+}/contact/remove [POST]. Form: " + form + ", contact id: " + id + ", User: " + user);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }

    //---------------------------------------------------------------------------------------------------------------------
    // Person Address get, edit , add, remove 
    @GetMapping(value = "/{id:\\d+}/address")
    protected OperationResponse getPatientAddress(BaseForm baseForm,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            
            log.info("/{id:\\d+}/address [GET]. Form: " + baseForm + ", patient id: " + id + ", User: " + user);
            
            operationResponse.setData(patient.getPatientAddress(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/address/{addressId:\\d+}/edit")
    protected OperationResponse editPatientAddress(BaseForm baseForm,
            AddressForm form,
            @PathVariable("id") int id,
            @PathVariable("addressId") int addressId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            
            form.setId(addressId);
            
            operationResponse = patient.editPatientAddress(baseForm.getToken(), form);
            log.info("patient /{id:\\d+}/language/edit [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/address/add")
    protected OperationResponse addPatientAddress(BaseForm baseForm,
            AddressForm form,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            
            log.info("patient /{id:\\d+}/address/add [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);
            
            operationResponse = patient.addPatientAddress(baseForm.getToken(), id, form);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/address/{addressId:\\d+}/remove")
    protected OperationResponse removePersonAddress(BaseForm baseForm,
            AddressForm form,
            @PathVariable("id") int id,
            @PathVariable("addressId") int addressId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            
            operationResponse = patient.removePatientAddress(baseForm.getToken(), id, addressId);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
        
    }

    // -------------------------------------------------------------------------------------------------------
    // ------------------------------------------Relation : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/relation/add")
    protected OperationResponse addPatientRealtionShip(BaseForm baseForm,
            PersonRelationForm form,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            log.info("patient /{id:\\d+}/relation/add [POST]. Form: " + baseForm + ", patient id: " + id + ", User: " + user);
            
            operationResponse = patient.addPatientRelationShip(baseForm.getToken(), id, form);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/relation/{relationId:\\d+}/edit")
    protected OperationResponse editPatientRealtionShip(BaseForm baseForm,
            PersonRelationForm form,
            @PathVariable("id") int id,
            @PathVariable("relationId") int relationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            
            form.setId(relationId);
            
            operationResponse = patient.editPatientRelationship(baseForm.getToken(), id, form);
            log.info("patient /{id:\\d+}/relation/edit [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/relation/{relationId:\\d+}/remove")
    protected OperationResponse removePatientRealtionShip(BaseForm baseForm,
            @PathVariable("id") int id,
            @PathVariable("relationId") int relationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = patient.removePatientRelationship(baseForm.getToken(), id, relationId);
            log.info("patient /{id:\\d+}/relation/edit [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/relation")
    protected OperationResponse getPatientRealtionShip(BaseForm baseForm,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            log.info("patient /{id:\\d+}/relation [GET]. Form: " + baseForm + ", patient id: " + id + ", User: " + user);
            operationResponse.setData(patient.getPatientRealtionship(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }

    // -------------------------------------------------------------------------------------------------------
    // ----------------DOCUMENTS ----------------------------------------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/document/add")
    protected OperationResponse addDocument(BaseForm baseForm,
            DocumentForm form,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            log.info("patient /{id:\\d+}/document/add [POST]. Form: " + baseForm + ", patient id: " + id + ", User: " + user + ", Form: " + form);
            
            operationResponse = patient.addDocument(form, baseForm.getToken(), id);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
        
    }

    // -------------------
    @PostMapping(value = "/{id:\\d+}/document/{documentId:\\d+}/edit")
    protected OperationResponse editDocument(BaseForm baseForm,
            DocumentForm form,
            @PathVariable("id") int id,
            @PathVariable("documentId") int documentId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            
            form.setId(documentId);
            log.info("patient /{id:\\d+}/document/edit [POST]. Form: " + baseForm + ", patient id: " + id + ", User: " + user + ", form: " + form);
            
            operationResponse = patient.editPatientDocument(baseForm.getToken(), id, form);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/document/{documentId:\\d+}/remove")
    protected OperationResponse removeDocument(BaseForm baseForm,
            @PathVariable("id") int id,
            @PathVariable("documentId") int documentId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            log.info("patient /{id:\\d+}/document/{documentId:\\d+}/remove [POST]. Form: " + baseForm + ", patient id: " + id + ", User: " + user + ", educationId: " + documentId);
            
            operationResponse = patient.removePatientDocument(baseForm.getToken(), id, documentId);
            List<FileWrapper> list = (List) operationResponse.getData();
            
            for (FileWrapper f : list) {
                operationResponse = ftpService.removeFtpFile(f.getPath());
            }
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }

    //----------------------------------
    // --- get Document
    @GetMapping(value = "/{id:\\d+}/document")
    protected OperationResponse getDocument(BaseForm baseForm,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            log.info("patient /{id:\\d+}/document [GET]. Form: " + baseForm + ", patient id: " + id + ", User: " + user);
            
            operationResponse.setData(patient.getPatientDocument(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }

    // -------------------------------------------------------------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/document/{docId:\\d+}/file/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE) //OK
    protected OperationResponse addFiles(BaseForm form,
            @PathVariable("id") int id,
            @PathVariable("docId") int docId,
            MultipartHttpServletRequest multipartHttpServletRequest,
            @RequestPart(name = "file", required = false) MultipartFile file,
            BindingResult result) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, form.getToken());
            
            log.info("{id:\\d+}/document/{docId:\\d+}/file/add [POST]. User: " + user + ", Form: " + form);
            
            MultiValueMap<String, MultipartFile> requestParts = multipartHttpServletRequest.getMultiFileMap();
            List<MultipartFile> files;
            String docFullPath;
            DocumentForm documentForm = new DocumentForm();
            files = requestParts.get("file");
            if (files.size() > 5) {
                throw new InvalidParametersException("invalid file count in document");
            }
            FileWrapperForm[] fileWrappers = new FileWrapperForm[files.size()];
            FileWrapperFormValidator fileWrapperFormValidator = new FileWrapperFormValidator();
            int i = 0;
            for (MultipartFile multipartFile : files) {
                
                if (multipartFile != null && multipartFile.getSize() > 0) {
                    fileWrappers[i] = new FileWrapperForm();
                    fileWrappers[i].setFile(file);
                    fileWrapperFormValidator.validate(fileWrappers[i], result);
                    
                    if (result.hasErrors()) {
                        operationResponse.setData(result.getAllErrors());
                        operationResponse.setCode(ResultCode.ERROR);
                        throw new InvalidParametersException("invalid file content type in patient form. Form: " + form + ", errors: " + result.getAllErrors());
                    }
                    OperationResponse saveFileResponse = ftpService.saveFtpFile(rootDirectory + "/patient_docs", multipartFile, Crypto.getGuid());
                    
                    if (saveFileResponse.getCode() == ResultCode.OK) {
                        docFullPath = (String) saveFileResponse.getData();
                        fileWrappers[i].setOriginalName(multipartFile.getOriginalFilename());
                        fileWrappers[i].setPath(docFullPath);
                        
                    }
                }
                documentForm.setUploads(fileWrappers);
                i = i + 1;
            }
            documentForm.setId(docId);
            operationResponse = service.addDocFile(documentForm, form.getToken());
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/file/{id:\\d+}", produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.APPLICATION_PDF_VALUE}) //OK
    protected byte[] getFile(@PathVariable int id,
            BaseForm form,
            HttpServletResponse response) {
        
        try {
            FileWrapper file = service.getFileByFileId(id);
            if (file != null) {
                response.setHeader("Content-Disposition", "attachment; filename=" + file.getOriginalName());
                return ftpService.downloadFtpFile(file.getPath());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return null;
    }
    
    @PostMapping("/{id:\\d+}/document/{docId:\\d+}/file/{fileId:\\d+}/remove") //OK
    protected OperationResponse removeTeacherDocFiles(@PathVariable("id") int id,
            @PathVariable("docId") int docId,
            @PathVariable("fileId") int fileId,
            BaseForm form) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, form.getToken());
            
            log.info("/patients/{id:\\d+}/document/file/{fileId:\\d+}/remove [POST]. Form: " + form + ", patient id: " + id + ", User: " + user);
            
            operationResponse = service.removeDocFile(form.getToken(), docId, fileId);
            
            FileWrapper file = (FileWrapper) operationResponse.getData();
            if (operationResponse.getCode() == ResultCode.OK) {
                if (file != null) {
                    operationResponse = ftpService.removeFtpFile(file.getPath());
                }
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }

    // ------------------ Xestetnin historissiii -------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/patientHistory")
    protected OperationResponse getPatientHistoryList(BaseForm form,
            @PathVariable("id") int id, PatientHistorySearchForm searchForm, BindingResult result) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, form.getToken());
            System.out.println("lenggh " + searchForm.getLength() + "stat " + searchForm.getStart() + " order by " + searchForm.getOrderType() + "dsds");
            operationResponse.setData(patient.getPatientHistoryList(form.getToken(), id, searchForm));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }

    //Change patient status
    @PostMapping(value = "/{id:\\d+}/status/{statusId:\\d+}/changeStatusPatient")
    protected OperationResponse changeStatusPatient(BaseForm form, @PathVariable("id") int id, @PathVariable("statusId") int statusId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(operationResponse, form.getToken());
            operationResponse.setData(patient.changePatientStatus(form.getToken(), id, statusId));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        
        return operationResponse;
        
    }
    
    @PostMapping(value = "/addHamfullStuff")
    protected OperationResponse addPatientHamfullStuff(BaseForm baseForm, HamfullStuffForm hamfullStuffForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.addPatientHamfullStuff(baseForm.getToken(), hamfullStuffForm));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/editHamfullStuff")
    protected OperationResponse editPatientHamfullStuff(BaseForm baseForm, HamfullStuffForm form, @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.OK);
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.editPatientHamfullStuff(baseForm.getToken(), id, form));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/getHarmfullStuff")
    protected OperationResponse getHarmfullStuffList(BaseForm baseForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.getHamfullStuff(baseForm.getToken()));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/addProphylaxlsesVaccination")
    protected OperationResponse addPatientProphylaxlsesVaccination(BaseForm baseForm, PatientProphylaxlsesVaccinationForm vaccinationForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.addPatientProphylaxlsesVaccination(baseForm.getToken(), vaccinationForm));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/getPatientProphylaxlsesVaccinatio")
    protected OperationResponse getPatientProphylaxlsesVaccinatio(BaseForm baseForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.getPatientProphylaxlsesVaccination(baseForm.getToken()));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/editPatientProphylaxlsesVaccination")
    protected OperationResponse editPatientProphylaxlsesVaccination(BaseForm baseForm, PatientProphylaxlsesVaccinationForm form, @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.OK);
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.editPatientProphylaxlsesVaccination(baseForm.getToken(), id, form));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/addPatientSpecialNotes")
    protected OperationResponse addPatientSpecialNotes(BaseForm baseForm, PatientSpecialNotesForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.addPatientSpecialNotes(baseForm.getToken(), form));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }    
    
    @GetMapping(value = "/getPatientSpecialNotes")
    protected OperationResponse getPatientSpecialNotes(BaseForm baseForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.getPatientSpecialNotes(baseForm.getToken()));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/removePatientHamfullStuff")
    protected OperationResponse removePatientHamfullStuff(BaseForm baseForm, @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.removePatientHamfullStuff(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
        
    }
    
    @PostMapping(value = "/{id:\\d+}/removePatientProphylaxlsesVaccination")
    protected OperationResponse removePatientProphylaxlsesVaccination(BaseForm baseForm, @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(patient.removePatientProphylaxlsesVaccination(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
        
    }
}
