/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.validator;

import az.neuron.dev.rs.caremed.enums.Regex;
import az.neuron.dev.rs.caremed.form.FileWrapperForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Bayram
 */
public class FileWrapperFormValidator implements Validator{
    
    @Override
    public boolean supports(Class<?> type) {
        return FileWrapperForm.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        FileWrapperForm form = (FileWrapperForm) o;
        
        if( form.getFile()!= null && form.getFile().getSize()>0 && !form.getFile().getContentType().matches(Regex.FILE_CONTENT_TYPE)) {
            errors.reject("invalid file content type");
        }
        
        if(form.getFile().getSize()> 5 * 1024 * 1024) {
            errors.reject("file size is more than 5 mb");
        }
    }
}
