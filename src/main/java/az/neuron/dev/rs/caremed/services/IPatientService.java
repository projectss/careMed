/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.services;

import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.HamfullStuff;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.Patient;
import az.neuron.dev.rs.caremed.domain.PatientProphylaxlsesVaccination;
import az.neuron.dev.rs.caremed.domain.PatientSpecialNotes;
import az.neuron.dev.rs.caremed.domain.PersonAddress;
import az.neuron.dev.rs.caremed.domain.PersonContact;
import az.neuron.dev.rs.caremed.domain.PersonDocument;
import az.neuron.dev.rs.caremed.domain.PersonRelationship;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.DocumentForm;
import az.neuron.dev.rs.caremed.form.HamfullStuffForm;
import az.neuron.dev.rs.caremed.form.PatientForm;
import az.neuron.dev.rs.caremed.form.PatientHamfullStuffForm;
import az.neuron.dev.rs.caremed.form.PatientHistoryForm;
import az.neuron.dev.rs.caremed.form.PatientProphylaxlsesVaccinationForm;
import az.neuron.dev.rs.caremed.form.PatientSpecialNotesForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import az.neuron.dev.rs.caremed.search.PatientHistorySearchForm;
import az.neuron.dev.rs.caremed.search.PatientSearchForm;
import java.util.List;

/**
 *
 * @author Bayram
 */
public interface IPatientService {

    //----------------- add patient deteils ----------------------------------------------------------------------------------- 
    public OperationResponse addPatient(String token, PatientForm form, String imageFullpath, String imageOriginalName);

    public OperationResponse addPatientContact(String token, int personId, ContactForm form);

    public OperationResponse addPatientAddress(String token, int personId, AddressForm form);

    public OperationResponse addPatientRelationShip(String token, int personId, PersonRelationForm form);

    public OperationResponse addDocument(DocumentForm form, String token, int personId);

    public OperationResponse addPatientHamfullStuff(String token, HamfullStuffForm hamfullStuffForm);

    public OperationResponse addPatientProphylaxlsesVaccination(String token, PatientProphylaxlsesVaccinationForm form);

    public OperationResponse addPatientSpecialNotes(String token, PatientSpecialNotesForm form);

//-----------------------------------------------------------------------------------------------------------------------
    public DataTable getPatientList(String token, PatientSearchForm form);

    public Patient getPatientDetails(String token, int patientId);

    public List<PersonContact> getPatientContact(String token, int patientId); // patient kontakti

    public List<PersonAddress> getPatientAddress(String token, int patientId); // patient adressi

    public List<PersonRelationship> getPatientRealtionship(String token, int patientId); // gohumluq elaqesi

    public List<PersonDocument> getPatientDocument(String token, int personId); // dokumentleri

    public DataTable getPatientHistoryList(String token, int patientId, PatientHistorySearchForm form);

    public OperationResponse checkPatientPincode(String token, String pincode);

    public OperationResponse changePatientStatus(String token, int id, int statusId);

    public List<HamfullStuff> getHamfullStuff(String token);

    public List<PatientProphylaxlsesVaccination> getPatientProphylaxlsesVaccination(String token);

    public List<PatientSpecialNotes> getPatientSpecialNotes(String token);

    //----------------- edit service -------------------------------------------------------
    public OperationResponse editPatient(String token, PatientForm form);

    public OperationResponse editPatientContact(String token, int id, ContactForm form);

    public OperationResponse editPatientAddress(String token, AddressForm form);

    public OperationResponse editPatientRelationship(String token, int id, PersonRelationForm form);

    public OperationResponse editPatientDocument(String token, int id, DocumentForm form);

    public OperationResponse editPatientHamfullStuff(String token, int id, HamfullStuffForm form);

    public OperationResponse editPatientProphylaxlsesVaccination(String token, int id, PatientProphylaxlsesVaccinationForm form);

    public OperationResponse editPatientSpecialNotes(String token, PatientSpecialNotesForm form);
// -------------------------------------------------------------------------------------
    // ------------------- remove patients ------------------------------------------------------------------------

    public OperationResponse removePatient(String token, int patientId, PatientHistoryForm form);

    public OperationResponse removePatientAddress(String token, int id, int addressId);

    public OperationResponse removePatientDocument(String token, int id, int documentId);

    public OperationResponse removePatientRelationship(String token, int id, int relationshipId);

    public OperationResponse removePatientHamfullStuff(String token, int id);

    public OperationResponse removePatientProphylaxlsesVaccination(String token, int id);

    //-------------------------------------------------------------------------------------------------------------
}
