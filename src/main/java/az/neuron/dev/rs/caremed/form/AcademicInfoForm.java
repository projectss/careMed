/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class AcademicInfoForm {
private int id;
private int typeId;
private int academicInfoId;
private String startDate;

    AcademicInfoForm(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getAcademicInfoId() {
        return academicInfoId;
    }

    public void setAcademicInfoId(int academicInfoId) {
        this.academicInfoId = academicInfoId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @Override
    public String toString() {
        return "AcademicInfoForm{" + "id=" + id + ", typeId=" + typeId + ", academicInfId=" + academicInfoId + ", startDate=" + startDate + '}';
    }
    
    
}
