/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.validator;

import az.neuron.dev.rs.caremed.form.EmployeeForm;
import az.neuron.dev.rs.caremed.search.EmployeeSearchForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Bayram
 */
public class EmployeePaginationFormValidator implements Validator {
    
    @Override
    public boolean supports(Class<?> type) {
        return EmployeeForm.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        EmployeeSearchForm paginationForm = (EmployeeSearchForm) o;
        
        if(paginationForm.getOrderColumn() == null || paginationForm.getOrderColumn().trim().isEmpty() || !paginationForm.getOrderColumn().matches("^[a-zA-Z_]+$")) {
            paginationForm.setOrderColumn("id");
            errors.reject("Invalid order column name");
        }
        
        if(paginationForm.getOrderType() == null || paginationForm.getOrderType().trim().isEmpty() || (!paginationForm.getOrderType().equalsIgnoreCase("asc") && !paginationForm.getOrderType().equalsIgnoreCase("desc"))) {
            paginationForm.setOrderType("asc");
            errors.reject("Invalid order type");
        }
        
        if(paginationForm.getStart()== null || paginationForm.getStart() <= 0) {
            paginationForm.setStart(0);
            errors.reject("Invalid page");
        }
        
        if(paginationForm.getLength()== null || (paginationForm.getLength() <= 0 && paginationForm.getLength() > 100)) {
            paginationForm.setLength(10);
            errors.reject("Invalid page size");
        }
    }
}
