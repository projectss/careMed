/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.controller;

import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.User;
import az.neuron.dev.rs.caremed.enums.Constants;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.exception.CareMedException;
import az.neuron.dev.rs.caremed.form.BaseForm;
import az.neuron.dev.rs.caremed.form.ConsuptionForm;
import az.neuron.dev.rs.caremed.form.ServicesForm;
import az.neuron.dev.rs.caremed.search.ConsuptionSearchForm;
import az.neuron.dev.rs.caremed.search.ServiceSearchForm;
import az.neuron.dev.rs.caremed.validator.ServicesFormValidator;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/services", produces = MediaType.APPLICATION_JSON_VALUE)
public class ServicesController extends SkeletonController {
    
    private static final Logger log = Logger.getLogger(ServicesController.class);
    
    @PostMapping(value = "/add")
    protected OperationResponse addServices(ServicesForm form,
            BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, form.getToken());
            ServicesFormValidator validator = new ServicesFormValidator(Constants.VALIDATOR_ADD);
            validator.validate(form, result);
            
            log.info("/services/add [POST]. User: " + user + ", Form: " + form);
            
            if (result.hasErrors()){
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                throw new CareMedException("invalid params: " + result.getAllErrors());
            }
            
            operationResponse = operation.addServices(form.getToken(), form);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
            
        }
        
        return operationResponse;
    }
    
    @PostMapping
    protected OperationResponse getServiceList(ServiceSearchForm form,
            BindingResult result) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            
            User user = checkToken(operationResponse, form.getToken());
//
//            EmployeePaginationFormValidator validator = new EmployeePaginationFormValidator();
//            validator.validate(form, result);

            operationResponse.setData(operation.getServices(form));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/edit")
    protected OperationResponse editServices(BaseForm baseForm,
            ServicesForm form,
            @PathVariable("id") int id,
            BindingResult result
    ) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            
            ServicesFormValidator validator = new ServicesFormValidator(Constants.VALIDATOR_EDIT);
            validator.validate(form, result);
            log.info("services /{id:\\d+}/edit [POST]. Form: " + baseForm + ", service id: " + id + ", User: " + user);
            
            if (result.hasErrors()){
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                throw new CareMedException("invalid params: " + result.getAllErrors());
            }
            
            operationResponse = operation.editServices(baseForm.getToken(), form);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/remove")
    protected OperationResponse removeServices(BaseForm baseForm,
            @PathVariable("id") int id
    ) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = operation.removeServices(baseForm.getToken(), id);
            log.info("services /{id:\\d+}/remove [POST]. Form: " + baseForm + ", services id: " + id + ", User: " + user);
            
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/info")
    protected OperationResponse getSerivcesInfo(BaseForm form,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            User user = checkToken(operationResponse, form.getToken());
            
            operationResponse.setData(operation.getServicesDetails(form.getToken(), id));
            
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/serviceList")
    protected OperationResponse getSerivcesList(BaseForm form,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            
            User user = checkToken(operationResponse, form.getToken());
            
            operationResponse.setData(operation.getSetvicesList(form.getToken(), id));
            
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/element")
    protected OperationResponse getFormElementListByServiceId(BaseForm form,
            @PathVariable("id") int id) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(operationResponse, form.getToken());
            
            operationResponse.setData(operation.getFormElementList(form.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
        
    }
    
    @PostMapping(value = "/addConsuption")
    
    protected OperationResponse addConsuption(BaseForm baseForm, ConsuptionForm form, BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(operationResponse, form.getToken());
            operationResponse.setData(operation.addConsuption(baseForm.getToken(), form));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/getConsuption")
    
    protected OperationResponse getConsuption(BaseForm baseForm, ConsuptionSearchForm searchForm, BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(operationResponse, baseForm.getToken());
            
            operationResponse.setData(operation.getConsuption(baseForm.getToken(), searchForm));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/editConsuption")
    protected OperationResponse editConsuption(BaseForm baseForm, ConsuptionForm consuptionForm, @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(operationResponse, baseForm.getToken());
            
            operationResponse.setData(operation.editConsuption(baseForm.getToken(), consuptionForm));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/id/{note}/removeConsuption")
    protected OperationResponse removeConsuption(BaseForm baseForm, @PathVariable("id") int id, @PathVariable String note) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(operationResponse, baseForm.getToken());
            
            operationResponse.setData(operation.removeConsuption(baseForm.getToken(), id, note));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @GetMapping(value = "/{code}/checkCodeConsuption")
    protected OperationResponse checkCodeConsuption(BaseForm baseForm, @PathVariable("code") String code) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(operation.checkCodeConsuption(baseForm.getToken(), code));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
        
    }
    
}
