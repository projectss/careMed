/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.services;

import az.neuron.dev.rs.caremed.dao.PatientDao;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.HamfullStuff;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.Patient;
import az.neuron.dev.rs.caremed.domain.PatientProphylaxlsesVaccination;
import az.neuron.dev.rs.caremed.domain.PatientSpecialNotes;
import az.neuron.dev.rs.caremed.domain.PersonAddress;
import az.neuron.dev.rs.caremed.domain.PersonContact;
import az.neuron.dev.rs.caremed.domain.PersonDocument;
import az.neuron.dev.rs.caremed.domain.PersonRelationship;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.DocumentForm;
import az.neuron.dev.rs.caremed.form.HamfullStuffForm;
import az.neuron.dev.rs.caremed.form.PatientForm;
import az.neuron.dev.rs.caremed.form.PatientHamfullStuffForm;
import az.neuron.dev.rs.caremed.form.PatientHistoryForm;
import az.neuron.dev.rs.caremed.form.PatientProphylaxlsesVaccinationForm;
import az.neuron.dev.rs.caremed.form.PatientSpecialNotesForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import az.neuron.dev.rs.caremed.search.PatientHistorySearchForm;
import az.neuron.dev.rs.caremed.search.PatientSearchForm;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Bayram
 */
@Service
public class PatientService implements IPatientService {

    @Autowired
    private PatientDao patientDao;

    @Override
    public OperationResponse addPatient(String token, PatientForm form, String imageFullpath, String imageOriginalName) {
        return this.patientDao.addPatient(token, form, imageFullpath, imageOriginalName);
    }

    @Override
    public DataTable getPatientList(String token, PatientSearchForm form) {

        return this.patientDao.getPatientList(token, form);
    }

    @Override
    public Patient getPatientDetails(String token, int patientId) {
        return this.patientDao.getPatientDetails(token, patientId);
    }

    @Override
    public List<PersonContact> getPatientContact(String token, int patientId) {
        return this.patientDao.getPatientContact(token, patientId);
    }

    @Override
    public List<PersonAddress> getPatientAddress(String token, int patientId) {
        return this.patientDao.getPatientAddress(token, patientId);
    }

    @Override
    public List<PersonRelationship> getPatientRealtionship(String token, int patientId) {
        return this.patientDao.getPatientRealtionship(token, patientId);
    }

    @Override
    public List<PersonDocument> getPatientDocument(String token, int patientId) {
        return this.patientDao.getPatientDocument(token, patientId);
    }

    @Override
    public OperationResponse checkPatientPincode(String token, String pincode) {
        return this.patientDao.checkPatientPincode(token, pincode);
    }

    @Override
    public OperationResponse editPatient(String token, PatientForm form) {
        return this.patientDao.editPatient(token, form);
    }

    @Override
    public OperationResponse editPatientContact(String token, int id, ContactForm form) {
        return this.patientDao.editPatientContact(token, id, form);
    }

    @Override
    public OperationResponse editPatientAddress(String token, AddressForm form) {
        return this.patientDao.editPatientAddress(token, form);
    }

    @Override
    public OperationResponse editPatientRelationship(String token, int id, PersonRelationForm form) {
        return this.patientDao.editPatientRelationship(token, id, form);
    }

    @Override
    public OperationResponse editPatientDocument(String token, int id, DocumentForm form) {
        return this.patientDao.editPatientDocument(token, id, form);
    }

    @Override
    public OperationResponse addPatientContact(String token, int personId, ContactForm form) {
        return this.patientDao.addPatientContact(token, personId, form);
    }

    @Override
    public OperationResponse addPatientAddress(String token, int personId, AddressForm form) {
        return this.patientDao.addPatientAddress(token, personId, form);
    }

    @Override
    public OperationResponse addPatientRelationShip(String token, int personId, PersonRelationForm form) {
        return this.patientDao.addPatientRelationShip(token, personId, form);
    }

    @Override
    public OperationResponse removePatient(String token, int patientId, PatientHistoryForm form) {
        return this.patientDao.removePatient(token, patientId, form);
    }

    @Override
    public OperationResponse removePatientAddress(String token, int id, int addressId) {
        return this.patientDao.removePatientAddress(token, id, addressId);
    }

    @Override
    public OperationResponse removePatientDocument(String token, int id, int documentId) {
        return this.patientDao.removePatientDocument(token, id, documentId);
    }

    @Override
    public OperationResponse removePatientRelationship(String token, int id, int relationshipId) {
        return this.patientDao.removePatientRelationship(token, id, relationshipId);
    }

    @Override
    public OperationResponse addDocument(DocumentForm form, String token, int personId) {
        return this.patientDao.addDocument(form, token, personId);
    }

    @Override
    public DataTable getPatientHistoryList(String token, int patientId, PatientHistorySearchForm form) {
        return this.patientDao.getPatientHistoryList(token, patientId, form);
    }

    @Override
    public OperationResponse changePatientStatus(String token, int id, int statusId) {
        return this.patientDao.changePatientStatus(token, id, statusId);
    }

    @Override
    public OperationResponse addPatientHamfullStuff(String token, HamfullStuffForm hamfullStuffForm) {
        return this.patientDao.addPatientHamfullStuff(token, hamfullStuffForm);
    }

    @Override
    public OperationResponse editPatientHamfullStuff(String token, int id, HamfullStuffForm form) {
        return this.patientDao.editPatientHamfullStuff(token, id, form);
    }

    @Override
    public List<HamfullStuff> getHamfullStuff(String token) {
        return this.patientDao.getHamfullStuff(token);
    }

    @Override
    public OperationResponse addPatientProphylaxlsesVaccination(String token, PatientProphylaxlsesVaccinationForm form) {
        return this.patientDao.addPatientProphylaxlsesVaccination(token, form);
    }

    @Override
    public List<PatientProphylaxlsesVaccination> getPatientProphylaxlsesVaccination(String token) {
        return this.patientDao.getPatientProphylaxlsesVaccination(token);
    }

    @Override
    public OperationResponse editPatientProphylaxlsesVaccination(String token, int id, PatientProphylaxlsesVaccinationForm form) {
        return this.patientDao.editPatientProphylaxlsesVaccination(token, id, form);
    }

    @Override
    public OperationResponse addPatientSpecialNotes(String token, PatientSpecialNotesForm form) {
        return this.patientDao.addPatientSpecialNotes(token, form);
    }

    @Override
    public List<PatientSpecialNotes> getPatientSpecialNotes(String token) {
        return this.patientDao.getPatientSpecialNotes(token);
    }

    @Override
    public OperationResponse editPatientSpecialNotes(String token, PatientSpecialNotesForm form) {
        return this.patientDao.editPatientSpecialNotes(token, form);
    }

    @Override
    public OperationResponse removePatientHamfullStuff(String token, int id) {
        return this.patientDao.removePatientHamfullStuff(token, id);
    }

    @Override
    public OperationResponse removePatientProphylaxlsesVaccination(String token, int id) {
        return this.patientDao.removePatientProphylaxlsesVaccination(token, id);
    }

}
