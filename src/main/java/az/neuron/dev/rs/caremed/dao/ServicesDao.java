/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.db.DbConnect;
import az.neuron.dev.rs.caremed.domain.Consuption;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.DictionaryWrapper;
import az.neuron.dev.rs.caremed.domain.Employee;
import az.neuron.dev.rs.caremed.domain.FormElement;
import az.neuron.dev.rs.caremed.domain.MultilanguageString;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.Services;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.form.ConsuptionForm;
import az.neuron.dev.rs.caremed.form.ServicesForm;
import az.neuron.dev.rs.caremed.search.ConsuptionSearchForm;
import az.neuron.dev.rs.caremed.search.ServiceSearchForm;
import java.sql.CallableStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bayram
 */
@Repository

public class ServicesDao implements IServicesDao {

    private static final Logger log = Logger.getLogger(ServicesDao.class);

    @Autowired
    private DbConnect dbConnect;

    @Override
    public OperationResponse addServices(String token, ServicesForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            int serviceId = 0;
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.add_services (?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, form.getServiceTypeId());
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setInt(5, form.getOrgId());
                callableStatement.setString(6, form.getName());
                callableStatement.setString(7, form.getDuration());
                callableStatement.setInt(8, form.getCost());
                callableStatement.setInt(9, form.getDiscountedCost());
                callableStatement.setInt(10, form.getCurrencyId());
                callableStatement.setString(11, form.getServiceCode());
                callableStatement.setString(12, form.getNote());
                callableStatement.executeUpdate();

                serviceId = callableStatement.getInt(1);

                operationResponse.setData(serviceId);
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editServices(String token, ServicesForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatemnt = connection.prepareCall("{ call cm_caremed.edit_services (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) }")) {
                callableStatemnt.setString(1, token);
                callableStatemnt.setInt(2, form.getId());
                callableStatemnt.setInt(3, form.getTypeId());
                callableStatemnt.setInt(4, form.getOrgId());
                callableStatemnt.setString(5, form.getName());
                callableStatemnt.setString(6, form.getServiceCode());
                callableStatemnt.setString(7, form.getDuration());
                callableStatemnt.setInt(8, form.getCost());
                callableStatemnt.setInt(9, form.getDiscountedCost());
                callableStatemnt.setInt(10, form.getCurrencyId());
                callableStatemnt.setString(11, form.getNote());
                callableStatemnt.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public DataTable getServices(ServiceSearchForm searchForm) {
        DataTable dataTable = new DataTable();
        List<Services> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.get_services_list (?,?,?,?,?,?,?)}")) {

                callableStatement.setString(1, searchForm.getToken());

                if (searchForm.getServiceTypeId() != null && searchForm.getServiceTypeId() > 0) {
                    callableStatement.setInt(2, searchForm.getServiceTypeId());
                } else {
                    callableStatement.setInt(2, 0);
                }

                if (searchForm.getName() != null && searchForm.getName().isEmpty()) {
                    callableStatement.setString(3, searchForm.getName());
                } else {
                    callableStatement.setString(3, null);
                }

                callableStatement.setInt(4, searchForm.getStart());
                callableStatement.setInt(5, searchForm.getLength());
                callableStatement.registerOutParameter(6, Types.INTEGER);
                callableStatement.registerOutParameter(7, Types.OTHER);

                callableStatement.execute();

                ResultSet resultSet = (ResultSet) callableStatement.getObject(7);

                while (resultSet.next()) {
                    list.add(new Services(
                            resultSet.getInt("r"),
                            resultSet.getInt("id"),
                            new DictionaryWrapper(resultSet.getInt("service_type_id"),
                                    new MultilanguageString(resultSet.getString("service_name_az"),
                                            resultSet.getString("service_name_en"),
                                            resultSet.getString("service_name_ru"))),
                            resultSet.getString("name"),
                            resultSet.getString("duration"),
                            resultSet.getString("cost"),
                            new DictionaryWrapper(resultSet.getInt("currency_id"),
                                    new MultilanguageString(resultSet.getString("currency_name_az"),
                                            resultSet.getString("currency_name_en"),
                                            resultSet.getString("currency_name_ru"))), resultSet.getString("service_code"), resultSet.getString("note")));
                }
                dataTable.setDataList(list);
                dataTable.setTotalCount(callableStatement.getInt(6));
            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return dataTable;
    }

    @Override
    public OperationResponse removeServices(String token, int id) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_services (?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public Services getServicesDetails(String token, int id) {

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_services s where s.id = ?")) {
                preparedStatement.setInt(1, id);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {

                        return new Services(0,
                                resultSet.getInt("id"),
                                new DictionaryWrapper(resultSet.getInt("service_type_id"),
                                        new MultilanguageString(resultSet.getString("service_name_az"),
                                                resultSet.getString("service_name_en"),
                                                resultSet.getString("service_name_ru"))),
                                resultSet.getString("name"),
                                resultSet.getString("duration"),
                                resultSet.getString("cost"),
                                new DictionaryWrapper(resultSet.getInt("currency_id"),
                                        new MultilanguageString(resultSet.getString("currency_name_az"),
                                                resultSet.getString("currency_name_en"),
                                                resultSet.getString("currency_name_ru"))), resultSet.getString("service_code"), resultSet.getString("note"));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public List<Services> getSetvicesList(String token, int servicesId) {
        List<Services> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_services_list where service_type_id = ? ")) {
                preparedStatement.setInt(1, servicesId);

                try (ResultSet rs = preparedStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new Services(rs.getInt("id"), null,
                                rs.getString("service_name"),
                                rs.getString("cost"),
                                new DictionaryWrapper(rs.getInt("currency_id"),
                                        new MultilanguageString(rs.getString("currency_name_az"),
                                                rs.getString("currency_name_en"),
                                                rs.getString("currency_name_ru")))));
                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<FormElement> getFormElementList(String token, int servicesId) {
        List<FormElement> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_form_element v where v.service_id = ?")) {
                preparedStatement.setInt(1, servicesId);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        list.add(new FormElement(resultSet.getInt("id"),
                                new Services(resultSet.getInt("service_id"),
                                        new DictionaryWrapper(resultSet.getInt("service_type"),
                                                new MultilanguageString(resultSet.getString("service_type_az"),
                                                        resultSet.getString("service_type_en"),
                                                        resultSet.getString("service_type_ru"))),
                                        resultSet.getString("service_name"),
                                        resultSet.getString("service_duration"),
                                        resultSet.getString("service_cost"),
                                        new DictionaryWrapper(resultSet.getInt("service_currency_id"),
                                                new MultilanguageString(resultSet.getString("service_currency_az"),
                                                        resultSet.getString("service_currency_en"),
                                                        resultSet.getString("service_currency_ru"))),
                                        resultSet.getString("service_code"),
                                        resultSet.getString("service_note")),
                                new DictionaryWrapper(resultSet.getInt("html_type_id"),
                                        new MultilanguageString(resultSet.getString("html_type_az"),
                                                resultSet.getString("html_type_en"),
                                                resultSet.getString("html_type_ru"))),
                                resultSet.getString("i18n"),
                                new DictionaryWrapper(resultSet.getInt("select_id"),
                                        new MultilanguageString(resultSet.getString("select_az"),
                                                resultSet.getString("select_en"),
                                                resultSet.getString("select_ru"))),
                                resultSet.getString("attr_class"),
                                resultSet.getString("input_type")));
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    @Override
    public OperationResponse addConsuption(String token, ConsuptionForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            int consuptionId = 0;
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.add_consuption (?, ?, ?, ?, ?, ?, ?, ?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setString(3, form.getCode());
                callableStatement.setString(4, form.getName());
                callableStatement.setInt(5, form.getType());
                callableStatement.setInt(6, form.getUnit());
                callableStatement.setInt(7, form.getCost());
                callableStatement.setInt(8, form.getCurrency());
                callableStatement.setString(9, form.getBarcodeNo());
                callableStatement.setString(10, form.getNote());
                callableStatement.execute();

                consuptionId = callableStatement.getInt(1);

                operationResponse.setData(consuptionId);
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public DataTable getConsuption(String token, ConsuptionSearchForm consuptionSearchForm) {
        DataTable dataTable = new DataTable();
        List<Consuption> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.get_consuption(?, ?, ?, ?, ?, ?, ?, ?)}")) {
                callableStatement.setString(1, token);
                if (consuptionSearchForm.getCode() != null && consuptionSearchForm.getCode().isEmpty()) {
                    callableStatement.setString(2, consuptionSearchForm.getCode());
                } else {
                    callableStatement.setString(2, null);
                }

                if (consuptionSearchForm.getType() != null && consuptionSearchForm.getType() > 0) {
                    callableStatement.setInt(3, consuptionSearchForm.getType());
                } else {

                    callableStatement.setInt(3, 0);
                }

                if (consuptionSearchForm.getUnit() != null && consuptionSearchForm.getUnit() > 0) {
                    callableStatement.setInt(4, consuptionSearchForm.getUnit());
                } else {
                    callableStatement.setInt(4, 0);
                }

                callableStatement.setInt(5, consuptionSearchForm.getStart());
                callableStatement.setInt(6, consuptionSearchForm.getLength());
                callableStatement.registerOutParameter(7, Types.INTEGER);
                callableStatement.registerOutParameter(8, Types.OTHER);
                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(8);

                while (rs.next()) {

                    list.add(new Consuption(rs.getInt("id"),
                            rs.getString("code"), rs.getString("name"), new DictionaryWrapper(rs.getInt("type"),
                            new MultilanguageString(rs.getString("type_name_az"),
                                    rs.getString("type_name_en"),
                                    rs.getString("type_name_ru"))),
                            new DictionaryWrapper(rs.getInt("unit"),
                                    new MultilanguageString(rs.getString("unit_name_az"),
                                            rs.getString("unit_name_en"),
                                            rs.getString("unit_name_ru"))), rs.getInt("cost"),
                            new DictionaryWrapper(rs.getInt("currency"),
                                    new MultilanguageString(rs.getString("currency_name_az"),
                                            rs.getString("currency_name_en"),
                                            rs.getString("currency_name_ru"))),
                            rs.getString("barcode_no"), rs.getString("note")));

                }
                dataTable.setDataList(list);
                dataTable.setTotalCount(callableStatement.getInt(7));
            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return dataTable;
    }

    @Override
    public OperationResponse editConsuption(String token, ConsuptionForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_consuption (?, ?, ?, ?, ?, ?, ?, ?, ?, ? )}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setString(3, form.getCode());
                callableStatement.setString(4, form.getName());
                callableStatement.setInt(5, form.getType());
                callableStatement.setInt(6, form.getUnit());
                callableStatement.setInt(7, form.getCost());
                callableStatement.setInt(8, form.getCurrency());
                callableStatement.setString(9, form.getBarcodeNo());
                callableStatement.setString(10, form.getNote());
                
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);
            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeConsuption(String token, int id, String note) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.remove_consuption(?, ?, ?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setString(3, note);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);
            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse checkCodeConsuption(String token, String code) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR, null, null);
        List<Consuption> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_consuption  c where lower(c.code) = lower(?)")) {
                preparedStatement.setString(1, code);

                try (ResultSet rs = (ResultSet) preparedStatement.executeQuery()) {
                    while (rs.next()) {

                        list.add(new Consuption(rs.getInt("id"),
                                rs.getString("code"), rs.getString("name"), new DictionaryWrapper(rs.getInt("type"),
                                new MultilanguageString(rs.getString("type_name_az"),
                                        rs.getString("type_name_en"),
                                        rs.getString("type_name_ru"))),
                                new DictionaryWrapper(rs.getInt("unit"),
                                        new MultilanguageString(rs.getString("unit_name_az"),
                                                rs.getString("unit_name_en"),
                                                rs.getString("unit_name_ru"))), rs.getInt("cost"),
                                new DictionaryWrapper(rs.getInt("currency"),
                                        new MultilanguageString(rs.getString("currency_name_az"),
                                                rs.getString("currency_name_en"),
                                                rs.getString("currency_name_ru"))),
                                rs.getString("barcode_no"), rs.getString("note")));

                        operationResponse.setData(list);
                        operationResponse.setCode(ResultCode.OK);
                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

}
