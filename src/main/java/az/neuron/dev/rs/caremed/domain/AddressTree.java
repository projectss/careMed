/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

import java.util.Date;

/**
 *
 * @author Bayram
 */
public class AddressTree {

    private int id;
    private DictionaryWrapper type;
    private MultilanguageString name;
    private String formula;
    private AddressTree parent;
    private int nodeLevel;
    private Date createDate;
    private Date lastUpdate;
    private boolean active;

    AddressTree() {

    }

    @Override
    public String toString() {
        return "AddressTree{" + "id=" + id + ", type=" + type + ", name=" + name + ", formula=" + formula + ", parent=" + parent + ", nodeLevel=" + nodeLevel + ", createDate=" + createDate + ", lastUpdate=" + lastUpdate + ", active=" + active + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getType() {
        return type;
    }

    public void setType(DictionaryWrapper type) {
        this.type = type;
    }

    public MultilanguageString getName() {
        return name;
    }

    public void setName(MultilanguageString name) {
        this.name = name;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public AddressTree getParent() {
        return parent;
    }

    public void setParent(AddressTree parent) {
        this.parent = parent;
    }

    public int getNodeLevel() {
        return nodeLevel;
    }

    public void setNodeLevel(int nodeLevel) {
        this.nodeLevel = nodeLevel;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
