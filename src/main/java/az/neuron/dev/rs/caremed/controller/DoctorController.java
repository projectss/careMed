/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.controller;

import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.User;
import az.neuron.dev.rs.caremed.enums.Constants;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.exception.CareMedException;
import az.neuron.dev.rs.caremed.form.BaseForm;
import az.neuron.dev.rs.caremed.form.CheckPasientForm;
import az.neuron.dev.rs.caremed.form.OperationForm;
import az.neuron.dev.rs.caremed.search.PatientSearchByDoctor;
import az.neuron.dev.rs.caremed.validator.CheckPasientFormValidator;
import az.neuron.dev.rs.caremed.validator.FileWrapperFormValidator;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 *
 * @author Bayram
 */
@RestController
@RequestMapping(value = "/doctor", produces = MediaType.APPLICATION_JSON_VALUE)
public class DoctorController extends SkeletonController {

    private static final Logger log = Logger.getLogger(DoctorController.class);

    @PostMapping(value = "/patients")
    protected OperationResponse getPatientListByDoctor(BaseForm form, PatientSearchByDoctor searchByDoctor) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());

            operationResponse.setData(doctor.getPatientListByDoctor(form.getToken(), searchByDoctor));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }

    @PostMapping(value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse addCheckUp(@RequestPart(name = "doctor", required = false) CheckPasientForm form,
            BaseForm baseForm,
            BindingResult result) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, baseForm.getToken());

            CheckPasientFormValidator validator = new CheckPasientFormValidator(Constants.VALIDATOR_ADD);
            validator.validate(form, result);

            log.info("/doctor/add [POST]. User: " + user + ", Form: " + form + ", BaseForm: " + baseForm);

            if (result.hasErrors()) {
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                throw new CareMedException("Invalid params: " + result.getAllErrors());
            }

            operationResponse = doctor.addCheckUpPatient(baseForm.getToken(), form);

        } catch (Exception e) {

            log.error(e.getMessage(), e);

        }

        return operationResponse;
    }

    @PostMapping(value = "/addSubServicesByDoctor")
    protected OperationResponse addSubServicesByDoctor(BaseForm baseForm, OperationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(doctor.addSubServicesByDoctor(baseForm.getToken(), form));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @GetMapping(value = "/{id:\\d+}/getPatientForwardByDoctor")
    protected OperationResponse getPatientForwardByDoctor(BaseForm baseForm, @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(doctor.getPatientForwardByDoctor(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
}
