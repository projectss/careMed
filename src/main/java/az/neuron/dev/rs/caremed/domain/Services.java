/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class Services {

    private int number;
    private int id;
    private DictionaryWrapper serviceTypeId;
    private String name;
    private String duration;
    private String cost;
    private DictionaryWrapper currencyId;
    private String serviceCode;
    private String note;

    public Services() {
    }

    public Services(int number, int id, DictionaryWrapper serviceTypeId,  String name, String duration, String cost, DictionaryWrapper currencyId,String serviceCode, String note) {
        this.number = number;
        this.id = id;
        this.serviceTypeId = serviceTypeId;
        this.name = name;
        this.duration = duration;
        this.cost = cost;
        this.currencyId = currencyId;
        this.serviceCode = serviceCode;
        this.note = note;
    }

    public Services(int id, DictionaryWrapper serviceTypeId, String name, String cost, DictionaryWrapper currencyId) {
        this.id = id;
        this.serviceTypeId = serviceTypeId;
        this.name = name;
        this.cost = cost;
        this.currencyId = currencyId;
    }

    public Services(int id, DictionaryWrapper serviceTypeId, String name, String duration, String cost, DictionaryWrapper currencyId, String serviceCode, String note) {
        this.id = id;
        this.serviceTypeId = serviceTypeId;
        this.name = name;
        this.duration = duration;
        this.cost = cost;
        this.currencyId = currencyId;
        this.serviceCode = serviceCode;
        this.note = note;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(DictionaryWrapper serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public DictionaryWrapper getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(DictionaryWrapper currencyId) {
        this.currencyId = currencyId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    
    
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Services{" + "number=" + number + ", id=" + id + ", serviceTypeId=" + serviceTypeId + ", name=" + name + ", duration=" + duration + ", cost=" + cost + ", currencyId=" + currencyId + ", serviceCode=" + serviceCode + ", note=" + note + '}';
    }


}
