/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class User {
    private int id;
    private UserAccount account;
    private String name;
    private String surname;
    private String patronymic;
    private String birthdate;
    private DictionaryWrapper gender;
    private String pin;
    private FileWrapper image;
    private boolean sessionActive;

    public User() {
    }

    public User(UserAccount account, String name, String surname, String patronymic) {
        this.account = account;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }
    
    public User(int id, UserAccount account, String name, String surname, String patronymic, String birthdate, DictionaryWrapper gender, String pin, FileWrapper image) {
        this.id = id;
        this.account = account;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.birthdate = birthdate;
        this.gender = gender;
        this.pin = pin;
        this.image = image;
    }

    public User(int id, UserAccount account, String name, String surname, String patronymic, String birthdate, DictionaryWrapper gender, String pin, FileWrapper image, boolean sessionActive) {
        this.id = id;
        this.account = account;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.birthdate = birthdate;
        this.gender = gender;
        this.pin = pin;
        this.image = image;
        this.sessionActive = sessionActive;
    }

    public FileWrapper getImage() {
        return image;
    }

    public void setImage(FileWrapper image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserAccount getAccount() {
        return account;
    }

    public void setAccount(UserAccount account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public DictionaryWrapper getGender() {
        return gender;
    }

    public void setGender(DictionaryWrapper gender) {
        this.gender = gender;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public boolean isSessionActive() {
        return sessionActive;
    }

    public void setSessionActive(boolean sessionActive) {
        this.sessionActive = sessionActive;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", account=" + account + ", name=" + name + ", surname=" + surname + ", patronymic=" + patronymic + ", birthdate=" + birthdate + ", gender=" + gender + ", pin=" + pin + ", image=" + image + ", sessionActive=" + sessionActive + '}';
    }
}
