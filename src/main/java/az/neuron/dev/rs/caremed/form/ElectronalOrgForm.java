/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class ElectronalOrgForm extends BaseForm {
    private int id;
    private int  personId;
    private String orgName;
    private String address;
    private String positionName;
    private String startDate;
    private String endDate;
    private String note;
    
    ElectronalOrgForm(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ElectronalOrgForm{" + "id=" + id + ", personId=" + personId + ", orgName=" + orgName + ", address=" + address + ", positionName=" + positionName + ", startDate=" + startDate + ", endDate=" + endDate + ", note=" + note + '}';
    }

}
