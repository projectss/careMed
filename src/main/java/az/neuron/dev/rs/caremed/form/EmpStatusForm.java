/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class EmpStatusForm  extends BaseForm{
    private int employeeId;
    private int statusId;

    public EmpStatusForm() {
    }

    
    
    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    @Override
    public String toString() {
        return "EmpStatusForm{" + "employeeId=" + employeeId + ", statusId=" + statusId + '}';
    }
    
    
    
}
