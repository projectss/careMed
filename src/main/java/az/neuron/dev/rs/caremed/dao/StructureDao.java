/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.db.DbConnect;
import az.neuron.dev.rs.caremed.domain.User;
import az.neuron.dev.rs.caremed.util.RowFetcher;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bayram
 */
@Repository
public class StructureDao implements IStructureDao{
 private static final Logger log = Logger.getLogger(StructureDao.class);

    @Autowired
    private DbConnect dbConnect;

    @Override
    public User checkToken(String token) {
        try(Connection connection= dbConnect.getConnection()){
            connection.setAutoCommit(false);
            try(CallableStatement callableStatement=connection.prepareCall("{? = call cm_security.check_token(?)}")){
            callableStatement.registerOutParameter(1, Types.OTHER);
            callableStatement.setString(2, token);
            callableStatement.execute();
                try(ResultSet resutlset=(ResultSet)callableStatement.getObject(1)) {
                    if (resutlset.next()) {
                        return RowFetcher.fetchUser(resutlset);
                    }
                }
            }
        }catch(Exception e){
        
        log.error(e.getMessage(), e);
        }
      
        
        
        return null;
    }
    
}
