/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.db.DbConnect;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.DictionaryWrapper;
import az.neuron.dev.rs.caremed.domain.Employee;
import az.neuron.dev.rs.caremed.domain.FileWrapper;
import az.neuron.dev.rs.caremed.domain.HamfullStuff;
import az.neuron.dev.rs.caremed.domain.MultilanguageString;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.Patient;
import az.neuron.dev.rs.caremed.domain.PatientHistory;
import az.neuron.dev.rs.caremed.domain.PatientProphylaxlsesVaccination;
import az.neuron.dev.rs.caremed.domain.PatientSpecialNotes;
import az.neuron.dev.rs.caremed.domain.PersonAddress;
import az.neuron.dev.rs.caremed.domain.PersonContact;
import az.neuron.dev.rs.caremed.domain.PersonDocument;
import az.neuron.dev.rs.caremed.domain.PersonRelationship;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.exception.CareMedException;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.DocumentForm;
import az.neuron.dev.rs.caremed.form.HamfullStuffForm;
import az.neuron.dev.rs.caremed.form.PatientForm;
import az.neuron.dev.rs.caremed.form.PatientHamfullStuffForm;
import az.neuron.dev.rs.caremed.form.PatientHistoryForm;
import az.neuron.dev.rs.caremed.form.PatientProphylaxlsesVaccinationForm;
import az.neuron.dev.rs.caremed.form.PatientSpecialNotesForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import az.neuron.dev.rs.caremed.search.PatientHistorySearchForm;
import az.neuron.dev.rs.caremed.search.PatientSearchForm;
import az.neuron.dev.rs.caremed.util.Converter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bayram
 */
@Repository
@Log4j
public class PatientDao implements IPatientDao {

    @Autowired
    private DbConnect dbConnect;

    @Override
    public OperationResponse addPatient(String token, PatientForm form, String imageFullpath, String imageOriginalName) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        int personId = 0;

        try (Connection connection = dbConnect.getConnection()) {

            try (CallableStatement callableStatemnt = connection.prepareCall("{? = call cm_caremed.add_patient(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)} ")) {
                callableStatemnt.registerOutParameter(1, Types.INTEGER);
                callableStatemnt.setString(2, form.getToken());
                callableStatemnt.setString(3, form.getFistName());
                callableStatemnt.setString(4, form.getLastName());
                callableStatemnt.setString(5, form.getMiddleName());
                callableStatemnt.setInt(6, form.getCitizenshipId());
                callableStatemnt.setInt(7, form.getMaritalStatusId());
                callableStatemnt.setInt(8, form.getMillitaryServiceId());
                callableStatemnt.setInt(9, form.getNationalityId());
                callableStatemnt.setInt(10, form.getSocialStatusId());
                callableStatemnt.setString(11, form.getBirthdate());
                callableStatemnt.setString(12, form.getPincode());
                callableStatemnt.setString(13, form.getSeriya());
                callableStatemnt.setInt(14, form.getBloodGroupId());
                callableStatemnt.setInt(15, form.getSupplyOrg());
                callableStatemnt.setInt(16, form.getGenderId());

                if (imageFullpath == null || imageFullpath.trim().isEmpty()) {
                    callableStatemnt.setNull(17, Types.NULL);
                } else {
                    callableStatemnt.setString(17, imageFullpath);

                }

                callableStatemnt.setString(18, imageOriginalName);
                callableStatemnt.setString(19, form.getPatientDocNum());
                callableStatemnt.setString(20, form.getNote());
                callableStatemnt.setInt(21, form.getChildCount());
                callableStatemnt.setInt(22, form.getSvSeriya());
                callableStatemnt.setInt(23, form.getTypeId());
                callableStatemnt.setInt(24, form.getCompanyNameId());
                callableStatemnt.setInt(25, form.getPositionId());
                callableStatemnt.setInt(26, form.getOrgId());
                callableStatemnt.executeUpdate();

                personId = callableStatemnt.getInt(1);

                operationResponse.setData(personId);
                operationResponse.setCode(ResultCode.OK);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addDocument(DocumentForm form, String token, int personId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.add_document(?,?,?,?,?,?,?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setString(5, form.getNumber());
                callableStatement.setString(6, form.getSerial());
                callableStatement.setString(7, form.getStartDate());
                callableStatement.setString(8, form.getEndDate());
                callableStatement.executeUpdate();

                int docId = callableStatement.getInt(1);

                operationResponse.setData(docId);
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse addDocFile(DocumentForm form, String token) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.add_doc_file(?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setArray(3, connection.createArrayOf("text", Converter.toPlgFileWrapper(form.getUploads())));
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;

    }

    @Override
    public DataTable getPatientList(String token, PatientSearchForm form) {
        DataTable dataTable = new DataTable();
        List<Patient> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.get_list_patient(?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, token);

                if (form.getPatientDocNum() != null && form.getPatientDocNum().length() > 0) {
                    callableStatement.setString(2, form.getPatientDocNum());
                } else {
                    callableStatement.setString(2, null);
                }

                if (form.getGenderId() != null && form.getGenderId() > 0) {
                    callableStatement.setInt(3, form.getGenderId());
                } else {
                    callableStatement.setInt(3, 0);
                }
                callableStatement.setInt(4, form.getStart());
                callableStatement.setInt(5, form.getLength());
                callableStatement.registerOutParameter(6, Types.INTEGER);
                callableStatement.registerOutParameter(7, Types.OTHER);
                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(7);

                while (rs.next()) {

                    list.add(new Patient(rs.getInt("id"), rs.getInt("r"), rs.getString("patient_doc_num"),
                            rs.getString("registration_date"),
                            new DictionaryWrapper(rs.getInt("company_name_id"),
                                    new MultilanguageString(rs.getString("company_name_az"),
                                            rs.getString("company_name_en"),
                                            rs.getString("company_name_ru"))),
                            new DictionaryWrapper(rs.getInt("position_id"),
                                    new MultilanguageString(rs.getString("position_name_az"),
                                            rs.getString("position_name_en"),
                                            rs.getString("position_name_ru"))),
                            new DictionaryWrapper(rs.getInt("type_id"),
                                    new MultilanguageString(rs.getString("type_name_az"),
                                            rs.getString("type_name_en"),
                                            rs.getString("type_name_ru"))),
                            rs.getInt("person_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("middle_name"),
                            new DictionaryWrapper(rs.getInt("gender_id"),
                                    new MultilanguageString(rs.getString("gender_name_az"),
                                            rs.getString("gender_name_en"),
                                            rs.getString("gender_name_ru"))), rs.getString("birthdate")));

                }
                dataTable.setDataList(list);
                dataTable.setTotalCount(callableStatement.getInt(6));

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return dataTable;
    }

    @Override
    public Patient getPatientDetails(String token, int patientId) {
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.get_patient_details(?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.OTHER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, patientId);                           
                callableStatement.execute();
                try (ResultSet rs = (ResultSet) callableStatement.getObject(1)) {
                    while (rs.next()) {

                        return new Patient(
                                rs.getString("patient_doc_num"),
                                rs.getString("registration_date"),
                                rs.getString("patient_note"),
                                new DictionaryWrapper(rs.getInt("company_name_id"),
                                        new MultilanguageString(rs.getString("company_name_az"),
                                                rs.getString("company_name_en"),
                                                rs.getString("company_name_ru"))),
                                new DictionaryWrapper(rs.getInt("position_id"),
                                        new MultilanguageString(rs.getString("position_name_az"),
                                                rs.getString("position_name_en"),
                                                rs.getString("position_name_ru"))),
                                new DictionaryWrapper(rs.getInt("type_id"),
                                        new MultilanguageString(rs.getString("type_name_az"),
                                                rs.getString("type_name_en"),
                                                rs.getString("type_name_ru"))),
                                rs.getInt("person_id"),
                                rs.getString("first_name"),
                                rs.getString("last_name"),
                                rs.getString("middle_name"),
                                new DictionaryWrapper(rs.getInt("citizenship_id"),
                                        new MultilanguageString(rs.getString("citizenship_name_az"),
                                                rs.getString("citizenship_name_en"),
                                                rs.getString("citizenship_name_ru"))),
                                new DictionaryWrapper(rs.getInt("marital_status_id"),
                                        new MultilanguageString(rs.getString("marital_status_name_az"),
                                                rs.getString("marital_status_name_en"),
                                                rs.getString("marital_status_name_ru"))),
                                new DictionaryWrapper(rs.getInt("millitary_service_id"),
                                        new MultilanguageString(rs.getString("millitary_service_name_az"),
                                                rs.getString("millitary_service_name_en"),
                                                rs.getString("millitary_service_name_ru"))),
                                new DictionaryWrapper(rs.getInt("nationality_id"),
                                        new MultilanguageString(rs.getString("nationality_name_az"),
                                                rs.getString("nationality_name_en"),
                                                rs.getString("nationality_name_ru"))),
                                new DictionaryWrapper(rs.getInt("social_status_id"),
                                        new MultilanguageString(rs.getString("social_status_name_az"),
                                                rs.getString("social_status_name_en"),
                                                rs.getString("social_status_name_ru"))),
                                rs.getString("birthdate"),
                                rs.getString("pincode"),
                                new DictionaryWrapper(rs.getInt("sv_seriya"),
                                        new MultilanguageString(rs.getString("sv_seriya_name_az"),
                                                rs.getString("sv_seriya_name_az"),
                                                rs.getString("sv_seriya_name_az"))),
                                rs.getString("seriya"),
                                new DictionaryWrapper(rs.getInt("blood_group_id"),
                                        new MultilanguageString(rs.getString("blood_group_az"),
                                                rs.getString("blood_group_en"),
                                                rs.getString("blood_group_ru"))),
                                new DictionaryWrapper(rs.getInt("supply_organization"),
                                        new MultilanguageString(rs.getString("supply_organization_az"),
                                                rs.getString("supply_organization_en"),
                                                rs.getString("supply_organization_ru"))),
                                null,
                                rs.getInt("height"),
                                new DictionaryWrapper(rs.getInt("gender_id"),
                                        new MultilanguageString(rs.getString("gender_name_az"),
                                                rs.getString("gender_name_en"),
                                                rs.getString("gender_name_ru"))),
                                rs.getInt("photo_file_id"),
                                rs.getInt("child_count"),
                                this.getPatientAddress(token, patientId),
                                this.getPatientContact(token, patientId),
                                this.getPatientDocument(token, patientId),
                                this.getPatientRealtionship(token, patientId));

                    }
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public List<PersonContact> getPatientContact(String token, int patientId) {
        List<PersonContact> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatemnt = connection.prepareStatement("select * from cm_caremed.v_person_contact p  where  p.person_id = ?")) {
                preparedStatemnt.setInt(1, patientId);
                try (ResultSet rs = preparedStatemnt.executeQuery()) {
                    while (rs.next()) {

                        list.add(new PersonContact(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("type_id"),
                                        new MultilanguageString(rs.getString("contact_name_az"),
                                                rs.getString("contact_name_en"),
                                                rs.getString("contact_name_ru"))),
                                rs.getString("contact")));

                    }

                }

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<PersonAddress> getPatientAddress(String token, int patientId) {
        List<PersonAddress> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatemnt = connection.prepareStatement("select * from cm_caremed.v_person_address p where p.person_id = ?")) {
                preparedStatemnt.setInt(1, patientId);
                try (ResultSet rs = preparedStatemnt.executeQuery()) {
                    while (rs.next()) {

                        list.add(new PersonAddress(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("address_id"),
                                        new MultilanguageString(rs.getString("address_name_az"),
                                                rs.getString("address_name_en"),
                                                rs.getString("address_name_ru"))),
                                new DictionaryWrapper(rs.getInt("type_id"),
                                        new MultilanguageString(rs.getString("type_name_az"),
                                                rs.getString("type_name_en"),
                                                rs.getString("type_name_ru"))),
                                rs.getString("address")));

                    }

                }

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<PersonRelationship> getPatientRealtionship(String token, int patientId) {
        List<PersonRelationship> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_person_relationship r  where r.person_id=?")) {
                prepareStatement.setInt(1, patientId);
                try (ResultSet rs = prepareStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new PersonRelationship(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("relationship_id"),
                                        new MultilanguageString(rs.getString("relationship_name_az"),
                                                rs.getString("relationship_name_en"),
                                                rs.getString("relationship_name_ru"))),
                                rs.getString("fullname"), rs.getString("birthdate"), rs.getString("contact_phone"), rs.getString("contact_home"), rs.getString("address"), rs.getString("note")));
                    }

                }

            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<PersonDocument> getPatientDocument(String token, int patientId) {
        List<PersonDocument> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_person_document d where d.person_id=?")) {
                prepareStatement.setInt(1, patientId);
                try (ResultSet rs = prepareStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new PersonDocument(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("type_id"),
                                        new MultilanguageString(rs.getString("type_name_az"),
                                                rs.getString("type_name_en"),
                                                rs.getString("type_name_ru"))),
                                rs.getString("doc_num"),
                                rs.getString("doc_serial"),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("note"),
                                this.getDocumentFiles(rs.getInt("id"))));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);

        }

        return list;
    }

    @Override
    public List<FileWrapper> getDocumentFiles(int docId) {
        List<FileWrapper> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_doc_files f  where f.doc_id = ?")) {
                prepareStatement.setInt(1, docId);

                try (ResultSet rs = prepareStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new FileWrapper(rs.getInt("id"),
                                rs.getString("original_name"),
                                rs.getString("path")));
                    }

                }
            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public OperationResponse checkPatientDocNum(String token, String patientDocNum) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.check_patinet_doc_num (?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setString(2, patientDocNum);

                try (ResultSet rs = callableStatement.executeQuery()) {

                    if (rs.next()) {

                        operationResponse.setData(rs.getInt("patient_id"));
                        operationResponse.setCode(ResultCode.OK);

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse checkPatientPincode(String token, String pincode) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select count(*) count_patient  from cm_common.persons p \n"
                    + "   join cm_caremed.patient a on p.id=a.patient_id\n"
                    + "   where lower(p.pincode) = lower(?) and p.active = 1")) {

                preparedStatement.setString(1, pincode);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        int count = resultSet.getInt("count_patient");

                        operationResponse.setData(count);
                        operationResponse.setCode(ResultCode.OK);
                        return operationResponse;
                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editPatient(String token, PatientForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            System.out.println("dsds  ");
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_patient(?,?,?,?,?,?) }")) {
                callableStatement.setInt(1, form.getId());
                callableStatement.setString(2, token);
                callableStatement.setInt(3, form.getTypeId());
                callableStatement.setString(4, form.getPatientDocNum());
                callableStatement.setInt(5, form.getCompanyNameId());

                callableStatement.setInt(6, form.getPositionId());

                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse removePatient(String token, int patientId, PatientHistoryForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.remove_patient(?,?,?,?,?)}")) {

                callableStatement.setString(1, token);
                callableStatement.setInt(2, patientId);
                callableStatement.setInt(3, form.getReasonId());
                callableStatement.setString(4, form.getNote());
                callableStatement.setString(5, form.getEndDate());

                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editPatientContact(String token, int id, ContactForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {

            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.edit_person_contact (?,?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setInt(3, form.getTypeId());
                callableStatement.setString(4, form.getContact());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editPatientAddress(String token, AddressForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_person_address (?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setInt(3, form.getAddressId());
                callableStatement.setString(4, form.getAddress());
                callableStatement.setInt(5, form.getTypeId());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editPatientRelationship(String token, int id, PersonRelationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_person_relationship (?,?,?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setInt(3, form.getRelationshipId());
                callableStatement.setString(4, form.getFullName());
                callableStatement.setString(5, form.getBirthdate());
                callableStatement.setString(6, form.getContactPhone());
                callableStatement.setString(7, form.getContactHome());
                callableStatement.setString(8, form.getAddress());
                callableStatement.setString(9, form.getNote());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editPatientDocument(String token, int id, DocumentForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {

            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_person_document (?,?,?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setString(5, form.getNumber());
                callableStatement.setString(6, form.getSerial());
                callableStatement.setString(7, form.getStartDate());
                callableStatement.setString(8, form.getEndDate());
                callableStatement.setString(9, "");
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPatientContact(String token, int personId, ContactForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_person_contact (?, ?, ?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setString(5, form.getContact());
                callableStatement.executeUpdate();

                int contId = callableStatement.getInt(1);

                operationResponse.setData(contId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPatientAddress(String token, int personId, AddressForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_person_address (?, ?, ?, ?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getAddressId());
                callableStatement.setString(5, form.getAddress());
                callableStatement.setInt(6, form.getTypeId());;

                callableStatement.executeUpdate();

                int addressId = callableStatement.getInt(1);

                operationResponse.setData(addressId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPatientRelationShip(String token, int personId, PersonRelationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_person_relationship (?, ?, ?, ?, ?, ?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getRelationshipId());
                callableStatement.setString(5, form.getFullName());
                callableStatement.setString(6, form.getBirthdate());
                callableStatement.setString(7, form.getContactPhone());
                callableStatement.setString(8, form.getContactHome());
                callableStatement.setString(9, form.getAddress());
                callableStatement.setString(10, form.getNote());

                callableStatement.executeUpdate();

                int relationShipId = callableStatement.getInt(1);

                operationResponse.setData(relationShipId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removePatientAddress(String token, int id, int addressId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_person_address (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, addressId);
                callableStatement.setInt(3, id);
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removePatientDocument(String token, int id, int documentId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        List<FileWrapper> list = this.getPersonFile(token, documentId);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.remove_person_document (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, documentId);
                callableStatement.executeUpdate();

                operationResponse.setData(list);
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public List<FileWrapper> getPersonFile(String token, int documentId) {
        System.out.println("sdsdss    ++++++ " + documentId);
        List<FileWrapper> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_person_file f  where f.id=?")) {
                prepareStatement.setInt(1, documentId);

                try (ResultSet rs = prepareStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new FileWrapper(rs.getInt("id"),
                                rs.getString("original_name"),
                                rs.getString("path")));
                    }

                }
            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public OperationResponse removePatientRelationship(String token, int id, int relationshipId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_person_relationship (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, relationshipId);
                callableStatement.setInt(3, id);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public DataTable getPatientHistoryList(String token, int patientId, PatientHistorySearchForm form) {
        DataTable dataTable = new DataTable();
        List<PatientHistory> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.get_patient_history(?,?,?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, token);

                if (form.getServiceId() != null && form.getServiceId() > 0) {
                    callableStatement.setInt(2, form.getServiceId());
                } else {
                    callableStatement.setInt(2, 0);
                }
                if (form.getStatusType() != null && form.getStatusType() > 0) {
                    callableStatement.setInt(3, form.getStatusType());
                } else {
                    callableStatement.setInt(3, 0);
                }
                if (form.getQueueId() != null && form.getQueueId() > 0) {
                    callableStatement.setInt(4, form.getQueueId());
                } else {
                    callableStatement.setInt(4, 0);
                }
                callableStatement.setInt(5, patientId);
                callableStatement.setInt(6, form.getStart());
                callableStatement.setInt(7, form.getLength());
                callableStatement.registerOutParameter(8, Types.INTEGER);
                callableStatement.registerOutParameter(9, Types.OTHER);
                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(9);

                while (rs.next()) {

                    list.add(new PatientHistory(rs.getInt("r"), rs.getInt("patient_person_id"),
                            rs.getString("patient_first_name"),
                            rs.getString("patient_last_name"),
                            rs.getString("patient_middle_name"),
                            rs.getString("patient_birthdate"),
                            new DictionaryWrapper(rs.getInt("to_employee_id"),
                                    new MultilanguageString(rs.getString("from_employee_last_name"),
                                            rs.getString("from_employee_first_name"),
                                            rs.getString("from_employee_middle_name"))),
                            rs.getString("to_employee_first_name"), rs.getString("to_employee_last_name"), rs.getString("to_employee_middle_name"),
                            new DictionaryWrapper(rs.getInt("org_id"),
                                    new MultilanguageString(rs.getString("org_name_az"),
                                            rs.getString("org_name_en"),
                                            rs.getString("org_name_ru"))),
                            new DictionaryWrapper(rs.getInt("to_employee_position_id"),
                                    new MultilanguageString(rs.getString("to_employee_position_type_name_az"),
                                            rs.getString("to_employee_position_type_name_en"),
                                            rs.getString("to_employee_position_type_name_ru"))),
                            new DictionaryWrapper(rs.getInt("patient_service"),
                                    new MultilanguageString(rs.getString("service_name_az"),
                                            null,
                                            null)),
                            new DictionaryWrapper(rs.getInt("status_type_id"),
                                    new MultilanguageString(rs.getString("status_name_az"),
                                            rs.getString("status_name_en"),
                                            rs.getString("status_name_ru"))),
                            rs.getString("start_date"), rs.getString("end_date"),
                            new DictionaryWrapper(rs.getInt("queue_type_id"),
                                    new MultilanguageString(rs.getString("queue_name_az"),
                                            rs.getString("queue_name_en"),
                                            rs.getString("queue_name_ru"))),
                            new DictionaryWrapper(rs.getInt("payment_status"),
                                    new MultilanguageString(rs.getString("payment_status_name_az"),
                                            rs.getString("payment_status_name_en"),
                                            rs.getString("payment_status_name_ru"))),
                            rs.getString("price"), rs.getString("note"), rs.getInt("medical_commition_id")));

                }
                dataTable.setDataList(list);
                dataTable.setTotalCount(callableStatement.getInt(8));

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return dataTable;
    }

    @Override
    public OperationResponse changePatientStatus(String token, int id, int statusId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.change_patient_status (?, ?, ?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, statusId);
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPatientHamfullStuff(String token, HamfullStuffForm hamfullStuffForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            int hamfullId = 0;
            try (CallableStatement callabStatement = connection.prepareCall("{call cm_caremed.add_patient_hamful_stuff(?,?,?,?,?)} ")) {

                callabStatement.setString(1, token);
                callabStatement.setInt(2, hamfullStuffForm.getPatientId());
                callabStatement.setInt(3, hamfullStuffForm.getHarmfullStuffId());
                callabStatement.setString(4, hamfullStuffForm.getDate());
                callabStatement.setString(5, hamfullStuffForm.getNote());
                callabStatement.executeUpdate();

                operationResponse.setData(hamfullId);
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editPatientHamfullStuff(String token, int id, HamfullStuffForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.edit_patient_hamful_stuff(?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getHarmfullStuffId());
                callableStatement.setString(4, form.getDate());
                callableStatement.setString(5, form.getNote());
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPatientSpecialNotes(String token, PatientSpecialNotesForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.add_patient_special_notes(?,?,?,?,?,?,?,?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, form.getPatientId());
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setString(5, form.getNote());
                callableStatement.setString(6, form.getOrginalName());
                callableStatement.setString(7, form.getPath());
                callableStatement.setInt(8, form.getSize());
                callableStatement.setString(9, form.getContentType());
                callableStatement.execute();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public List<HamfullStuff> getHamfullStuff(String token) {
        List<HamfullStuff> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_patient_harmful_stuff  ")) {

                try (ResultSet rs = preparedStatement.executeQuery()) {
                    while (rs.next()) {

                        list.add(new HamfullStuff(rs.getInt("id"), rs.getInt("patient_id"), new DictionaryWrapper(rs.getInt("harmfull_stuff_id"),
                                new MultilanguageString(rs.getString("harmfull_name_az"),
                                        rs.getString("harmfull_name_en"),
                                        rs.getString("harmfull_name_ru"))),
                                rs.getString("date_of"), rs.getString("note")));
                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);

        }

        return list;
    }

    @Override
    public OperationResponse addPatientProphylaxlsesVaccination(String token, PatientProphylaxlsesVaccinationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            int vaccinationId = 0;

            try (CallableStatement callabStatement = connection.prepareCall("{? = call cm_caremed.add_patient_prophylaxlses_vaccination(?,?,?,?,?,?,?,?,?)} ")) {
                callabStatement.registerOutParameter(1, Types.INTEGER);
                callabStatement.setString(2, token);
                callabStatement.setInt(3, form.getPatientId());
                callabStatement.setInt(4, form.getVaccinationId());
                callabStatement.setString(5, form.getDateOfVaccination());
                callabStatement.setString(6, form.getAgainstWhichInfaction());
                callabStatement.setString(7, form.getDoze());
                callabStatement.setString(8, form.getNumberOfSeries());
                callabStatement.setString(9, form.getResult());
                callabStatement.setString(10, form.getNote());
                callabStatement.executeUpdate();

                operationResponse.setData(callabStatement.getInt(1));
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);

        }

        return operationResponse;
    }

    @Override
    public List<PatientProphylaxlsesVaccination> getPatientProphylaxlsesVaccination(String token) {
        List<PatientProphylaxlsesVaccination> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_patient_prophylaxlses_vaccination  ")) {

                try (ResultSet rs = preparedStatement.executeQuery()) {
                    while (rs.next()) {
                        list.add(new PatientProphylaxlsesVaccination(rs.getInt("id"),
                                rs.getInt("patient_id"),
                                new DictionaryWrapper(rs.getInt("vaccination_id"),
                                        new MultilanguageString(rs.getString("vaccionation_name_az"),
                                                rs.getString("vaccionation_name_en"),
                                                rs.getString("vaccionation_name_ru"))),
                                rs.getString("date_of_vaccination"),
                                rs.getString("against_which_infaction"), rs.getString("doze"), rs.getString("number_of_series"), rs.getString("result"), rs.getString("note")));
                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);

        }

        return list;
    }

    @Override
    public OperationResponse editPatientProphylaxlsesVaccination(String token, int id, PatientProphylaxlsesVaccinationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_patient_prophylaxlses_vaccination(?,?,?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getVaccinationId());
                callableStatement.setString(4, form.getDateOfVaccination());
                callableStatement.setString(5, form.getAgainstWhichInfaction());
                callableStatement.setString(6, form.getDoze());
                callableStatement.setString(7, form.getNumberOfSeries());
                callableStatement.setString(8, form.getResult());
                callableStatement.setString(9, form.getNote());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public List<PatientSpecialNotes> getPatientSpecialNotes(String token) {
        List<PatientSpecialNotes> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_patient_special_notes")) {

                try (ResultSet rs = preparedStatement.executeQuery()) {
                    list.add(new PatientSpecialNotes(rs.getInt("id"), rs.getInt("patient_id"),
                            new DictionaryWrapper(rs.getInt("vaccination_id"),
                                    new MultilanguageString(rs.getString("vaccionation_name_az"),
                                            rs.getString("vaccionation_name_en"),
                                            rs.getString("vaccionation_name_ru"))),
                            rs.getInt("file_id"),
                            rs.getString("note"),
                            rs.getString("original_name"),
                            rs.getString("path"),
                            rs.getInt("size"),
                            rs.getString("content_type")));

                }

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public OperationResponse editPatientSpecialNotes(String token, PatientSpecialNotesForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall(" call cm_caremed.edit_patient_special_notes (?,?,?,?,?,?,?,?)")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getPatientId());
                callableStatement.setInt(3, form.getTypeId());
                callableStatement.setString(4, form.getNote());
                callableStatement.setString(5, form.getOrginalName());
                callableStatement.setString(6, form.getPath());
                callableStatement.setInt(7, form.getSize());
                callableStatement.setString(8, form.getContentType());
                callableStatement.executeUpdate();

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse removePatientHamfullStuff(String token, int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.remove_patient_hamful_stuff(?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.executeUpdate();
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removePatientProphylaxlsesVaccination(String token, int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.remove_patient_prophylaxlses_vaccination(?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.executeUpdate();
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
}
