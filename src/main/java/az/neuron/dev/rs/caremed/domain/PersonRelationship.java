/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class PersonRelationship {
    private int id;
    private DictionaryWrapper relation;
    private String fullName;
    private String birthdate;
    private String contactHome;
    private String contactPhone;
    private String address;
    private String note;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserId;

    public PersonRelationship() {
    }

    public PersonRelationship(int id, DictionaryWrapper relation, String fullName, String birthdate, String contactHome, String contactPhone, String address, String note) {
        this.id = id;
        this.relation = relation;
        this.fullName = fullName;
        this.birthdate = birthdate;
        this.contactHome = contactHome;
        this.contactPhone = contactPhone;
        this.address = address;
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getRelation() {
        return relation;
    }

    public void setRelation(DictionaryWrapper relation) {
        this.relation = relation;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getContactHome() {
        return contactHome;
    }

    public void setContactHome(String contactHome) {
        this.contactHome = contactHome;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Override
    public String toString() {
        return "PersonRelationship{" + "id=" + id + ", relation=" + relation + ", fullName=" + fullName + ", birthdate=" + birthdate + ", contactHome=" + contactHome + ", contactPhone=" + contactPhone + ", address=" + address + ", note=" + note + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }    
}
