/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class EmployeeList {

    private int id;
    private int custNum;
    private int personId;
    private DictionaryWrapper specId;
    private DictionaryWrapper positionId;
    private DictionaryWrapper positionCategoryId;
    private DictionaryWrapper orgId;
    private DictionaryWrapper statId;
    private DictionaryWrapper payId;
    private String empStartDate;
    private String empNote;
    private String empCreateDate;
    private int createUserId;
    private String empUpdateDate;
    private int updateUserId;
    private String fullName;
    private String fistName;
    private String lastName;
    private String middleName;
    private DictionaryWrapper citizenshipId;
    private DictionaryWrapper maritalStatusId;
    private DictionaryWrapper millitaryServiceId;
    private DictionaryWrapper nationalityId;
    private DictionaryWrapper socialStatusId;
    private String birthdate;
    private String pincode;
    private String seriya;
    private DictionaryWrapper svSeriyaId;
    private String fullSeriya;
    private DictionaryWrapper bloodGroupId;
    private DictionaryWrapper supplyOrganization;
    private String svEndDate;
    private int height;
    private DictionaryWrapper genderId;
    private int photoFileId;
    private String memberOrgName;
    private String memberPeriod;
    private String memberCardNum;
    private String memberNote;

    EmployeeList() {
    }

    public EmployeeList(DictionaryWrapper specId, DictionaryWrapper positionId, DictionaryWrapper orgId, DictionaryWrapper statId, String fullName, DictionaryWrapper maritalStatusId, DictionaryWrapper nationalityId, String birthdate, String fullSeriya, DictionaryWrapper supplyOrganization, int photoFileId) {
        this.specId = specId;
        this.positionId = positionId;
        this.orgId = orgId;
        this.statId = statId;
        this.fullName = fullName;
        this.maritalStatusId = maritalStatusId;
        this.nationalityId = nationalityId;
        this.birthdate = birthdate;
        this.fullSeriya = fullSeriya;
        this.supplyOrganization = supplyOrganization;
        this.photoFileId = photoFileId;
    }

    public EmployeeList(int id, int custNum, int personId, DictionaryWrapper specId, DictionaryWrapper positionId, DictionaryWrapper positionCategoryId, DictionaryWrapper orgId, DictionaryWrapper statId, DictionaryWrapper payId, String empStartDate, String empNote, String empCreateDate, int createUserId, String empUpdateDate, int updateUserId, String fistName, String lastName, String middleName, DictionaryWrapper citizenshipId, DictionaryWrapper maritalStatusId, DictionaryWrapper millitaryServiceId, DictionaryWrapper nationalityId, DictionaryWrapper socialStatusId, String birthdate, String pincode, String seriya, DictionaryWrapper svSeriyaId, DictionaryWrapper bloodGroupId, DictionaryWrapper supplyOrganization, String svEndDate, int height, DictionaryWrapper genderId, int photoFileId, String memberOrgName, String memberPeriod, String memberCardNum, String memberNote) {
        this.id = id;
        this.custNum = custNum;
        this.personId = personId;
        this.specId = specId;
        this.positionId = positionId;
        this.positionCategoryId = positionCategoryId;
        this.orgId = orgId;
        this.statId = statId;
        this.payId = payId;
        this.empStartDate = empStartDate;
        this.empNote = empNote;
        this.empCreateDate = empCreateDate;
        this.createUserId = createUserId;
        this.empUpdateDate = empUpdateDate;
        this.updateUserId = updateUserId;
        this.fistName = fistName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.citizenshipId = citizenshipId;
        this.maritalStatusId = maritalStatusId;
        this.millitaryServiceId = millitaryServiceId;
        this.nationalityId = nationalityId;
        this.socialStatusId = socialStatusId;
        this.birthdate = birthdate;
        this.pincode = pincode;
        this.seriya = seriya;
        this.svSeriyaId = svSeriyaId;
        this.bloodGroupId = bloodGroupId;
        this.supplyOrganization = supplyOrganization;
        this.svEndDate = svEndDate;
        this.height = height;
        this.genderId = genderId;
        this.photoFileId = photoFileId;
        this.memberOrgName = memberOrgName;
        this.memberPeriod = memberPeriod;
        this.memberCardNum = memberCardNum;
        this.memberNote = memberNote;
    }

    public String getFullSeriya() {
        return fullSeriya;
    }

    public void setFullSeriya(String fullSeriya) {
        this.fullSeriya = fullSeriya;
    }

    public EmployeeList(int custNum, int personId, DictionaryWrapper specId, DictionaryWrapper positionId, DictionaryWrapper positionCategoryId, DictionaryWrapper orgId, DictionaryWrapper statId, DictionaryWrapper payId, String empStartDate, String empNote, String empCreateDate, int createUserId, String empUpdateDate, int updateUserId, String fullName, String fistName, String lastName, String middleName, DictionaryWrapper citizenshipId, DictionaryWrapper maritalStatusId, DictionaryWrapper millitaryServiceId, DictionaryWrapper nationalityId, DictionaryWrapper socialStatusId, String birthdate, String pincode, String seriya, DictionaryWrapper svSeriyaId, DictionaryWrapper bloodGroupId, DictionaryWrapper supplyOrganization, String svEndDate, int height, DictionaryWrapper genderId, int photoFileId, String memberOrgName, String memberPeriod, String memberCardNum, String memberNote) {
        this.custNum = custNum;
        this.personId = personId;
        this.specId = specId;
        this.positionId = positionId;
        this.positionCategoryId = positionCategoryId;
        this.orgId = orgId;
        this.statId = statId;
        this.payId = payId;
        this.empStartDate = empStartDate;
        this.empNote = empNote;
        this.empCreateDate = empCreateDate;
        this.createUserId = createUserId;
        this.empUpdateDate = empUpdateDate;
        this.updateUserId = updateUserId;
        this.fullName = fullName;
        this.fistName = fistName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.citizenshipId = citizenshipId;
        this.maritalStatusId = maritalStatusId;
        this.millitaryServiceId = millitaryServiceId;
        this.nationalityId = nationalityId;
        this.socialStatusId = socialStatusId;
        this.birthdate = birthdate;
        this.pincode = pincode;
        this.seriya = seriya;
        this.svSeriyaId = svSeriyaId;
        this.bloodGroupId = bloodGroupId;
        this.supplyOrganization = supplyOrganization;
        this.svEndDate = svEndDate;
        this.height = height;
        this.genderId = genderId;
        this.photoFileId = photoFileId;
        this.memberOrgName = memberOrgName;
        this.memberPeriod = memberPeriod;
        this.memberCardNum = memberCardNum;
        this.memberNote = memberNote;
    }

    public EmployeeList(int personId, String fullName, DictionaryWrapper orgId, DictionaryWrapper specId, DictionaryWrapper positionId, DictionaryWrapper genderId) {
        this.personId = personId;
        this.fullName = fullName;
        this.orgId = orgId;
        this.specId = specId;
        this.positionId = positionId;
        this.genderId = genderId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustNum() {
        return custNum;
    }

    public void setCustNum(int custNum) {
        this.custNum = custNum;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public DictionaryWrapper getSpecId() {
        return specId;
    }

    public void setSpecId(DictionaryWrapper specId) {
        this.specId = specId;
    }

    public DictionaryWrapper getPositionId() {
        return positionId;
    }

    public void setPositionId(DictionaryWrapper positionId) {
        this.positionId = positionId;
    }

    public DictionaryWrapper getPositionCategoryId() {
        return positionCategoryId;
    }

    public void setPositionCategoryId(DictionaryWrapper positionCategoryId) {
        this.positionCategoryId = positionCategoryId;
    }

    public DictionaryWrapper getOrgId() {
        return orgId;
    }

    public void setOrgId(DictionaryWrapper orgId) {
        this.orgId = orgId;
    }

    public DictionaryWrapper getStatId() {
        return statId;
    }

    public void setStatId(DictionaryWrapper statId) {
        this.statId = statId;
    }

    public DictionaryWrapper getPayId() {
        return payId;
    }

    public void setPayId(DictionaryWrapper payId) {
        this.payId = payId;
    }

    public String getEmpStartDate() {
        return empStartDate;
    }

    public void setEmpStartDate(String empStartDate) {
        this.empStartDate = empStartDate;
    }

    public String getEmpNote() {
        return empNote;
    }

    public void setEmpNote(String empNote) {
        this.empNote = empNote;
    }

    public String getEmpCreateDate() {
        return empCreateDate;
    }

    public void setEmpCreateDate(String empCreateDate) {
        this.empCreateDate = empCreateDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getEmpUpdateDate() {
        return empUpdateDate;
    }

    public void setEmpUpdateDate(String empUpdateDate) {
        this.empUpdateDate = empUpdateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public DictionaryWrapper getCitizenshipId() {
        return citizenshipId;
    }

    public void setCitizenshipId(DictionaryWrapper citizenshipId) {
        this.citizenshipId = citizenshipId;
    }

    public DictionaryWrapper getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setMaritalStatusId(DictionaryWrapper maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }

    public DictionaryWrapper getMillitaryServiceId() {
        return millitaryServiceId;
    }

    public void setMillitaryServiceId(DictionaryWrapper millitaryServiceId) {
        this.millitaryServiceId = millitaryServiceId;
    }

    public DictionaryWrapper getNationalityId() {
        return nationalityId;
    }

    public void setNationalityId(DictionaryWrapper nationalityId) {
        this.nationalityId = nationalityId;
    }

    public DictionaryWrapper getSocialStatusId() {
        return socialStatusId;
    }

    public void setSocialStatusId(DictionaryWrapper socialStatusId) {
        this.socialStatusId = socialStatusId;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getSeriya() {
        return seriya;
    }

    public void setSeriya(String seriya) {
        this.seriya = seriya;
    }

    public DictionaryWrapper getSvSeriyaId() {
        return svSeriyaId;
    }

    public void setSvSeriyaId(DictionaryWrapper svSeriyaId) {
        this.svSeriyaId = svSeriyaId;
    }

    public DictionaryWrapper getBloodGroupId() {
        return bloodGroupId;
    }

    public void setBloodGroupId(DictionaryWrapper bloodGroupId) {
        this.bloodGroupId = bloodGroupId;
    }

    public DictionaryWrapper getSupplyOrganization() {
        return supplyOrganization;
    }

    public void setSupplyOrganization(DictionaryWrapper supplyOrganization) {
        this.supplyOrganization = supplyOrganization;
    }

    public String getSvEndDate() {
        return svEndDate;
    }

    public void setSvEndDate(String svEndDate) {
        this.svEndDate = svEndDate;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public DictionaryWrapper getGenderId() {
        return genderId;
    }

    public void setGenderId(DictionaryWrapper genderId) {
        this.genderId = genderId;
    }

    public int getPhotoFileId() {
        return photoFileId;
    }

    public void setPhotoFileId(int photoFileId) {
        this.photoFileId = photoFileId;
    }

    public String getMemberOrgName() {
        return memberOrgName;
    }

    public void setMemberOrgName(String memberOrgName) {
        this.memberOrgName = memberOrgName;
    }

    public String getMemberPeriod() {
        return memberPeriod;
    }

    public void setMemberPeriod(String memberPeriod) {
        this.memberPeriod = memberPeriod;
    }

    public String getMemberCardNum() {
        return memberCardNum;
    }

    public void setMemberCardNum(String memberCardNum) {
        this.memberCardNum = memberCardNum;
    }

    public String getMemberNote() {
        return memberNote;
    }

    public void setMemberNote(String memberNote) {
        this.memberNote = memberNote;
    }

    @Override
    public String toString() {
        return "EmployeeList{" + "id=" + id + ", custNum=" + custNum + ", personId=" + personId + ", specId=" + specId + ", positionId=" + positionId + ", positionCategoryId=" + positionCategoryId + ", orgId=" + orgId + ", statId=" + statId + ", payId=" + payId + ", empStartDate=" + empStartDate + ", empNote=" + empNote + ", empCreateDate=" + empCreateDate + ", createUserId=" + createUserId + ", empUpdateDate=" + empUpdateDate + ", updateUserId=" + updateUserId + ", fullName=" + fullName + ", fistName=" + fistName + ", lastName=" + lastName + ", middleName=" + middleName + ", citizenshipId=" + citizenshipId + ", maritalStatusId=" + maritalStatusId + ", millitaryServiceId=" + millitaryServiceId + ", nationalityId=" + nationalityId + ", socialStatusId=" + socialStatusId + ", birthdate=" + birthdate + ", pincode=" + pincode + ", seriya=" + seriya + ", svSeriyaId=" + svSeriyaId + ", bloodGroupId=" + bloodGroupId + ", supplyOrganization=" + supplyOrganization + ", svEndDate=" + svEndDate + ", height=" + height + ", genderId=" + genderId + ", photoFileId=" + photoFileId + ", memberOrgName=" + memberOrgName + ", memberPeriod=" + memberPeriod + ", memberCardNum=" + memberCardNum + ", memberNote=" + memberNote + '}';
    }

}
