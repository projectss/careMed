/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class ResearchInfo {
    private int id;
    private DictionaryWrapper researchTypeId;
    private String researchName;
    private String startDate;
    private String note;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserid;

    public ResearchInfo() {
    }

    public ResearchInfo(int id, DictionaryWrapper researchTypeId, String researchName, String startDate, String note) {
        this.id = id;
        this.researchTypeId = researchTypeId;
        this.researchName = researchName;
        this.startDate = startDate;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getResearchTypeId() {
        return researchTypeId;
    }

    public void setResearchTypeId(DictionaryWrapper researchTypeId) {
        this.researchTypeId = researchTypeId;
    }

    public String getResearchName() {
        return researchName;
    }

    public void setResearchName(String researchName) {
        this.researchName = researchName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserid() {
        return updateUserid;
    }

    public void setUpdateUserid(int updateUserid) {
        this.updateUserid = updateUserid;
    }

    @Override
    public String toString() {
        return "ResearchInfo{" + "id=" + id + ", researchTypeId=" + researchTypeId + ", researchName=" + researchName + ", startDate=" + startDate + ", note=" + note + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserid=" + updateUserid + '}';
    }    
}
