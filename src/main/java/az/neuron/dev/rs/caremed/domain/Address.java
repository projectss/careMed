/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

import java.util.Date;

/**
 *
 * @author Bayram
 */
public class Address {
    private int id;
    private DictionaryWrapper type;
    private AddressTree addressTree;
    private String name;
    private Date createDate;
    private Date lastUpdate;
    private boolean active;
    private MultilanguageString fullAddress;

    Address(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getType() {
        return type;
    }

    public void setType(DictionaryWrapper type) {
        this.type = type;
    }

    public AddressTree getAddressTree() {
        return addressTree;
    }

    public void setAddressTree(AddressTree addressTree) {
        this.addressTree = addressTree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public MultilanguageString getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(MultilanguageString fullAddress) {
        this.fullAddress = fullAddress;
    }

    @Override
    public String toString() {
        return "Address{" + "id=" + id + ", type=" + type + ", addressTree=" + addressTree + ", name=" + name + ", createDate=" + createDate + ", lastUpdate=" + lastUpdate + ", active=" + active + ", fullAddress=" + fullAddress + '}';
    }
    
    
    
}

