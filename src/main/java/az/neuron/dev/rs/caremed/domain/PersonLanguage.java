/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class PersonLanguage {
    private int id;
    private DictionaryWrapper languageId;
    private DictionaryWrapper degreeId;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserId;

    public PersonLanguage() {
    }

    public PersonLanguage(int id, DictionaryWrapper languageId, DictionaryWrapper degreeId) {
        this.id = id;
        this.languageId = languageId;
        this.degreeId = degreeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getLanguageId() {
        return languageId;
    }

    public void setLanguageId(DictionaryWrapper languageId) {
        this.languageId = languageId;
    }

    public DictionaryWrapper getDegreeId() {
        return degreeId;
    }

    public void setDegreeId(DictionaryWrapper degreeId) {
        this.degreeId = degreeId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Override
    public String toString() {
        return "PersonLanguage{" + "id=" + id + ", languageId=" + languageId + ", degreeId=" + degreeId + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }
    
}
