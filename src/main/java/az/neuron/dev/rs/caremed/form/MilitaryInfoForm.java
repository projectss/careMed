/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class MilitaryInfoForm extends BaseForm{
    private int id;
    private int personId;
    private int millitaryNameId;
    private int staffType;
    private int armyType;
    private String note;
    
    MilitaryInfoForm(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public int getMillitaryNameId() {
        return millitaryNameId;
    }

    public void setMillitaryNameId(int millitaryNameId) {
        this.millitaryNameId = millitaryNameId;
    }

    public int getStaffType() {
        return staffType;
    }

    public void setStaffType(int staffType) {
        this.staffType = staffType;
    }

    public int getArmyType() {
        return armyType;
    }

    public void setArmyType(int armyType) {
        this.armyType = armyType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "MillitaryinfoForm{" + "id=" + id + ", personId=" + personId + ", millitaryNameId=" + millitaryNameId + ", staffType=" + staffType + ", armyType=" + armyType + ", note=" + note + '}';
    }
    
    
    
}
