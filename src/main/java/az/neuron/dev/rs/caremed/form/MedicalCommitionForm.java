/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Admin
 */
public class MedicalCommitionForm {
private int id;
private int patientId;
private String startDate;
private String endDate;
private String nextDate;
private int statusId;
private int reasonId;
private String reasonNote;
private ServiceOperationForm [] serviceOperationForm;

    public MedicalCommitionForm() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNextDate() {
        return nextDate;
    }

    public void setNextDate(String nextDate) {
        this.nextDate = nextDate;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getReasonId() {
        return reasonId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    public ServiceOperationForm[] getServiceOperationForm() {
        return serviceOperationForm;
    }

    public void setServiceOperationForm(ServiceOperationForm[] serviceOperationForm) {
        this.serviceOperationForm = serviceOperationForm;
    }

    @Override
    public String toString() {
        return "MedicalCommitionForm{" + "id=" + id + ", patientId=" + patientId + ", startDate=" + startDate + ", endDate=" + endDate + ", nextDate=" + nextDate + ", statusId=" + statusId + ", reasonId=" + reasonId + ", reasonNote=" + reasonNote + ", serviceOperationForm=" + serviceOperationForm + '}';
    }


    
       
    
}
