/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.db.DbConnect;
import az.neuron.dev.rs.caremed.domain.CommitionHistory;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.DictionaryWrapper;
import az.neuron.dev.rs.caremed.domain.EmployeeStatus;
import az.neuron.dev.rs.caremed.domain.MultilanguageString;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.Patient;
import az.neuron.dev.rs.caremed.domain.PatientHistory;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.form.EmpStatusForm;
import az.neuron.dev.rs.caremed.form.EmployeeStatusForm;
import az.neuron.dev.rs.caremed.form.OperationForm;
import az.neuron.dev.rs.caremed.form.ServicesForm;
import az.neuron.dev.rs.caremed.search.PatientCommitionHistory;
import az.neuron.dev.rs.caremed.search.PatientSearchForm;
import az.neuron.dev.rs.caremed.util.Converter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bayram
 */
@Repository
public class OperationDao implements IOperationDao {

    private static final Logger log = Logger.getLogger(OperationDao.class);

    @Autowired
    private DbConnect dbConnect;

    @Override
    public List<EmployeeStatus> getEmployeeStatus(String token, EmployeeStatusForm form) {
        List<EmployeeStatus> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.get_employee_status(?,?,?)}")) {
                callableStatement.registerOutParameter(1, Types.OTHER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, form.getOrgId());
                callableStatement.setInt(4, form.getPositionId());
                callableStatement.execute();
                try (ResultSet rs = (ResultSet) callableStatement.getObject(1)) {
                    while (rs.next()) {

                        list.add(new EmployeeStatus(new DictionaryWrapper(rs.getInt("position_id"),
                                new MultilanguageString(rs.getString("position_name_az"),
                                        rs.getString("position_name_en"),
                                        rs.getString("position_name_ru"))),
                                new DictionaryWrapper(rs.getInt("status"),
                                        new MultilanguageString(rs.getString("status_name_az"),
                                                rs.getString("status_name_en"),
                                                rs.getString("status_name_ru"))),
                                rs.getInt("id"),
                                rs.getString("first_name"),
                                rs.getString("last_name"),
                                rs.getString("middle_name"),
                                null));

                    }

                }

            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public OperationResponse addPatientServices(String token, ServicesForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.add_patient_service(?,?,?,?,?,?,?,?,?) }")) {
//               
//                callableStatement.setString(1, token);
//                callableStatement.setInt(2, form.getEmployeeId());
//                callableStatement.setInt(3, form.getPatientId());
//                callableStatement.setInt(4, form.getServiceTypeId());
//                callableStatement.setInt(5, form.getOrgId());
//                callableStatement.setString(6, form.getDuration());
//                callableStatement.setString(7, form.getCost());
//                callableStatement.setInt(8, form.getCurrencyId());
//                callableStatement.setInt(9, form.getPatientStatusId());
                callableStatement.execute();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse changeEmployeeStatus(String token, EmpStatusForm empStatusForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.change_status (?, ?, ?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, empStatusForm.getEmployeeId());
                callableStatement.setInt(3, empStatusForm.getStatusId());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addServices(String token, ServicesForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            int serviceId = 0;
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.add_services (?, ?, ?, ?, ?, ?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.OTHER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, form.getServiceTypeId());
                callableStatement.setString(4, form.getName());
                callableStatement.setString(5, form.getDuration());
                callableStatement.setInt(6, form.getCost());
                callableStatement.setInt(7, form.getCurrencyId());
                callableStatement.setString(8, form.getNote());
                callableStatement.execute();

                serviceId = callableStatement.getInt(1);

                operationResponse.setData(serviceId);
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPatientServiceOperation(String token, OperationForm operationForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            int serviceId = 0;
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.add_patient_service_operation (?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setArray(3, connection.createArrayOf("text", Converter.toPlpgServiceOperation(operationForm.getServiceOperationForm())));
                callableStatement.execute();

                serviceId = callableStatement.getInt(1);
                operationResponse.setData(serviceId);
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public DataTable getPatientForCommition(String token, PatientSearchForm form) {
        DataTable dataTable = new DataTable();
        List<Patient> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.get_patient_for_commition(?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, token);

                if (form.getPatientDocNum() != null && form.getPatientDocNum().length() > 0) {
                    callableStatement.setString(2, form.getPatientDocNum());
                } else {
                    callableStatement.setString(2, null);
                }

                if (form.getGenderId() != null && form.getGenderId() > 0) {
                    callableStatement.setInt(3, form.getGenderId());
                } else {
                    callableStatement.setInt(3, 0);
                }
                callableStatement.setInt(4, form.getStart());
                callableStatement.setInt(5, form.getLength());
                callableStatement.registerOutParameter(6, Types.INTEGER);
                callableStatement.registerOutParameter(7, Types.OTHER);
                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(7);

                while (rs.next()) {

                    list.add(new Patient(rs.getInt("id"), rs.getInt("r"), rs.getString("patient_doc_num"),
                            rs.getString("registration_date"),
                            new DictionaryWrapper(rs.getInt("company_name_id"),
                                    new MultilanguageString(rs.getString("company_name_az"),
                                            rs.getString("company_name_en"),
                                            rs.getString("company_name_ru"))),
                            new DictionaryWrapper(rs.getInt("position_id"),
                                    new MultilanguageString(rs.getString("position_name_az"),
                                            rs.getString("position_name_en"),
                                            rs.getString("position_name_ru"))),
                            new DictionaryWrapper(rs.getInt("type_id"),
                                    new MultilanguageString(rs.getString("type_name_az"),
                                            rs.getString("type_name_en"),
                                            rs.getString("type_name_ru"))),
                            rs.getInt("person_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("middle_name"),
                            new DictionaryWrapper(rs.getInt("gender_id"),
                                    new MultilanguageString(rs.getString("gender_name_az"),
                                            rs.getString("gender_name_en"),
                                            rs.getString("gender_name_ru"))), rs.getString("birthdate")));

                }
                dataTable.setDataList(list);
                dataTable.setTotalCount(callableStatement.getInt(6));

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return dataTable;
    }

    @Override
    public DataTable getCommitionHistory(String token, int patientId, PatientCommitionHistory patientCommitionHistory) {
        DataTable dataTable = new DataTable();
        List<CommitionHistory> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.get_patient_commition_history ( ?, ?,?, ?,?, ?,?, ?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, patientId);
                if (patientCommitionHistory.getStatusId()!= null && patientCommitionHistory.getStatusId()>0){
                callableStatement.setInt(3, patientCommitionHistory.getStatusId());
                
                }else {
                callableStatement.setInt(3, 0);
                }
                    
                if (patientCommitionHistory.getReasonId()!= null && patientCommitionHistory.getReasonId() >0){
                callableStatement.setInt(4, patientCommitionHistory.getReasonId());
                }else {
                callableStatement.setInt(4, 0);
                }
                
                callableStatement.setInt(5, patientCommitionHistory.getStart());
                callableStatement.setInt(6, patientCommitionHistory.getLength());
                callableStatement.registerOutParameter(7, Types.INTEGER);
                callableStatement.registerOutParameter(8, Types.OTHER);
                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(8);

                while (rs.next()) {
                    list.add(new CommitionHistory(rs.getInt("r"),rs.getInt("id"),
                            rs.getInt("patient_id"),
                            rs.getString("start_date"),
                            rs.getString("end_date"),
                            rs.getString("next_date"),
                            new DictionaryWrapper(rs.getInt("status_id"),
                                    new MultilanguageString(rs.getString("status_name_az"),
                                            rs.getString("status_name_en"),
                                            rs.getString("status_name_ru"))),
                            new DictionaryWrapper(rs.getInt("reason_id"),
                                    new MultilanguageString(rs.getString("reason_name_az"),
                                            rs.getString("reason_name_en"),
                                            rs.getString("reason_name_ru"))),
                            rs.getString("reason_note")));

                    dataTable.setDataList(list);
                    dataTable.setTotalCount(callableStatement.getInt(7));

                }

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return dataTable;
    }

    @Override
    public OperationResponse addPatientMedicalCommition(String token, int patientId, OperationForm operationForm
    ) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.add_patient_medical_commition(?, ?, ?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, patientId);
                callableStatement.setArray(3, connection.createArrayOf("text", Converter.toPlpgServiceOperation(operationForm.getServiceOperationForm())));
                callableStatement.execute();

                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public List<PatientHistory> getServicesForMedicalCommition(String token, int commitionId) {
        List <PatientHistory> list = new ArrayList<>();
        
        try(Connection connection = dbConnect.getConnection()){
        connection.setAutoCommit(false);
            try(PreparedStatement preparedStatement = connection.prepareCall("select * from cm_caremed.v_patient_service_operations   s where s.medical_commition_id = ?")){
                preparedStatement.setInt(1, commitionId);
                
                try(ResultSet rs = preparedStatement.executeQuery()){
                
                    while(rs.next()){
                    
                    
                   list.add(new PatientHistory(0, rs.getInt("person_id"),
                            rs.getString("patient_first_name"),
                            rs.getString("patient_last_name"),
                            rs.getString("patient_middle_name"),
                            rs.getString("patient_birthdate"),
                            new DictionaryWrapper(rs.getInt("doctor_id"),
                                    new MultilanguageString(rs.getString("doctor_last_name"),
                                            rs.getString("doctor_first_name"),
                                            rs.getString("doctor_middle_name"))),
                            rs.getString("doctor_first_name"), rs.getString("doctor_last_name"), rs.getString("doctor_middle_name"),
                            new DictionaryWrapper(rs.getInt("doctor_org"),
                                    new MultilanguageString(rs.getString("doctor_org_name_az"),
                                            rs.getString("doctor_org_name_en"),
                                            rs.getString("doctor_org_name_ru"))),
                            new DictionaryWrapper(rs.getInt("doctor_position"),
                                    new MultilanguageString(rs.getString("doctor_position_name_az"),
                                            rs.getString("doctor_position_name_en"),
                                            rs.getString("doctor_position_name_ru"))),
                            new DictionaryWrapper(rs.getInt("patinet_service"),
                                    new MultilanguageString(rs.getString("service_name_az"),
                                            null,
                                            null)),
                            new DictionaryWrapper(rs.getInt("patinet_status_type"),
                                    new MultilanguageString(rs.getString("status_type_name_az"),
                                            rs.getString("status_type_name_en"),
                                            rs.getString("status_type_name_ru"))),
                            rs.getString("start_date"), rs.getString("end_date"),
                            new DictionaryWrapper(rs.getInt("queue_type_id"),
                                    new MultilanguageString(rs.getString("queue_type_name_az"),
                                            rs.getString("queue_type_name_en"),
                                            rs.getString("queue_type_name_ru"))),
                            new DictionaryWrapper(rs.getInt("payment_status"),
                                    new MultilanguageString(rs.getString("payment_status_name_az"),
                                            rs.getString("payment_status_name_en"),
                                            rs.getString("payment_status_name_ru"))),
                            rs.getString("price"), rs.getString("note"), rs.getInt("medical_commition_id")));
                    }
                
                }
            
            
            }
        
        }catch(Exception e){
        log.error(e.getMessage(), e);
        }
        
        
        return list;
    }

}
