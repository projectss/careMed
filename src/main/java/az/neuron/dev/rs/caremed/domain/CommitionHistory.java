/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class CommitionHistory {
    private int number;
    private int id;
    private int patientId;
    private String startDate;
    private String endDate;
    private String nextDate;
    private DictionaryWrapper statusId;
    private DictionaryWrapper reasonId;
    private String reasonNote;

    public CommitionHistory() {
    }

    public CommitionHistory(int number,int id, int patientId, String startDate, String endDate, String nextDate, DictionaryWrapper statusId, DictionaryWrapper reasonId, String reasonNote) {
        this.number = number;
        this.id = id;
        this.patientId = patientId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.nextDate = nextDate;
        this.statusId = statusId;
        this.reasonId = reasonId;
        this.reasonNote = reasonNote;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNextDate() {
        return nextDate;
    }

    public void setNextDate(String nextDate) {
        this.nextDate = nextDate;
    }

    public DictionaryWrapper getStatusId() {
        return statusId;
    }

    public void setStatusId(DictionaryWrapper statusId) {
        this.statusId = statusId;
    }

    public DictionaryWrapper getReasonId() {
        return reasonId;
    }

    public void setReasonId(DictionaryWrapper reasonId) {
        this.reasonId = reasonId;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    @Override
    public String toString() {
        return "CommitionHistory{" + "number=" + number + ", id=" + id + ", patientId=" + patientId + ", startDate=" + startDate + ", endDate=" + endDate + ", nextDate=" + nextDate + ", statusId=" + statusId + ", reasonId=" + reasonId + ", reasonNote=" + reasonNote + '}';
    }


    
    
}
