/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class PersonAddress {
    private int id;
    private DictionaryWrapper address;
    private DictionaryWrapper type;
    private String addressName;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserId;

    public PersonAddress() {
    }

    public PersonAddress(int id, DictionaryWrapper address, DictionaryWrapper type, String addressName) {
        this.id = id;
        this.address = address;
        this.type = type;
        this.addressName = addressName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getAddress() {
        return address;
    }

    public void setAddress(DictionaryWrapper address) {
        this.address = address;
    }

    public DictionaryWrapper getType() {
        return type;
    }

    public void setType(DictionaryWrapper type) {
        this.type = type;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Override
    public String toString() {
        return "PersonAddress{" + "id=" + id + ", address=" + address + ", type=" + type + ", addressName=" + addressName + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }    
    
}
