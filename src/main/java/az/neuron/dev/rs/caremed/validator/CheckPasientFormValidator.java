/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.validator;

import az.neuron.dev.rs.caremed.enums.Constants;
import az.neuron.dev.rs.caremed.form.CheckPasientForm;
import az.neuron.dev.rs.caremed.form.CheckUpForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Bayram
 */
public class CheckPasientFormValidator implements Validator {

    private static String type;

    public CheckPasientFormValidator(String type) {
        this.type = type;
    }

    @Override
    public boolean supports(Class<?> type) {
        return CheckPasientFormValidator.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {

        CheckPasientForm form = (CheckPasientForm) o;

        if (Constants.VALIDATOR_ADD.equals(type)) {
            if (form.getPatientServiceOperationId() == null || form.getPatientServiceOperationId() <= 0) {
                errors.reject("Invalid patient service operation id!");
            }

            for (CheckUpForm f : form.getCheckUp()) {

                if (f.getId() == null || f.getId() <= 0) {
                    errors.reject("invalid check up id!");
                }

            }
        }

    }

}
