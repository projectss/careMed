/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.controller;

import az.neuron.dev.rs.caremed.domain.FileWrapper;
import az.neuron.dev.rs.caremed.domain.MultilanguageString;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.User;
import az.neuron.dev.rs.caremed.enums.Constants;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.exception.InvalidParametersException;
import az.neuron.dev.rs.caremed.form.AbroadForm;
import az.neuron.dev.rs.caremed.form.AcademicInfoForm;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.AwardForm;
import az.neuron.dev.rs.caremed.form.BaseForm;
import az.neuron.dev.rs.caremed.form.CaseRecordForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.DocumentForm;
import az.neuron.dev.rs.caremed.form.EducationForm;
import az.neuron.dev.rs.caremed.form.ElectronalOrgForm;
import az.neuron.dev.rs.caremed.form.EmployeeForm;
import az.neuron.dev.rs.caremed.form.EmployeeHistoryForm;
import az.neuron.dev.rs.caremed.form.EmployementForm;
import az.neuron.dev.rs.caremed.form.FileWrapperForm;
import az.neuron.dev.rs.caremed.form.MembershipForm;
import az.neuron.dev.rs.caremed.form.MilitaryInfoForm;
import az.neuron.dev.rs.caremed.form.PersonForm;
import az.neuron.dev.rs.caremed.form.PersonLanguageForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import az.neuron.dev.rs.caremed.form.ResearchForm;
import az.neuron.dev.rs.caremed.form.VacationForm;
import az.neuron.dev.rs.caremed.search.EmployeeSearchForm;
import az.neuron.dev.rs.caremed.util.Crypto;
import az.neuron.dev.rs.caremed.validator.ContactFormValidator;
import az.neuron.dev.rs.caremed.validator.EmployeeFormValidator;
import az.neuron.dev.rs.caremed.validator.EmployeePaginationFormValidator;
import az.neuron.dev.rs.caremed.validator.FileWrapperFormValidator;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 *
 * @author Bayram
 */
@RestController
@RequestMapping(value = "/employees")

public class EmployeeController extends SkeletonController {

    private static final Logger log = Logger.getLogger(EmployeeController.class);

// -------------  personun elavesi
    @PostMapping(value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse addEmployee(@RequestPart(name = "employee", required = false) EmployeeForm employeeForm,
                                            @RequestPart(name = "image", required = false) MultipartFile image,
                                            MultipartHttpServletRequest multipartHttpServletRequest,
                                            BindingResult result) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, employeeForm.getToken());
//
//            EmployeeFormValidator validator = new EmployeeFormValidator(Constants.VALIDATOR_ADD);
//            validator.validate(employeeForm, result);
//
//            if (result.hasErrors()) {
//                operationResponse.setData(result.getAllErrors());
//                operationResponse.setCode(ResultCode.INVALID_PARAMS);
//                throw new InvalidParametersException("Invalid employeeform params. EmployeeForm :" + employeeForm + ",errors: " + result.getAllErrors());
//            }

            MultiValueMap<String, MultipartFile> requestParts = multipartHttpServletRequest.getMultiFileMap();
            DocumentForm[] documents = employeeForm.getDocuments();
            List<MultipartFile> files;
            FileWrapperForm[] fileWrappers;
            String docFullPath;

            FileWrapperFormValidator fileWrapperFormValidator = new FileWrapperFormValidator();

            for (DocumentForm documentForm : documents) {
                files = requestParts.get("doc_" + documentForm.getId());

                if (files != null) {
                    if (files.size() > 5) {
                        throw new InvalidParametersException("invalid file count in document");
                    }
                    fileWrappers = new FileWrapperForm[files.size()];
                    int i = 0;

                    for (MultipartFile file : files) {
                        if (file != null && file.getSize() > 0) {
                            fileWrappers[i] = new FileWrapperForm();
                            fileWrappers[i].setFile(file);
//                            fileWrapperFormValidator.validate(fileWrappers[i], result);
//
//                            if (result.hasErrors()) {
//                                operationResponse.setData(result.getAllErrors());
//                                operationResponse.setCode(ResultCode.ERROR);
//                                throw new InvalidParametersException("Invalid file content type in teacher form. Form:  " + employeeForm + ", errors: " + result.getAllErrors());
//                            }

                            OperationResponse saveFileResponse = ftpService.saveFtpFile(rootDirectory + "/employee_docs", file, Crypto.getGuid());

                            if (saveFileResponse.getCode() == ResultCode.OK) {
                                docFullPath = (String) saveFileResponse.getData();
                                fileWrappers[i].setOriginalName(file.getOriginalFilename());
                                fileWrappers[i].setPath(docFullPath);
                                i++;
                            }
                        }
                    }

                    documentForm.setUploads(fileWrappers);
                }
            }

            employeeForm.setDocuments(documents);

            String fullPath = null;

            if (image != null && image.getSize() > 0) {
                OperationResponse saveImageResponse = ftpService.saveFtpFile(rootDirectory + "/employees", image, Crypto.getGuid());

                if (saveImageResponse.getCode() == ResultCode.OK) {
                    fullPath = (String) saveImageResponse.getData();
                }
            }

            operationResponse = service.addEmployee(employeeForm, fullPath, image != null ? image.getOriginalFilename() : "");

        } catch (Exception e) {

            log.error(e.getMessage(), e);

        }

        return operationResponse;
    }
    // --------------------------------------------------------------------------------------------------------------------------------
    
    
    /// --------- Employee ilkin siyahisi , 5 column
    @PostMapping
    protected OperationResponse getEmployeeList(EmployeeSearchForm form,
                                                BindingResult result) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, form.getToken());

            EmployeePaginationFormValidator validator = new EmployeePaginationFormValidator();
            validator.validate(form, result);

            operationResponse.setData(service.getEmployeeList(form));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    // -----------------------------------------------------------------------------------
    
    //// -------- bir persona gore ,personun melumatlari cixardir 
    @GetMapping(value = "/{id:\\d+}")
    protected OperationResponse getEmployeeDetails(BaseForm form,
            @PathVariable("id") int personId) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, form.getToken());

            operationResponse.setData(service.getEmployeeDetails(form.getToken(), personId));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    // ------------------------------------------------------------------------------------------
    
    // ---------- Sag panelin person informaciyasi 
    @GetMapping(value = "/{id:\\d+}/info")
    protected OperationResponse getEmployeePersonalInfo(BaseForm form,
            @PathVariable("id") int personId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {

            User user = checkToken(operationResponse, form.getToken());

            operationResponse.setData(service.getEmployeePersonalInfo(form.getToken(), personId));
            operationResponse.setMessage(new MultilanguageString("succsess", "succsess", "succsess"));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    // sekiller
    
    @GetMapping(value = "/{id:\\d+}/image", produces = MediaType.IMAGE_JPEG_VALUE) //OK
    protected byte[] getEmployeeImage(@PathVariable int id,
            BaseForm form) {

        try {
            User user = checkToken(form.getToken());
            log.info("/employee/{id}/image [GET]. Id: " + id + ", User: " + user + ", Form: " + form);

            FileWrapper image = service.getEmployeeImage(id);

            if (image != null) {
                return ftpService.downloadFtpFile(image.getPath());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
    // -----------------------------------------------------------------------------------------------------
    
    // pinkodu yoxlamasi
    
   @GetMapping(value = "/pincode/{pincode}")
    protected OperationResponse checkPincode(@PathVariable("pincode") String pincode,
            BaseForm form) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(form.getToken());

            log.info("employee/pincode/{pincode} [GET]. Form: " + form + ", pincode: " + pincode);

            operationResponse = service.checkPincode(pincode);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    // person employee : edit, add , remove
    
    
     @PostMapping(value="/{id:\\d+}/job/edit")
    protected  OperationResponse editEmployee (EmployeeForm form, 
                                                @PathVariable("id")  int id){
        
        OperationResponse operationResponse = new  OperationResponse(ResultCode.ERROR);
        
        try{
            User user = checkToken(form.getToken());
            form.setId(id);
            operationResponse = service.editEmployee(form);
            log.info("employee /{id:\\d+}/job/edit [POST]. Form: " + form + ", person id: " + id + ", User: " + user);
        }catch(Exception e){
            log.error(e.getMessage(),e);
        }
        
        return operationResponse;
    }

    // Person contakt : edit
//                   add   
//                   remove ------------------------------------------------------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/contact/{contactId:\\d+}/edit")
    protected OperationResponse editContact(BaseForm baseForm,
            ContactForm contactForm,
            @PathVariable("id") int id,
            @PathVariable("contactId") int contactId,
             BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            
            ContactFormValidator validator= new ContactFormValidator(Constants.VALIDATOR_EDIT);
            validator.validate(contactForm, result);
            
            if(result.hasErrors()){
            operationResponse.setData(result.getAllErrors());
            operationResponse.setCode(ResultCode.INVALID_PARAMS);
            throw new InvalidParametersException("Invalid Employee form params. Form:  " + contactForm + ", errors: " + result.getAllErrors());
            }
            
            
            contactForm.setId(contactId);
            operationResponse = service.editPesonContact(baseForm.getToken(), id, contactForm);
            log.info("employee /{id:\\d+}/contact/edit [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

 
    @PostMapping(value = "/{id:\\d+}/contact/add")
    protected OperationResponse addCotanct(BaseForm baseForm,
            ContactForm form, @PathVariable("id") int id,
             BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            
            ContactFormValidator validator = new ContactFormValidator(Constants.VALIDATOR_ADD);
            validator.validate(form, result);
            
            if(result.hasErrors()){
            operationResponse.setData(result.getAllErrors());
            operationResponse.setCode(ResultCode.INVALID_PARAMS);
            throw new InvalidParametersException("Invalid Employee form params. Form:  " + form + ", errors: " + result.getAllErrors());
            
            }
            
            form.setId(id);
            operationResponse = service.addPersonContact(baseForm.getToken(), id, form);
            log.info("employee /{id:\\d+}/contact/add [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }


    @PostMapping(value = "/{id:\\d+}/contact/{contactId:\\d+}/remove")
    protected OperationResponse removePersonContact(BaseForm baseform,
            ContactForm form, @PathVariable("id") int id, @PathVariable("contactId") int contactId,
            BindingResult result) {
        
        
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseform.getToken());
        
            ContactFormValidator validator = new ContactFormValidator(Constants.VALIDATOR_REMOVE);
            validator.validate(form, result);
            
            if(result.hasErrors()){
            operationResponse.setData(result.getAllErrors());
            operationResponse.setCode(ResultCode.INVALID_PARAMS);
            throw new InvalidParametersException("Invalid Employee form params. Form:  " + form + ", errors: " + result.getAllErrors());
            
            }
            
            form.setId(id);
            operationResponse = service.removePersonContact(baseform.getToken(), id, contactId);
            log.info("employee /{id:\\d+}/contact/remove [POST]. Form: " + form + ", contact id: " + id + ", User: " + user);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

// ----------------------------------------------------------------------------------------------------------------------------    
    
    
    // Person Address edit , add, remove 
    
    
    @PostMapping(value="/{id:\\d+}/address/{addressId:\\d+}/edit")
    protected OperationResponse editPersonAddress ( BaseForm baseForm,
                                                    AddressForm form,
                                                    @PathVariable("id") int id,
                                                    @PathVariable("addressId") int addressId){
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(addressId);
            operationResponse = service.editPersonAddress(baseForm.getToken(), form);
            log.info("employee /{id:\\d+}/language/edit [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }
        
    return operationResponse;
    }

    @PostMapping(value = "/{id:\\d+}/address/add")
    protected OperationResponse addPersonAddress(BaseForm baseForm,
            AddressForm form,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            log.info("employee /{id:\\d+}/address/add [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);

            operationResponse = service.addPersonAddress(baseForm.getToken(), id, form);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @PostMapping(value = "/{id:\\d+}/address/{addressId:\\d+}/remove")
    protected OperationResponse removePersonAddress(BaseForm baseForm,
            AddressForm form,
            @PathVariable("id") int id,
            @PathVariable("addressId") int addressId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            operationResponse = service.removePersonAddress(baseForm.getToken(), id, addressId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
   // -------------------------------------------------------------------------------------------------------
   // ------------------------------------------Relation : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/relation/add")
    protected OperationResponse addPersonRealtionShip(BaseForm baseForm,
                                                       PersonRelationForm form, 
                                                       @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/relation/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse = service.addPersonRelationShip(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/relation/{relationId:\\d+}/edit")
    protected OperationResponse editPersonRealtionShip(BaseForm baseForm,
                                                       PersonRelationForm form, 
                                                       @PathVariable("id") int id, 
                                                       @PathVariable("relationId") int relationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(relationId);
            operationResponse = service.editPersonRelationship(baseForm.getToken(), id, form);
            log.info("employee /{id:\\d+}/relation/edit [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/relation/{relationId:\\d+}/remove")
    protected OperationResponse removePersonRealtionShip(BaseForm baseForm,
                                                       @PathVariable("id") int id, 
                                                       @PathVariable("relationId") int relationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removePersonRelationship(baseForm.getToken(), id, relationId);
            log.info("employee /{id:\\d+}/relation/edit [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/relation")
    protected OperationResponse getPersonRealtionShip(BaseForm baseForm,
                                                       @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/relation [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getPersonRealtionship(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------

    @GetMapping(value = "/{id:\\d+}/address")
    protected OperationResponse getPersonAddress(BaseForm baseForm,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            log.info("/{id:\\d+}/address [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getPersonAddress(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }



    @PostMapping(value = "/{id:\\d+}/personalInfo/edit", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse editPersonalInfo(@RequestPart(name = "employee1", required = false) PersonForm form,
                                            @RequestPart(name = "image", required = false) MultipartFile image,
                                            MultipartHttpServletRequest multipartHttpServletRequest
                                                
                                           ) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/{id:\\d+}/personalInfo/edit [POST]. Form: " + form + ", person id: " + ", User: " + user);
            
            String fullPath = null;

            if (image != null && image.getSize() > 0) {
                OperationResponse saveImageResponse = ftpService.saveFtpFile(rootDirectory + "/employees", image, Crypto.getGuid());

                if (saveImageResponse.getCode() == ResultCode.OK) {
                    fullPath = (String) saveImageResponse.getData();
                }
            }
            operationResponse.setData(service.editPersonalInfo(form.getToken(), form,fullPath, image != null ? image.getOriginalFilename() : ""));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
        
    }

    @PostMapping(value = "/{id:\\d+}/membershipInfo/edit")
    protected OperationResponse editMembershipInfo(MembershipForm form, @PathVariable("id") int id) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(form.getToken());
            log.info("/{id:\\d+}/membershipInfo/edit [POST]. Form: " + form + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.editMembershipInfo(form.getToken(), form));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @GetMapping(value = "/{id:\\d+}/contact")
    protected OperationResponse getPersonContact(BaseForm baseForm,
            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(baseForm.getToken());

            log.info("/{id:\\d+}/contact [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getPersonContact(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    // ------------------------------------------Employment : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/employment/add")
    protected OperationResponse addEmployment(BaseForm baseForm,
                                                       EmployementForm form, 
                                                       @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/employment/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse = service.addEmployement(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/employment/{employmentId:\\d+}/edit")
    protected OperationResponse editEmployement(BaseForm baseForm,
                                                       EmployementForm form, 
                                                       @PathVariable("id") int id, 
                                                       @PathVariable("employmentId") int employmentId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(employmentId);
            operationResponse = service.editEmployement(baseForm.getToken(), id, form);
            log.info("employee /{id:\\d+}/employment/edit [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/employment/{employmentId:\\d+}/remove")
    protected OperationResponse removeEmployment(BaseForm baseForm,
                                                       @PathVariable("id") int id, 
                                                       @PathVariable("employmentId") int employmentId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removeEmployment(baseForm.getToken(), id, employmentId);
            log.info("employee /{id:\\d+}/employmentId/remove [POST]. Form: " + baseForm + ", contact id: " + id + ", User: " + user + ", employmentId: " + employmentId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/employment")
    protected OperationResponse getEmployment(BaseForm baseForm,
                                              @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/employment [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getEmploymentList(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    
    // ------------------------------------------Abroad : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/abroad/add")
    protected OperationResponse addAbroad(BaseForm baseForm,
                                            AbroadForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/abroad/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", Form: " + form);
            operationResponse = service.addAbroadInfo(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/abroad/{abroadId:\\d+}/edit")
    protected OperationResponse editAbroad(BaseForm baseForm,
                                            AbroadForm form, 
                                            @PathVariable("id") int id, 
                                            @PathVariable("abroadId") int abroadId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(abroadId);
            log.info("employee /{id:\\d+}/abroad/edit [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", form: " + form);
            operationResponse = service.editAbroadInfo(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/abroad/{abroadId:\\d+}/remove")
    protected OperationResponse removeAbroad(BaseForm baseForm,
                                            @PathVariable("id") int id, 
                                            @PathVariable("abroadId") int abroadId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removeAbroadInfo(baseForm.getToken(), id, abroadId);
            log.info("employee /{id:\\d+}/abroad/{abroadId:\\d+}/remove [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", abroadId: " + abroadId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/abroad")
    protected OperationResponse getAbroad(BaseForm baseForm,
                                        @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/abroad [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getAbroadInfo(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    // ------------------------------------------Vacation : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/vacation/add")
    protected OperationResponse addVacation(BaseForm baseForm,
                                            VacationForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/vacation/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", Form: " + form);
            operationResponse = service.addVacationRecord(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/vacation/{vacationId:\\d+}/edit")
    protected OperationResponse editVacation(BaseForm baseForm,
                                            VacationForm form, 
                                            @PathVariable("id") int id, 
                                            @PathVariable("vacationId") int vacationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(vacationId);
            log.info("employee /{id:\\d+}/vacation/edit [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", form: " + form);
            operationResponse = service.editVacationRecord(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/vacation/{vacationId:\\d+}/remove")
    protected OperationResponse removeVacation(BaseForm baseForm,
                                            @PathVariable("id") int id, 
                                            @PathVariable("vacationId") int vacationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removeVacationRecord(baseForm.getToken(), id, vacationId);
            log.info("employee /{id:\\d+}/vacation/{vacationId:\\d+}/remove [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", abroadId: " + vacationId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/vacation")
    protected OperationResponse getVacation(BaseForm baseForm,
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/vacation [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getVacationList(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    // ------------------------------------------Case Record : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/caserecord/add")
    protected OperationResponse addCaseRecord(BaseForm baseForm,
                                            CaseRecordForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/casereciord/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", Form: " + form);
            operationResponse = service.addCaseRecord(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/caserecord/{caserecordId:\\d+}/edit")
    protected OperationResponse editCaseRecord(BaseForm baseForm,
                                            CaseRecordForm form, 
                                            @PathVariable("id") int id, 
                                            @PathVariable("caserecordId") int caserecordId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(caserecordId);
            log.info("employee /{id:\\d+}/caserecord/edit [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", form: " + form);
            operationResponse = service.editCaseRecord(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/caserecord/{caserecordId:\\d+}/remove")
    protected OperationResponse removeCaseRecord(BaseForm baseForm,
                                            @PathVariable("id") int id, 
                                            @PathVariable("caserecordId") int caserecordId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removeCaseRecord(baseForm.getToken(), id, caserecordId);
            log.info("employee /{id:\\d+}/caserecord/{caserecordId:\\d+}/remove [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", abroadId: " + caserecordId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/caserecord")
    protected OperationResponse getCaseRecord(BaseForm baseForm,
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/caserecord [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getCaseList(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    // ------------------------------------------Award : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/award/add")
    protected OperationResponse addAward(BaseForm baseForm,
                                            AwardForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/award/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", Form: " + form);
            operationResponse = service.addAward(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/award/{awardId:\\d+}/edit")
    protected OperationResponse editAward(BaseForm baseForm,
                                            AwardForm form, 
                                            @PathVariable("id") int id, 
                                            @PathVariable("awardId") int awardId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(awardId);
            log.info("employee /{id:\\d+}/award/edit [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", form: " + form);
            operationResponse = service.editAward(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/award/{awardId:\\d+}/remove")
    protected OperationResponse removeAward(BaseForm baseForm,
                                            @PathVariable("id") int id, 
                                            @PathVariable("awardId") int awardId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removeAward(baseForm.getToken(), id, awardId);
            log.info("employee /{id:\\d+}/award/{awardId:\\d+}/remove [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", awardId: " + awardId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/award")
    protected OperationResponse getAward(BaseForm baseForm,
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/award [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getAwardInfo(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    
    
    
    // ------------------------------------------ Electronal Organization : add, edit remove, get----------------------------------------------------
    
    @PostMapping(value = "/{id:\\d+}/electronal/add")
    protected OperationResponse addElectronalOrganization(ElectronalOrgForm form, 
                                                          @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(form.getToken());
            form.setPersonId(id);
            
            log.info("employee /{id:\\d+}/electronal/add [POST]. Form: " + form + ", person id: " + id + ", User: " + user);
            operationResponse = service.addElectronalOrgaznization(form.getToken(), form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/electronal/{elecOrgId:\\d+}/edit")
    protected OperationResponse editElectronalOrganization(ElectronalOrgForm form,
                                                            @PathVariable("id") int id, 
                                                            @PathVariable("elecOrgId") int elecOrgId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(form.getToken());

            form.setId(elecOrgId);
            log.info("/{id:\\d+}/electronal/{elecOrgId:\\d+}/edit [POST]. Form: " + form + ", person id: " + id + ", User: " + user);
            operationResponse = service.editElectronalOrganization(form.getToken(), form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @PostMapping(value = "/{id:\\d+}/electronal/{elecOrgId:\\d+}/remove")
    protected OperationResponse removeElectronalOrganization(ElectronalOrgForm form,
                                                            @PathVariable("id") int id, 
                                                            @PathVariable("elecOrgId") int elecOrgId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(form.getToken());
            operationResponse = service.removeElectronalOrganization(form.getToken(), id, elecOrgId);
            log.info("/{id:\\d+}/electronal/{elecOrgId:\\d+}/remove [POST]. Form: " + form + ", person id: " + id + ", User: " + user + ", elecOrgId: " + elecOrgId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @GetMapping(value = "/{id:\\d+}/electronalOrg")
    protected OperationResponse getElectronalOrg(BaseForm baseForm,
                                                @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/electronalOrg [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getElectronalOrgList(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    // -------------------------------------------------------------------------------------------------------
    
    // ------------------------------------------ Military : add, edit remove, get----------------------------------------------------
    
    @PostMapping(value = "/{id:\\d+}/military/add")
    protected OperationResponse addMilitary(MilitaryInfoForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(form.getToken());
            form.setPersonId(id);
            
            log.info("employee /{id:\\d+}/military/add [POST]. Form: " + form + ", person id: " + id + ", User: " + user);
            operationResponse = service.addMilitary(form.getToken(), form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/military/{militaryId:\\d+}/edit")
    protected OperationResponse editMilitary(MilitaryInfoForm form,
                                                            @PathVariable("id") int id, 
                                                            @PathVariable("militaryId") int militaryId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(form.getToken());

            form.setId(militaryId);
            log.info("/{id:\\d+}/military/{militaryId:\\d+}/edit [POST]. Form: " + form + ", person id: " + id + ", User: " + user);
            operationResponse = service.editMillitaryInfo(form.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @PostMapping(value = "/{id:\\d+}/military/{militaryId:\\d+}/remove")
    protected OperationResponse removeMilitary(BaseForm form,
                                                @PathVariable("id") int id, 
                                                @PathVariable("militaryId") int militaryId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(form.getToken());
            operationResponse = service.removeMillitaryInfo(form.getToken(), id, militaryId);
            log.info("/{id:\\d+}/military/{militaryId:\\d+}/remove [POST]. Form: " + form + ", person id: " + id + ", User: " + user + ", elecOrgId: " + militaryId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @GetMapping(value = "/{id:\\d+}/military")
    protected OperationResponse getMilitary(BaseForm baseForm,
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/military [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getMilitaryList(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    // ------------------------------------------Education : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/education/add")
    protected OperationResponse addEducation(BaseForm baseForm,
                                            EducationForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/education/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", Form: " + form);
            operationResponse = service.addEducation(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/education/{educationId:\\d+}/edit")
    protected OperationResponse editEducation(BaseForm baseForm,
                                            EducationForm form, 
                                            @PathVariable("id") int id, 
                                            @PathVariable("educationId") int educationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(educationId);
            log.info("employee /{id:\\d+}/education/edit [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", form: " + form);
            operationResponse = service.editEducation(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/education/{educationId:\\d+}/remove")
    protected OperationResponse removeEducation(BaseForm baseForm,
                                            @PathVariable("id") int id, 
                                            @PathVariable("educationId") int educationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removeEducation(baseForm.getToken(), id, educationId);
            log.info("employee /{id:\\d+}/education/{educationId:\\d+}/remove [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", educationId: " + educationId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/education")
    protected OperationResponse getEducation(BaseForm baseForm,
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/education [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getPersonEducation(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    // ------------------------------------------Language : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/language/add")
    protected OperationResponse addLanguage(BaseForm baseForm,
                                            PersonLanguageForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/language/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", Form: " + form);
            operationResponse = service.addPersonLanguage(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/language/{languageId:\\d+}/edit")
    protected OperationResponse editLanguage(BaseForm baseForm,
                                            PersonLanguageForm form, 
                                            @PathVariable("id") int id, 
                                            @PathVariable("languageId") int languageId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(languageId);
            log.info("employee /{id:\\d+}/language/edit [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", form: " + form);
            operationResponse = service.editPersonLanguage(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/language/{languageId:\\d+}/remove")
    protected OperationResponse removeLanguage(BaseForm baseForm,
                                            @PathVariable("id") int id, 
                                            @PathVariable("languageId") int languageId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removePersonLanguage(baseForm.getToken(), id, languageId);
            log.info("employee /{id:\\d+}/language/{languageId:\\d+}/remove [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", educationId: " + languageId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/language")
    protected OperationResponse getLanguage(BaseForm baseForm,
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/language [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getPersonLanguage(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    // ------------------------------------------Academic info : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/academic/add")
    protected OperationResponse addAcademicINfo(BaseForm baseForm,
                                            AcademicInfoForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/language/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", Form: " + form);
            operationResponse = service.addAcademicInfo(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/academic/{academicId:\\d+}/edit")
    protected OperationResponse editAcademicInfo(BaseForm baseForm,
                                            AcademicInfoForm form, 
                                            @PathVariable("id") int id, 
                                            @PathVariable("academicId") int academicId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(academicId);
            log.info("employee /{id:\\d+}/academic/edit [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", form: " + form);
            operationResponse = service.editAcademicInfo(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/academic/{academicId:\\d+}/remove")
    protected OperationResponse removeAcademicİnfo(BaseForm baseForm,
                                            @PathVariable("id") int id, 
                                            @PathVariable("academicId") int academicId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removeAcademicInfo(baseForm.getToken(), id, academicId);
            log.info("employee /{id:\\d+}/academic/{academicId:\\d+}/remove [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", educationId: " + academicId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/academic")
    protected OperationResponse getAcademicInfo(BaseForm baseForm,
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/academic [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getPersonAcademicInfo(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    // ------------------------------------------Research : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/research/add")
    protected OperationResponse addResearch(BaseForm baseForm,
                                            ResearchForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/research/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", Form: " + form);
            operationResponse = service.addPersonResearchInfo(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/research/{researchId:\\d+}/edit")
    protected OperationResponse editResearch(BaseForm baseForm,
                                            ResearchForm form, 
                                            @PathVariable("id") int id, 
                                            @PathVariable("researchId") int researchId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(researchId);
            log.info("employee /{id:\\d+}/research/edit [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", form: " + form);
            operationResponse = service.editResearchInfo(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/research/{researchId:\\d+}/remove")
    protected OperationResponse removeResearch(BaseForm baseForm,
                                            @PathVariable("id") int id, 
                                            @PathVariable("researchId") int researchId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            operationResponse = service.removeResearchInfo(baseForm.getToken(), id, researchId);
            log.info("employee /{id:\\d+}/research/{researchId:\\d+}/remove [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", educationId: " + researchId);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/research")
    protected OperationResponse getResearch(BaseForm baseForm,
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/research [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getResearchList(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    // ------------------------------------------Research : add, edit remove, get----------------------------------------------------
    @PostMapping(value = "/{id:\\d+}/document/add")
    protected OperationResponse addDocument(BaseForm baseForm,
                                            DocumentForm form, 
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/document/add [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", Form: " + form);
            operationResponse = service.addDocument(form, baseForm.getToken(), id);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/document/{documentId:\\d+}/edit")
    protected OperationResponse editDocument(BaseForm baseForm,
                                            DocumentForm form, 
                                            @PathVariable("id") int id, 
                                            @PathVariable("documentId") int documentId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());

            form.setId(documentId);
            log.info("employee /{id:\\d+}/document/edit [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", form: " + form);
            operationResponse = service.editPersonDocument(baseForm.getToken(), id, form);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/document/{documentId:\\d+}/remove")
    protected OperationResponse removeDocument(BaseForm baseForm,
                                            @PathVariable("id") int id, 
                                            @PathVariable("documentId") int documentId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/document/{documentId:\\d+}/remove [POST]. Form: " + baseForm + ", person id: " + id + ", User: " + user + ", educationId: " + documentId);
            operationResponse = service.removePersonDocument(baseForm.getToken(), id, documentId);
            List<FileWrapper> list = (List) operationResponse.getData();
            
            for(FileWrapper f: list) {
                operationResponse = ftpService.removeFtpFile(f.getPath());
            }
            
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/{id:\\d+}/document")
    protected OperationResponse getDocument(BaseForm baseForm,
                                            @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(baseForm.getToken());
            log.info("employee /{id:\\d+}/document [GET]. Form: " + baseForm + ", person id: " + id + ", User: " + user);
            operationResponse.setData(service.getPersonDocument(baseForm.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
            

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

     // -------------------------------------------------------------------------------------------------------
    
    @PostMapping(value = "/{id:\\d+}/document/{docId:\\d+}/file/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE) //OK
    protected OperationResponse addFiles(BaseForm form,
                                        @PathVariable("id") int id,
                                        @PathVariable("docId") int docId,
                                        MultipartHttpServletRequest multipartHttpServletRequest,
                                        @RequestPart(name = "file", required = false) MultipartFile file,
                                        BindingResult result) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, form.getToken());

            log.info("{id:\\d+}/document/{docId:\\d+}/file/add [POST]. User: " + user + ", Form: " + form);

            MultiValueMap<String, MultipartFile> requestParts = multipartHttpServletRequest.getMultiFileMap();
            List<MultipartFile> files;
            String docFullPath;
            DocumentForm documentForm = new DocumentForm();
            files = requestParts.get("file");
            if (files.size() > 5) {
                throw new InvalidParametersException("invalid file count in document");
            }
            FileWrapperForm[] fileWrappers = new FileWrapperForm[files.size()];
            FileWrapperFormValidator fileWrapperFormValidator = new FileWrapperFormValidator();
            int i = 0;
            for (MultipartFile multipartFile : files) {

                if (multipartFile != null && multipartFile.getSize() > 0) {
                    fileWrappers[i] = new FileWrapperForm();
                    fileWrappers[i].setFile(file);
                    fileWrapperFormValidator.validate(fileWrappers[i], result);

                    if (result.hasErrors()) {
                        operationResponse.setData(result.getAllErrors());
                        operationResponse.setCode(ResultCode.ERROR);
                        throw new InvalidParametersException("invalid file content type in teacher form. Form: " + form + ", errors: " + result.getAllErrors());
                    }
                    OperationResponse saveFileResponse = ftpService.saveFtpFile(rootDirectory+"/employee_docs", multipartFile, Crypto.getGuid());

                    if (saveFileResponse.getCode() == ResultCode.OK) {
                        docFullPath = (String) saveFileResponse.getData();
                        fileWrappers[i].setOriginalName(multipartFile.getOriginalFilename());
                        fileWrappers[i].setPath(docFullPath);

                    }
                }
                documentForm.setUploads(fileWrappers);
                i = i + 1;
            }
            documentForm.setId(docId);
            operationResponse = service.addDocFile(documentForm, form.getToken());
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @GetMapping(value = "/file/{id:\\d+}", produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.APPLICATION_PDF_VALUE}) //OK
    protected byte[] getFile(@PathVariable int id,
            BaseForm form,
            HttpServletResponse response) {

        try {
            FileWrapper file = service.getFileByFileId(id);
            if (file != null) {
                response.setHeader("Content-Disposition", "attachment; filename=" + file.getOriginalName());
                return ftpService.downloadFtpFile(file.getPath());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
    
    @PostMapping("/{id:\\d+}/document/{docId:\\d+}/file/{fileId:\\d+}/remove") //OK
    protected OperationResponse removeTeacherDocFiles(@PathVariable("id") int id,
            @PathVariable("docId") int docId,
            @PathVariable("fileId") int fileId,
            BaseForm form) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, form.getToken());

            log.info("/teachers/{id:\\d+}/document/file/{fileId:\\d+}/remove [POST]. Form: " + form + ", teacher id: " + id + ", User: " + user);

            operationResponse = service.removeDocFile(form.getToken(), docId, fileId);
            
            FileWrapper file = (FileWrapper) operationResponse.getData();
            if (operationResponse.getCode() == ResultCode.OK) {
                if(file != null) {
                  operationResponse = ftpService.removeFtpFile(file.getPath());
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    
    
        
        @PostMapping(value = "/{id:\\d+}/remove")
        protected OperationResponse removeEmployee(@PathVariable("id") int id,
                EmployeeHistoryForm historyForm ,
                BindingResult result){
             OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
                                                
            try{
                  User user = checkToken(operationResponse, historyForm.getToken());

            log.info("{id:\\d+}/remove/employee/ [POST]. Form: " + historyForm + ", employee id: " + id + ", User: " + user);
            
            operationResponse=service.removeEmployee(id,historyForm);
            
            }catch(Exception e){
            
            log.error(e.getMessage(),e);
            }
             
             
             
             return operationResponse;
                                            
        }

}
