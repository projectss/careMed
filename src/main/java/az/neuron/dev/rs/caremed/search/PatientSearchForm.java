/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.search;

import oracle.jdbc.babelfish.BabelfishCallableStatement;

/**
 *
 * @author Bayram
 */
public class PatientSearchForm  {

    private String orderColumn = "id";
    private String orderType = "asc";
    private Integer start = 0;
    private Integer length = 10;
    private String patientDocNum;
    private Integer genderId;

    public PatientSearchForm() {
    }

    
    
    
    public String getOrderColumn() {
        return orderColumn;
    }

    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getPatientDocNum() {
        return patientDocNum;
    }

    public void setPatientDocNum(String patientDocNum) {
        this.patientDocNum = patientDocNum;
    }

    public Integer getGenderId() {
        return genderId;
    }

    public void setGenderId(Integer genderId) {
        this.genderId = genderId;
    }

    @Override
    public String toString() {
        return "PatientSearchForm{" + "orderColumn=" + orderColumn + ", orderType=" + orderType + ", start=" + start + ", length=" + length + ", patientDocNum=" + patientDocNum + ", genderId=" + genderId + '}';
    }



}
