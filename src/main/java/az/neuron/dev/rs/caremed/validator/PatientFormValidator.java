/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.validator;

import az.neuron.dev.rs.caremed.enums.Constants;
import az.neuron.dev.rs.caremed.enums.Regex;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.DocumentForm;
import az.neuron.dev.rs.caremed.form.PatientForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Bayram
 */
public class PatientFormValidator implements Validator{
    public static String type;

    public PatientFormValidator(String type) {
        this.type=type;
    }
    

    @Override
    public boolean supports(Class<?> type) {
        return PatientForm.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PatientForm patientForm = (PatientForm)o;
        
        if( Constants.VALIDATOR_ADD.equals(type)){
        if (patientForm.getFistName() == null || patientForm.getFistName().isEmpty()) {
                errors.reject("empty patient firstname");

            }
            if (patientForm.getLastName() == null || patientForm.getLastName().isEmpty()) {

                errors.reject("empty patient lastname");
            }
            if (patientForm.getMiddleName() == null || patientForm.getMiddleName().isEmpty()) {
                errors.reject("empty patient middlename");

            }
            if (patientForm.getGenderId() <= 0) {

                errors.reject("empty patient gender id");

            }
            if (patientForm.getBirthdate() == null || patientForm.getBirthdate().trim().isEmpty() || !patientForm.getBirthdate().matches(Regex.DATE)) {
                errors.reject("empty birthdate");
            }

            if (patientForm.getCitizenshipId() <= 0) {
                errors.reject("empty citizenship id");

            }

            if (patientForm.getSvSeriya() <= 0) {

                errors.reject("empty sv_seriya ");
            }

            if (patientForm.getSeriya() == null || patientForm.getSeriya().isEmpty()) {

                errors.reject("empty seriya");
            }

            if (patientForm.getSupplyOrg() <= 0) {

                errors.reject("empty supply organization name");
            }

            if (patientForm.getMaritalStatusId() <= 0) {
                errors.reject("empty marital status id");
            }

            if (patientForm.getChildCount() == null || patientForm.getChildCount() < 0) {

                errors.reject("empty child count");
            }
            if (patientForm.getMillitaryServiceId() <= 0) {
                errors.reject("empty millitary service id");
            }

            if (patientForm.getNationalityId() <= 0) {
                errors.reject("empty nationality id");
            }

            if (patientForm.getSocialStatusId() <= 0) {

                errors.reject("empty social status id");
            }

            if (patientForm.getBloodGroupId() <= 0) {

                errors.reject("empty blood id");
            }
            

       // ------------------- address validation ---------------------------
            if (patientForm.getAddresses().length > 0) {
                for (AddressForm form : patientForm.getAddresses()) {

                    if (form.getAddressId() <= 0) {
                        errors.reject("empty address id");
                    }
                    if (form.getTypeId() <= 0) {
                        errors.reject("empty type id");
                    }
                    if (form.getTypeId() != 1000026 && form.getAddress().trim().isEmpty()) {

                        errors.reject("empty birth place");
                    }

                }
            }

       // --------------------------------------------------------------------
       // ----------- contact validation ----------------------------------------
            if (patientForm.getContacts().length > 0) {

                for (ContactForm form : patientForm.getContacts()) {

                    if (form.getContact() == null || form.getContact().isEmpty()) {

                        errors.reject("empty patient contact");
                    }

                    if (form.getTypeId() <= 0) {
                        errors.reject("empty contact type id");
                    }

                }

            }

       //-------------------------------------------------------------------     
       //----------------- document validation -------------------------------------
            if (patientForm.getDocuments().length > 0) {

                for (DocumentForm form : patientForm.getDocuments()) {
                    if (form.getTypeId() <= 0) {

                        errors.reject("empty document type id");
                    }

                    if (form.getSerial() == null || form.getSerial().isEmpty()) {
                        errors.reject("empty document seria ");
                    }

                    if (form.getNumber() == null || form.getNumber().isEmpty()) {

                        errors.reject("empty document number");
                    }

                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {

                        errors.reject("empty start date");
                    }

                }

            }

       // -------------------------------------------------------------------------------------------  
            // ----------RelationShip validation --------------------------------------------------
            if (patientForm.getRelations().length > 0) {
                for (PersonRelationForm form : patientForm.getRelations()) {

                    if (form.getRelationshipId() <= 0) {
                        errors.reject("empty realtionship id");

                    }
                    if (form.getFullName() == null || form.getFullName().isEmpty()) {
                        errors.reject("empty realtionship fullname ");

                    }

                }

            }
        
        
            
            //------------------ patient validation ------------
            
            if(patientForm.getPatientDocNum()==null || patientForm.getPatientDocNum().isEmpty()){
            errors.reject("empty patient doc num");
            }
            
            if(patientForm.getRegistrationDate()==null || patientForm.getRegistrationDate().isEmpty()){
            errors.reject("empty patient registration date");
            }
            
                       
            
        }
        
    }
    
}
