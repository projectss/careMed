/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class MilitaryInfo {
    private int id;
    private DictionaryWrapper millitaryNameId;
    private DictionaryWrapper staffTypeId;
    private DictionaryWrapper armyTypeId;
    private String note;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserId;

    public MilitaryInfo() {
    }

    public MilitaryInfo(int id, DictionaryWrapper millitaryNameId, DictionaryWrapper staffTypeId, DictionaryWrapper armyTypeId, String note) {
        this.id = id;
        this.millitaryNameId = millitaryNameId;
        this.staffTypeId = staffTypeId;
        this.armyTypeId = armyTypeId;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getMillitaryNameId() {
        return millitaryNameId;
    }

    public void setMillitaryNameId(DictionaryWrapper millitaryNameId) {
        this.millitaryNameId = millitaryNameId;
    }

    public DictionaryWrapper getStaffTypeId() {
        return staffTypeId;
    }

    public void setStaffTypeId(DictionaryWrapper staffTypeId) {
        this.staffTypeId = staffTypeId;
    }

    public DictionaryWrapper getArmyTypeId() {
        return armyTypeId;
    }

    public void setArmyTypeId(DictionaryWrapper armyTypeId) {
        this.armyTypeId = armyTypeId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Override
    public String toString() {
        return "MillitaryInfo{" + "id=" + id + ", millitaryNameId=" + millitaryNameId + ", staffTypeId=" + staffTypeId + ", armyTypeId=" + armyTypeId + ", note=" + note + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }   
}
