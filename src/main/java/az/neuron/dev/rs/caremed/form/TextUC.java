/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class TextUC {
    private String Ad;
    private String Soyad;
    private String Ata;

    public String getAd() {
        return Ad;
    }

    public void setAd(String Ad) {
        this.Ad = Ad;
    }

    public String getSoyad() {
        return Soyad;
    }

    public void setSoyad(String Soyad) {
        this.Soyad = Soyad;
    }

    public String getAta() {
        return Ata;
    }

    public void setAta(String Ata) {
        this.Ata = Ata;
    }

    @Override
    public String toString() {
        return "TextUC{" + "Ad=" + Ad + ", Soyad=" + Soyad + ", Ata=" + Ata + '}';
    }
    
}
