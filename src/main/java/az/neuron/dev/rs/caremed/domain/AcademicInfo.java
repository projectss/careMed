/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class AcademicInfo {
private int id;
private DictionaryWrapper typeId;
private DictionaryWrapper academicInfoId;
private String startDate;
private String createDate;
private int createUserId;
private String updateDate;
private int updateUserid;

    public AcademicInfo() {
    }

    public AcademicInfo(int id, DictionaryWrapper typeId, DictionaryWrapper academicInfoId, String startDate) {
        this.id = id;
        this.typeId = typeId;
        this.academicInfoId = academicInfoId;
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getTypeId() {
        return typeId;
    }

    public void setTypeId(DictionaryWrapper typeId) {
        this.typeId = typeId;
    }

    public DictionaryWrapper getAcademicInfoId() {
        return academicInfoId;
    }

    public void setAcademicInfoId(DictionaryWrapper academicInfoId) {
        this.academicInfoId = academicInfoId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserid() {
        return updateUserid;
    }

    public void setUpdateUserid(int updateUserid) {
        this.updateUserid = updateUserid;
    }

    @Override
    public String toString() {
        return "AcademicInfo{" + "id=" + id + ", typeId=" + typeId + ", academicInfoId=" + academicInfoId + ", startDate=" + startDate + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserid=" + updateUserid + '}';
    }   
}
