/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class PersonForm extends BaseForm {
    private int id;
    private String fistName;
    private String lastName;
    private String middleName;
    private int citizenshipId;
    private int maritalStatusId;
    private int millitaryServiceId;
    private int nationalityId;
    private int socialStatusId;
    private String birthdate;
    private String pincode;
    private String seriya;
    private int bloodGroupId;
    private int supplyOrg;
    private String endDate;
    private int height;
    private int genderId;
    private int svSeriya;
    private Integer childCount;
    private AddressForm[] addresses;
    private ContactForm[] contacts;
    private DocumentForm[] documents;
    private PersonRelationForm[] relations;

    public PersonForm() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getCitizenshipId() {
        return citizenshipId;
    }

    public void setCitizenshipId(int citizenshipId) {
        this.citizenshipId = citizenshipId;
    }

    public int getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setMaritalStatusId(int maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }

    public int getMillitaryServiceId() {
        return millitaryServiceId;
    }

    public void setMillitaryServiceId(int millitaryServiceId) {
        this.millitaryServiceId = millitaryServiceId;
    }

    public int getNationalityId() {
        return nationalityId;
    }

    public void setNationalityId(int nationalityId) {
        this.nationalityId = nationalityId;
    }

    public int getSocialStatusId() {
        return socialStatusId;
    }

    public void setSocialStatusId(int socialStatusId) {
        this.socialStatusId = socialStatusId;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getSeriya() {
        return seriya;
    }

    public void setSeriya(String seriya) {
        this.seriya = seriya;
    }

    public int getBloodGroupId() {
        return bloodGroupId;
    }

    public void setBloodGroupId(int bloodGroupId) {
        this.bloodGroupId = bloodGroupId;
    }

    public int getSupplyOrg() {
        return supplyOrg;
    }

    public void setSupplyOrg(int supplyOrg) {
        this.supplyOrg = supplyOrg;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getGenderId() {
        return genderId;
    }

    public void setGenderId(int genderId) {
        this.genderId = genderId;
    }

    public AddressForm[] getAddresses() {
        return addresses;
    }

    public void setAddresses(AddressForm[] addresses) {
        this.addresses = addresses;
    }

    public ContactForm[] getContacts() {
        return contacts;
    }

    public void setContacts(ContactForm[] contacts) {
        this.contacts = contacts;
    }

    public DocumentForm[] getDocuments() {
        return documents;
    }

    public void setDocuments(DocumentForm[] documents) {
        this.documents = documents;
    }

    public PersonRelationForm[] getRelations() {
        return relations;
    }

    public void setRelations(PersonRelationForm[] relations) {
        this.relations = relations;
    }

    public int getSvSeriya() {
        return svSeriya;
    }

    public void setSvSeriya(int svSeriya) {
        this.svSeriya = svSeriya;
    }

    public Integer getChildCount() {
        return childCount;
    }

    public void setChildCount(Integer childCount) {
        this.childCount = childCount;
    }

    @Override
    public String toString() {
        return "PersonForm{" + "id=" + id + ", fistName=" + fistName + ", lastName=" + lastName + ", middleName=" + middleName + ", citizenshipId=" + citizenshipId + ", maritalStatusId=" + maritalStatusId + ", millitaryServiceId=" + millitaryServiceId + ", nationalityId=" + nationalityId + ", socialStatusId=" + socialStatusId + ", birthdate=" + birthdate + ", pincode=" + pincode + ", seriya=" + seriya + ", bloodGroupId=" + bloodGroupId + ", supplyOrg=" + supplyOrg + ", endDate=" + endDate + ", height=" + height + ", genderId=" + genderId + ", svSeriya=" + svSeriya + ", childCount=" + childCount + ", addresses=" + addresses + ", contacts=" + contacts + ", documents=" + documents + ", relations=" + relations + '}';
    }

  

  

}
