/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.services;

import az.neuron.dev.rs.caremed.dao.OperationDao;
import az.neuron.dev.rs.caremed.dao.ServicesDao;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.EmployeeStatus;
import az.neuron.dev.rs.caremed.domain.FormElement;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.Services;
import az.neuron.dev.rs.caremed.form.EmpStatusForm;
import az.neuron.dev.rs.caremed.form.EmployeeStatusForm;
import az.neuron.dev.rs.caremed.form.MedicalCommitionForm;
import az.neuron.dev.rs.caremed.form.OperationForm;
import az.neuron.dev.rs.caremed.form.ServiceOperationForm;
import az.neuron.dev.rs.caremed.form.ServicesForm;
import az.neuron.dev.rs.caremed.search.PatientSearchForm;
import az.neuron.dev.rs.caremed.search.ServiceSearchForm;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import az.neuron.dev.rs.caremed.dao.IOperationDao;
import az.neuron.dev.rs.caremed.domain.PatientHistory;
import az.neuron.dev.rs.caremed.form.ConsuptionForm;
import az.neuron.dev.rs.caremed.search.ConsuptionSearchForm;
import az.neuron.dev.rs.caremed.search.PatientCommitionHistory;

/**
 *
 * @author Bayram
 */
@Service
public class OperationService implements IOperationService {

    @Autowired
    private IOperationDao operationDao;

    @Autowired
    private ServicesDao servicesDao;

    @Override
    public List<EmployeeStatus> getEmployeeStatus(String token, EmployeeStatusForm form) {
        return this.operationDao.getEmployeeStatus(token, form);
    }

    @Override
    public OperationResponse addPatientServices(String token, ServicesForm form) {
        return this.operationDao.addPatientServices(token, form);
    }

    @Override
    public OperationResponse changeEmployeeStatus(String token, EmpStatusForm empStatusForm) {
        return this.operationDao.changeEmployeeStatus(token, empStatusForm);
    }

    @Override
    public OperationResponse addServices(String token, ServicesForm form) {
        return this.servicesDao.addServices(token, form);
    }

    @Override
    public OperationResponse editServices(String token, ServicesForm form) {
        return this.servicesDao.editServices(token, form);
    }

    @Override
    public DataTable getServices(ServiceSearchForm searchForm) {
        return this.servicesDao.getServices(searchForm);
    }

    @Override
    public OperationResponse removeServices(String token, int id) {
        return this.servicesDao.removeServices(token, id);
    }

    @Override
    public Services getServicesDetails(String token, int id) {
        return this.servicesDao.getServicesDetails(token, id);
    }

    @Override
    public List<Services> getSetvicesList(String token, int servicesId) {
        return this.servicesDao.getSetvicesList(token, servicesId);
    }

    @Override
    public OperationResponse addPatientServiceOperation(String token, OperationForm operationForm) {
        return this.operationDao.addPatientServiceOperation(token, operationForm);
    }

    @Override
    public DataTable getPatientForCommition(String token, PatientSearchForm form) {
        return this.operationDao.getPatientForCommition(token, form);
    }

    @Override
    public OperationResponse addPatientMedicalCommition(String token, int patientId, OperationForm operationForm) {
        return this.operationDao.addPatientMedicalCommition(token, patientId, operationForm);
    }

    @Override
    public DataTable getCommitionHistory(String token, int patientId, PatientCommitionHistory patientCommitionHistory) {
        return this.operationDao.getCommitionHistory(token, patientId, patientCommitionHistory);
    }

    @Override
    public List<FormElement> getFormElementList(String token, int servicesId) {
        return this.servicesDao.getFormElementList(token, servicesId);
    }

    @Override
    public List<PatientHistory> getServicesForMedicalCommition(String token, int commitionId) {
        return this.operationDao.getServicesForMedicalCommition(token, commitionId);
    }

    @Override
    public OperationResponse addConsuption(String token, ConsuptionForm form) {
        return this.servicesDao.addConsuption(token, form);
    }

    @Override
    public DataTable getConsuption(String token, ConsuptionSearchForm consuptionSearchForm) {
        return this.servicesDao.getConsuption(token, consuptionSearchForm);
    }

    @Override
    public OperationResponse editConsuption(String token, ConsuptionForm form) {
        return this.servicesDao.editConsuption(token, form);
    }

    @Override
    public OperationResponse removeConsuption(String token, int id, String note) {
        return this.servicesDao.removeConsuption(token, id, note);
    }

    @Override
    public OperationResponse checkCodeConsuption(String token, String code) {
        return this.servicesDao.checkCodeConsuption(token, code);
    }

}
