/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

import java.util.List;

/**
 *
 * @author Bayram
 */
public class Employee extends Person{
    private int employeeid;
    private String custNum;
    private DictionaryWrapper specialityId;
    private DictionaryWrapper positionId;
    private DictionaryWrapper positionCategoryId;
    private DictionaryWrapper orgId;
    private DictionaryWrapper statId;
    private DictionaryWrapper payId;
    private String startDate;
    private String note;
    private int membershipId;
    private DictionaryWrapper membershipOrgName;
    private String membershipPeriod;
    private String membershipCardNum;
    private String membershipNote;
    private List<AbroadInfo> abroadInfos;
    private List<AcademicInfo> academicInfos;
    private List<AwardInfo> awardInfos;
    private List<Education> educations;
    private List<ElectronalOrganization> electronalOrganizations;
    private List<Employment> employments;
    private List<MilitaryInfo> millitaryInfos;
    private List<ResearchInfo> researchInfos;
    private List<Vacation> vacations;
    private List<CaseRecord> caseRecord;

    public Employee() {
    }

    public List<Vacation> getVacations() {
        return vacations;
    }

    public void setVacations(List<Vacation> vacations) {
        this.vacations = vacations;
    }

    public List<CaseRecord> getCaseRecord() {
        return caseRecord;
    }

    public void setCaseRecord(List<CaseRecord> caseRecord) {
        this.caseRecord = caseRecord;
    }


    public Employee(int employeeid, String custNum, DictionaryWrapper specialityId, DictionaryWrapper positionId, DictionaryWrapper positionCategoryId, DictionaryWrapper orgId, DictionaryWrapper statId, DictionaryWrapper payId, String startDate, String note,int membershipId, DictionaryWrapper membershipOrgName, String membershipPeriod, String membershipCardNum,String membershipNote, List<AbroadInfo> abroadInfos, List<AcademicInfo> academicInfos, List<AwardInfo> awardInfos, List<Education> educations, List<ElectronalOrganization> electronalOrganizations, List<Employment> employments, List<MilitaryInfo> millitaryInfos, List<ResearchInfo> researchInfos, int id, String firstname, String lastname, String middlename, DictionaryWrapper citizenshipId, DictionaryWrapper maritalStatusId, DictionaryWrapper millitaryServiceId, DictionaryWrapper nationalityId, DictionaryWrapper socialStatusId, String birthdate, String pincode, DictionaryWrapper svSeriya, String seriya, DictionaryWrapper bloodGroupId, DictionaryWrapper supplyOrgId, String svEndDate, int height, DictionaryWrapper genderId, int photoFileId, int childCount, List<PersonAddress> addreses, List<PersonContact> contacts, List<PersonDocument> documents, List<PersonRelationship> relationships, List<PersonLanguage> languages,List<Vacation> vacations,List<CaseRecord> caseRecord) {
        super(id, firstname, lastname, middlename, citizenshipId, maritalStatusId, millitaryServiceId, nationalityId, socialStatusId, birthdate, pincode, svSeriya, seriya, bloodGroupId, supplyOrgId, svEndDate, height, genderId, photoFileId, childCount, addreses, contacts, documents, relationships, languages);
        
        this.employeeid = employeeid;
        this.custNum = custNum;
        this.specialityId = specialityId;
        this.positionId = positionId;
        this.positionCategoryId = positionCategoryId;
        this.orgId = orgId;
        this.statId = statId;
        this.payId = payId;
        this.startDate = startDate;
        this.note = note;
        this.membershipId=membershipId;
        this.membershipOrgName = membershipOrgName;
        this.membershipPeriod = membershipPeriod;
        this.membershipCardNum = membershipCardNum;
        this.membershipNote=membershipNote;
        this.abroadInfos = abroadInfos;
        this.academicInfos = academicInfos;
        this.awardInfos = awardInfos;
        this.educations = educations;
        this.electronalOrganizations = electronalOrganizations;
        this.employments = employments;
        this.millitaryInfos = millitaryInfos;
        this.researchInfos = researchInfos;
        this.vacations=vacations;
        this.caseRecord=caseRecord;
        
    }

    public Employee(int employeeid, String custNum, DictionaryWrapper specialityId, DictionaryWrapper positionId, DictionaryWrapper positionCategoryId, DictionaryWrapper orgId, int id, String firstname, String lastname, String middlename, DictionaryWrapper genderId) {
        super(id, firstname, lastname, middlename, genderId);
        this.employeeid = employeeid;
        this.custNum = custNum;
        this.specialityId = specialityId;
        this.positionId = positionId;
        this.positionCategoryId = positionCategoryId;
        this.orgId = orgId;
    }

   
    
    
    public Employee(DictionaryWrapper specialityId, DictionaryWrapper positionId, DictionaryWrapper orgId, int id, String fistname, String lastname, String middlename, DictionaryWrapper genderId) {
        super(id, fistname, lastname, middlename, genderId);
        this.specialityId = specialityId;
        this.positionId = positionId;
        this.orgId = orgId;
    }

    public Employee(DictionaryWrapper specialityId, DictionaryWrapper positionId, DictionaryWrapper orgId, DictionaryWrapper statId, int id, String fistname, String lastname, String middlename, DictionaryWrapper maritalStatusId, DictionaryWrapper nationalityId, String birthdate, DictionaryWrapper svSeriya, String seriya, DictionaryWrapper supplyOrgId) {
        super(id, fistname, lastname, middlename, maritalStatusId, nationalityId, birthdate, svSeriya, seriya, supplyOrgId);
        this.specialityId = specialityId;
        this.positionId = positionId;
        this.orgId = orgId;
        this.statId = statId;
    }

    public int getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(int membershipId) {
        this.membershipId = membershipId;
    }

    public String getMembershipNote() {
        return membershipNote;
    }

    public void setMembershipNote(String membershipNote) {
        this.membershipNote = membershipNote;
    }
    
    


    

    public List<ResearchInfo> getResearchInfos() {
        return researchInfos;
    }

    public void setResearchInfos(List<ResearchInfo> researchInfos) {
        this.researchInfos = researchInfos;
    }

    public int getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }

    public String getCustNum() {
        return custNum;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public DictionaryWrapper getSpecialityId() {
        return specialityId;
    }

    public void setSpecialityId(DictionaryWrapper specialityId) {
        this.specialityId = specialityId;
    }

    public DictionaryWrapper getPositionId() {
        return positionId;
    }

    public void setPositionId(DictionaryWrapper positionId) {
        this.positionId = positionId;
    }

    public DictionaryWrapper getPositionCategoryId() {
        return positionCategoryId;
    }

    public void setPositionCategoryId(DictionaryWrapper positionCategoryId) {
        this.positionCategoryId = positionCategoryId;
    }

    public DictionaryWrapper getOrgId() {
        return orgId;
    }

    public void setOrgId(DictionaryWrapper orgId) {
        this.orgId = orgId;
    }

    public DictionaryWrapper getStatId() {
        return statId;
    }

    public void setStatId(DictionaryWrapper statId) {
        this.statId = statId;
    }

    public DictionaryWrapper getPayId() {
        return payId;
    }

    public void setPayId(DictionaryWrapper payId) {
        this.payId = payId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public DictionaryWrapper getMembershipOrgName() {
        return membershipOrgName;
    }

    public void setMembershipOrgName(DictionaryWrapper membershipOrgName) {
        this.membershipOrgName = membershipOrgName;
    }

    public String getMembershipPeriod() {
        return membershipPeriod;
    }

    public void setMembershipPeriod(String membershipPeriod) {
        this.membershipPeriod = membershipPeriod;
    }

    public String getMembershipCardNum() {
        return membershipCardNum;
    }

    public void setMembershipCardNum(String membershipCardNum) {
        this.membershipCardNum = membershipCardNum;
    }

    public List<AbroadInfo> getAbroadInfos() {
        return abroadInfos;
    }

    public void setAbroadInfos(List<AbroadInfo> abroadInfos) {
        this.abroadInfos = abroadInfos;
    }

    public List<AcademicInfo> getAcademicInfos() {
        return academicInfos;
    }

    public void setAcademicInfos(List<AcademicInfo> academicInfos) {
        this.academicInfos = academicInfos;
    }

    public List<AwardInfo> getAwardInfos() {
        return awardInfos;
    }

    public void setAwardInfos(List<AwardInfo> awardInfos) {
        this.awardInfos = awardInfos;
    }

    public List<Education> getEducations() {
        return educations;
    }

    public void setEducations(List<Education> educations) {
        this.educations = educations;
    }

    public List<ElectronalOrganization> getElectronalOrganizations() {
        return electronalOrganizations;
    }

    public void setElectronalOrganizations(List<ElectronalOrganization> electronalOrganizations) {
        this.electronalOrganizations = electronalOrganizations;
    }

    public List<Employment> getEmployments() {
        return employments;
    }

    public void setEmployments(List<Employment> employments) {
        this.employments = employments;
    }

    public List<MilitaryInfo> getMillitaryInfos() {
        return millitaryInfos;
    }

    public void setMillitaryInfos(List<MilitaryInfo> millitaryInfos) {
        this.millitaryInfos = millitaryInfos;
    }

    @Override
    public String toString() {
        return "Employee{" + "employeeid=" + employeeid + ", custNum=" + custNum + ", specialityId=" + specialityId + ", positionId=" + positionId + ", positionCategoryId=" + positionCategoryId + ", orgId=" + orgId + ", statId=" + statId + ", payId=" + payId + ", startDate=" + startDate + ", note=" + note + ", membershipId=" + membershipId + ", membershipOrgName=" + membershipOrgName + ", membershipPeriod=" + membershipPeriod + ", membershipCardNum=" + membershipCardNum + ", membershipNote=" + membershipNote + ", abroadInfos=" + abroadInfos + ", academicInfos=" + academicInfos + ", awardInfos=" + awardInfos + ", educations=" + educations + ", electronalOrganizations=" + electronalOrganizations + ", employments=" + employments + ", millitaryInfos=" + millitaryInfos + ", researchInfos=" + researchInfos + ", vacations=" + vacations + ", caseRecord=" + caseRecord + '}';
    }

    
}
