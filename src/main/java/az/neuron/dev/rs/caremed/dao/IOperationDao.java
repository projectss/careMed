/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.EmployeeStatus;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.PatientHistory;
import az.neuron.dev.rs.caremed.form.EmpStatusForm;
import az.neuron.dev.rs.caremed.form.EmployeeStatusForm;
import az.neuron.dev.rs.caremed.form.MedicalCommitionForm;
import az.neuron.dev.rs.caremed.form.OperationForm;
import az.neuron.dev.rs.caremed.form.ServiceOperationForm;
import az.neuron.dev.rs.caremed.form.ServicesForm;
import az.neuron.dev.rs.caremed.search.PatientCommitionHistory;
import az.neuron.dev.rs.caremed.search.PatientSearchForm;
import java.util.List;

/**
 *
 * @author Bayram
 */
public interface IOperationDao {

    public List<EmployeeStatus> getEmployeeStatus(String token, EmployeeStatusForm form);

    public OperationResponse addPatientServices(String token, ServicesForm form);

    public OperationResponse changeEmployeeStatus(String token, EmpStatusForm empStatusForm);

    public OperationResponse addServices(String token, ServicesForm form);

    public OperationResponse addPatientServiceOperation(String token, OperationForm operationForm);

    public DataTable getPatientForCommition(String token, PatientSearchForm form);
    
    public DataTable getCommitionHistory(String token,int patientId, PatientCommitionHistory patientCommitionHistory);
    
    public OperationResponse addPatientMedicalCommition (String token,int patientId,OperationForm operationForm);
    
    public List<PatientHistory> getServicesForMedicalCommition (String token,int commitionId );
    
    
    
    
    

}
