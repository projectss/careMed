/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.db.DbConnect;
import az.neuron.dev.rs.caremed.domain.AbroadInfo;
import az.neuron.dev.rs.caremed.domain.AcademicInfo;
import az.neuron.dev.rs.caremed.domain.AwardInfo;
import az.neuron.dev.rs.caremed.domain.CaseRecord;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.DictionaryWrapper;
import az.neuron.dev.rs.caremed.domain.Education;
import az.neuron.dev.rs.caremed.domain.ElectronalOrganization;
import az.neuron.dev.rs.caremed.domain.Employee;
import az.neuron.dev.rs.caremed.domain.EmployeeFullList;
import az.neuron.dev.rs.caremed.domain.EmployeeList;
import az.neuron.dev.rs.caremed.domain.Employment;
import az.neuron.dev.rs.caremed.domain.FileWrapper;
import az.neuron.dev.rs.caremed.domain.MilitaryInfo;
import az.neuron.dev.rs.caremed.domain.MultilanguageString;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.Person;
import az.neuron.dev.rs.caremed.domain.PersonAddress;
import az.neuron.dev.rs.caremed.domain.PersonContact;
import az.neuron.dev.rs.caremed.domain.PersonDocument;
import az.neuron.dev.rs.caremed.domain.PersonLanguage;
import az.neuron.dev.rs.caremed.domain.PersonRelationship;
import az.neuron.dev.rs.caremed.domain.PersonTest;
import az.neuron.dev.rs.caremed.domain.ResearchInfo;
import az.neuron.dev.rs.caremed.domain.Vacation;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.exception.CareMedException;
import az.neuron.dev.rs.caremed.form.AbroadForm;
import az.neuron.dev.rs.caremed.form.AcademicInfoForm;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.AwardForm;
import az.neuron.dev.rs.caremed.form.CaseRecordForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.DocumentForm;
import az.neuron.dev.rs.caremed.form.EducationForm;
import az.neuron.dev.rs.caremed.form.ElectronalOrgForm;
import az.neuron.dev.rs.caremed.form.EmployeeForm;
import az.neuron.dev.rs.caremed.form.EmployeeHistoryForm;
import az.neuron.dev.rs.caremed.form.EmployementForm;
import az.neuron.dev.rs.caremed.form.MembershipForm;
import az.neuron.dev.rs.caremed.form.MilitaryInfoForm;
import az.neuron.dev.rs.caremed.form.PFile;
import az.neuron.dev.rs.caremed.form.PersonForm;
import az.neuron.dev.rs.caremed.form.PersonLanguageForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import az.neuron.dev.rs.caremed.form.ResearchForm;
import az.neuron.dev.rs.caremed.form.TextUC;
import az.neuron.dev.rs.caremed.form.VacationForm;
import az.neuron.dev.rs.caremed.search.EmployeeSearchForm;
import az.neuron.dev.rs.caremed.util.Converter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bayram
 */
@Repository
public class EmployeeDao implements IEmployeeDao {

    private static final Logger log = Logger.getLogger(EmployeeDao.class);

    @Autowired
    private DbConnect dbConnect;

    @Override
    public DataTable getEmployeeList(EmployeeSearchForm form) {

        List<Employee> list = new ArrayList<>();
        DataTable dataTable = new DataTable();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.get_list_employee (?,?,?,?,?,?,?,?)}")) {

                callableStatement.setString(1, form.getToken());

                if (form.getOrgId() != null && form.getOrgId() > 0) {
                    callableStatement.setInt(2, form.getOrgId());
                } else {
                    callableStatement.setInt(2, 0);
                }

                if (form.getPositionId() != null && form.getPositionId() > 0) {
                    callableStatement.setInt(3, form.getPositionId());
                } else {
                    callableStatement.setInt(3, 0);
                }

                if (form.getGenderId() != null && form.getGenderId() > 0) {
                    callableStatement.setInt(4, form.getGenderId());
                } else {
                    callableStatement.setInt(4, 0);
                }

                callableStatement.setInt(5, form.getStart());
                callableStatement.setInt(6, form.getLength());
                callableStatement.registerOutParameter(7, Types.INTEGER);
                callableStatement.registerOutParameter(8, Types.OTHER);

                callableStatement.execute();

                ResultSet resultSet = (ResultSet) callableStatement.getObject(8);

                while (resultSet.next()) {

                    list.add(new Employee(new DictionaryWrapper(resultSet.getInt("org_id"),
                            new MultilanguageString(resultSet.getString("org_name_az"),
                                    resultSet.getString("org_name_en"),
                                    resultSet.getString("org_name_ru"))),
                            new DictionaryWrapper(resultSet.getInt("spec_id"),
                                    new MultilanguageString(resultSet.getString("spec_name_az"),
                                            resultSet.getString("spec_name_en"),
                                            resultSet.getString("spec_name_ru"))),
                            new DictionaryWrapper(resultSet.getInt("position_id"),
                                    new MultilanguageString(resultSet.getString("pos_name_az"),
                                            resultSet.getString("pos_name_en"),
                                            resultSet.getString("pos_name_ru"))),
                            resultSet.getInt("person_id"),
                            resultSet.getString("first_name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("middle_name"),
                            new DictionaryWrapper(resultSet.getInt("gender_id"),
                                    new MultilanguageString(resultSet.getString("gender_name_az"),
                                            resultSet.getString("gender_name_en"),
                                            resultSet.getString("gender_name_ru")))));
                }
                dataTable.setDataList(list);
                dataTable.setTotalCount(callableStatement.getInt(7));
            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return dataTable;
    }

    @Override
    public OperationResponse addEmployeeFile(String token, int id, int docId, PFile file) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_common.add_file (?,?,?,?,?)}")) {
                callableStatement.registerOutParameter(1, Types.OTHER);
                callableStatement.setString(2, token);
                callableStatement.setString(3, file.getOrginalName());
                callableStatement.setString(4, file.getPath());
                callableStatement.setInt(5, file.getSize());
                callableStatement.setString(6, file.getContentType());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse addEmployee(EmployeeForm form, String imageFullpath, String imageOriginalName) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        int personId = 0;

        try (Connection connection = dbConnect.getConnection()) {

            try (CallableStatement callableStatemnt = connection.prepareCall("{? = call cm_caremed.add_employee(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)} ")) {
                callableStatemnt.registerOutParameter(1, Types.INTEGER);
                callableStatemnt.setString(2, form.getToken());
                callableStatemnt.setString(3, form.getFistName());
                callableStatemnt.setString(4, form.getLastName());
                callableStatemnt.setString(5, form.getMiddleName());
                callableStatemnt.setInt(6, form.getCitizenshipId());
                callableStatemnt.setInt(7, form.getMaritalStatusId());
                callableStatemnt.setInt(8, form.getMillitaryServiceId());
                callableStatemnt.setInt(9, form.getNationalityId());
                callableStatemnt.setInt(10, form.getSocialStatusId());
                callableStatemnt.setString(11, form.getBirthdate());
                callableStatemnt.setString(12, form.getPincode());
                callableStatemnt.setString(13, form.getSeriya());
                callableStatemnt.setInt(14, form.getBloodGroupId());
                callableStatemnt.setInt(15, form.getSupplyOrg());
                callableStatemnt.setString(16, form.getEndDate());
                callableStatemnt.setInt(17, form.getHeight());
                callableStatemnt.setInt(18, form.getGenderId());
                callableStatemnt.setInt(19, form.getElectoralOrgName());
                callableStatemnt.setString(20, form.getElectoralCardNum());
                callableStatemnt.setString(21, form.getElectoralPeriod());
                callableStatemnt.setInt(22, form.getSpecId());
                callableStatemnt.setInt(23, form.getPositionId());
                callableStatemnt.setInt(24, form.getPositionCatId());
                callableStatemnt.setInt(25, form.getOrgId());
                callableStatemnt.setInt(26, form.getStatId());
                callableStatemnt.setInt(27, form.getPayId());
                callableStatemnt.setString(28, form.getStartDate());
                callableStatemnt.setString(29, form.getNote());
                callableStatemnt.setArray(30, connection.createArrayOf("text", Converter.toPlpgContactObject(form.getContacts())));
                callableStatemnt.setArray(31, connection.createArrayOf("text", Converter.toPlpgAddressObject(form.getAddresses())));
                callableStatemnt.setArray(32, connection.createArrayOf("text", Converter.toPlpgRelationship(form.getRelations())));
                
                if (imageFullpath == null || imageFullpath.trim().isEmpty()) {
                    callableStatemnt.setNull(33, Types.NULL);
                } else {
                    callableStatemnt.setString(33, imageFullpath);

                }

                callableStatemnt.setString(34, imageOriginalName);

                callableStatemnt.setArray(35, connection.createArrayOf("text", Converter.toPlpgPersonLanguage(form.getLanguageForms())));
                callableStatemnt.setArray(36, connection.createArrayOf("text", Converter.toPlpgAbroadObject(form.getAbroad())));
                callableStatemnt.setArray(37, connection.createArrayOf("text", Converter.toPlpgAcademicInfoObject(form.getAcademicInfo())));
                callableStatemnt.setArray(38, connection.createArrayOf("text", Converter.toPlpgAward(form.getAward())));
                callableStatemnt.setArray(39, connection.createArrayOf("text", Converter.toPlgEducation(form.getEducation())));
                callableStatemnt.setArray(40, connection.createArrayOf("text", Converter.toPlpgElectoralOrg(form.getElectronalOrg())));
                callableStatemnt.setArray(41, connection.createArrayOf("text", Converter.toPlpgEmployement(form.getEmployement())));
                callableStatemnt.setArray(42, connection.createArrayOf("text", Converter.toPlpgMilitaryInfo(form.getMillitatyInfo())));
                callableStatemnt.setArray(43, connection.createArrayOf("text", Converter.toPlpgResearch(form.getResearch())));
                callableStatemnt.setArray(44, connection.createArrayOf("text", Converter.toPlgCaseRecord(form.getCaseRecordForms())));
                callableStatemnt.setArray(45, connection.createArrayOf("text", Converter.toPlgVacation(form.getVacationForm())));      
                callableStatemnt.setInt(46, form.getSvSeriya());          
                callableStatemnt.setInt(47, form.getChildCount());
                callableStatemnt.setString(48, form.getNote());
                callableStatemnt.executeUpdate();

                personId = callableStatemnt.getInt(1);
                int documentId;
                for (DocumentForm documentForm : form.getDocuments()) {

                    operationResponse = this.addDocument(documentForm, form.getToken(), personId);

                    if (operationResponse.getCode() != ResultCode.OK) {
                        throw new CareMedException("Document has not been saved to dataBase");
                    }

                    documentId = (int) operationResponse.getData();
                    documentForm.setId(documentId);
                    operationResponse = this.addDocFile(documentForm, form.getToken());

                    if (operationResponse.getCode() != ResultCode.OK) {
                        throw new CareMedException("Document file has not been saved to dataBase");
                    }
                }
                operationResponse.setData(personId);
                operationResponse.setCode(ResultCode.OK);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public Employee getEmployeeDetails(String token, int personId) {
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_get_employee_fulllist e  where e.person_id=?")) {
                preparedStatement.setInt(1, personId);
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    if (rs.next()) {

                        return new Employee(rs.getInt("id"),
                                rs.getString("cust_num"),
                                new DictionaryWrapper(rs.getInt("spec_id"),
                                        new MultilanguageString(rs.getString("spec_name_az"),
                                                rs.getString("spec_name_en"),
                                                rs.getString("spec_name_ru"))),
                                new DictionaryWrapper(rs.getInt("position_id"),
                                        new MultilanguageString(rs.getString("pos_name_az"),
                                                rs.getString("pos_name_en"),
                                                rs.getString("pos_name_ru"))),
                                new DictionaryWrapper(rs.getInt("position_category_id"),
                                        new MultilanguageString(rs.getString("pos_category_name_az"),
                                                rs.getString("pos_category_name_en"),
                                                rs.getString("pos_category_name_ru"))),
                                new DictionaryWrapper(rs.getInt("org_id"),
                                        new MultilanguageString(rs.getString("org_name_az"),
                                                rs.getString("org_name_en"),
                                                rs.getString("org_name_ru"))),
                                new DictionaryWrapper(rs.getInt("stat_id"),
                                        new MultilanguageString(rs.getString("stat_name_az"),
                                                rs.getString("stat_name_en"),
                                                rs.getString("stat_name_ru"))),
                                new DictionaryWrapper(rs.getInt("pay_id"),
                                        new MultilanguageString(rs.getString("pay_name_az"),
                                                rs.getString("pay_name_en"),
                                                rs.getString("pay_name_ru"))),
                                rs.getString("emp_start_date"),
                                rs.getString("emp_note"), rs.getInt("membership_id"),
                                new DictionaryWrapper(rs.getInt("membership_type_id"),
                                        new MultilanguageString(rs.getString("membership_az"),
                                                rs.getString("membership_en"),
                                                rs.getString("membership_ru"))),
                                rs.getString("m_period"),
                                rs.getString("m_card_num"),
                                rs.getString("m_note"),
                                this.getAbroadInfo(token, personId),
                                this.getPersonAcademicInfo(token, personId),
                                this.getAwardInfo(token, personId),
                                this.getPersonEducation(token, personId),
                                this.getPersonElectronalOrganization(token, personId),
                                this.getEmploymentList(token, personId),
                                this.getMillitaryList(token, personId),
                                this.getResearchList(token, personId),
                                rs.getInt("person_id"),
                                rs.getString("first_name"),
                                rs.getString("last_name"),
                                rs.getString("middle_name"),
                                new DictionaryWrapper(rs.getInt("citizenship_id"),
                                        new MultilanguageString(rs.getString("citizenship_name_az"),
                                                rs.getString("citizenship_name_en"),
                                                rs.getString("citizenship_name_ru"))),
                                new DictionaryWrapper(rs.getInt("marital_status_id"),
                                        new MultilanguageString(rs.getString("marital_status_name_az"),
                                                rs.getString("marital_status_name_en"),
                                                rs.getString("marital_status_name_ru"))),
                                new DictionaryWrapper(rs.getInt("millitary_service_id"),
                                        new MultilanguageString(rs.getString("millitary_service_name_az"),
                                                rs.getString("millitary_service_name_en"),
                                                rs.getString("millitary_service_name_ru"))),
                                new DictionaryWrapper(rs.getInt("nationality_id"),
                                        new MultilanguageString(rs.getString("nationality_name_az"),
                                                rs.getString("nationality_name_en"),
                                                rs.getString("nationality_name_ru"))),
                                new DictionaryWrapper(rs.getInt("social_status_id"),
                                        new MultilanguageString(rs.getString("social_status_name_az"),
                                                rs.getString("social_status_name_en"),
                                                rs.getString("social_status_name_ru"))),
                                rs.getString("birthdate"),
                                rs.getString("pincode"),
                                new DictionaryWrapper(rs.getInt("sv_seriya"),
                                        new MultilanguageString(rs.getString("sv_seriya_name_az"),
                                                rs.getString("sv_seriya_name_az"),
                                                rs.getString("sv_seriya_name_az"))),
                                rs.getString("seriya"),
                                new DictionaryWrapper(rs.getInt("blood_group_id"),
                                        new MultilanguageString(rs.getString("blood_group_az"),
                                                rs.getString("blood_group_en"),
                                                rs.getString("blood_group_ru"))),
                                new DictionaryWrapper(rs.getInt("supply_organization"),
                                        new MultilanguageString(rs.getString("supply_organization_az"),
                                                rs.getString("supply_organization_en"),
                                                rs.getString("supply_organization_ru"))),
                                rs.getString("sv_enddate"),
                                rs.getInt("height"),
                                new DictionaryWrapper(rs.getInt("gender_id"),
                                        new MultilanguageString(rs.getString("gender_name_az"),
                                                rs.getString("gender_name_en"),
                                                rs.getString("gender_name_ru"))),
                                rs.getInt("photo_file_id"),
                                rs.getInt("child_count"),
                                this.getPersonAddress(token, personId),
                                this.getPersonContact(token, personId),
                                this.getPersonDocument(token, personId),
                                this.getPersonRealtionship(token, personId),
                                this.getPersonLanguage(token, personId),
                                this.getVacationList(token, personId),
                                this.getCaseList(token, personId));

                    }
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public OperationResponse editPersons(String token, PersonForm form, String imageFullpath, String imageOriginalName) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.edit_persons(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}")) {
                callableStatement.setInt(1, form.getId());
                callableStatement.setString(2, token);
                callableStatement.setString(3, form.getFistName());
                callableStatement.setString(4, form.getLastName());
                callableStatement.setString(5, form.getMiddleName());
                callableStatement.setInt(6, form.getCitizenshipId());
                callableStatement.setInt(7, form.getMaritalStatusId());
                callableStatement.setInt(8, form.getMillitaryServiceId());
                callableStatement.setInt(9, form.getNationalityId());
                callableStatement.setInt(10, form.getSocialStatusId());
                callableStatement.setString(11, form.getBirthdate());
                callableStatement.setString(12, form.getPincode());
                callableStatement.setString(13, form.getSeriya());
                callableStatement.setInt(14, form.getBloodGroupId());
                callableStatement.setInt(15, form.getSupplyOrg());
                callableStatement.setString(16, form.getEndDate());
                callableStatement.setInt(17, form.getHeight());
                callableStatement.setInt(18, form.getGenderId());
                callableStatement.setInt(19, form.getSvSeriya());
                callableStatement.setInt(20, form.getChildCount());

                if (imageFullpath == null || imageFullpath.trim().isEmpty()) {
                    callableStatement.setNull(22, Types.NULL);
                } else {
                    callableStatement.setString(22, imageFullpath);

                }

                callableStatement.setString(21, imageOriginalName);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editPersonLanguage(String token, int id, PersonLanguageForm language) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall(" {call cm_caremed.edit_person_language (?,?,?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, language.getId());
                callableStatement.setInt(4, language.getLanguageId());
                callableStatement.setInt(5, language.getDegreeId());

                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);

        }

        return operationResponse;
    }

    @Override
    public Employee getEmployeePersonalInfo(String token, int personId) {

        try (Connection connection = dbConnect.getConnection()) {
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_get_employee_fulllist e  where e.person_id=?")) {
                prepareStatement.setInt(1, personId);
                try (ResultSet rs = prepareStatement.executeQuery()) {

                    if (rs.next()) {
                        return new Employee(new DictionaryWrapper(rs.getInt("spec_id"),
                                new MultilanguageString(rs.getString("spec_name_az"),
                                        rs.getString("spec_name_en"),
                                        rs.getString("spec_name_ru"))),
                                new DictionaryWrapper(rs.getInt("position_id"),
                                        new MultilanguageString(rs.getString("pos_name_az"),
                                                rs.getString("pos_name_en"),
                                                rs.getString("pos_name_ru"))),
                                new DictionaryWrapper(rs.getInt("org_id"),
                                        new MultilanguageString(rs.getString("org_name_az"),
                                                rs.getString("org_name_en"),
                                                rs.getString("org_name_ru"))),
                                new DictionaryWrapper(rs.getInt("stat_id"),
                                        new MultilanguageString(rs.getString("stat_name_az"),
                                                rs.getString("stat_name_en"),
                                                rs.getString("stat_name_ru"))),
                                rs.getInt("person_id"),
                                rs.getString("first_name"),
                                rs.getString("last_name"),
                                rs.getString("middle_name"),
                                new DictionaryWrapper(rs.getInt("marital_status_id"),
                                        new MultilanguageString(rs.getString("marital_status_name_az"),
                                                rs.getString("marital_status_name_en"),
                                                rs.getString("marital_status_name_ru"))),
                                new DictionaryWrapper(rs.getInt("nationality_id"),
                                        new MultilanguageString(rs.getString("nationality_name_az"),
                                                rs.getString("nationality_name_en"),
                                                rs.getString("nationality_name_ru"))),
                                rs.getString("birthdate"),
                                new DictionaryWrapper(rs.getInt("sv_seriya"),
                                        new MultilanguageString(rs.getString("sv_seriya_name_az"),
                                                rs.getString("sv_seriya_name_en"),
                                                rs.getString("sv_seriya_name_ru"))),
                                rs.getString("seriya"),
                                new DictionaryWrapper(rs.getInt("supply_organization"),
                                        new MultilanguageString(rs.getString("supply_organization_az"),
                                                rs.getString("supply_organization_en"),
                                                rs.getString("supply_organization_ru")))
                        );
                    }
                }
            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public List<PersonLanguage> getPersonLanguage(String token, int personId) {
        List<PersonLanguage> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatemnt = connection.prepareStatement("select * from cm_caremed.v_person_language l where l.person_id = ?")) {
                preparedStatemnt.setInt(1, personId);
                try (ResultSet rs = preparedStatemnt.executeQuery()) {
                    while (rs.next()) {

                        list.add(new PersonLanguage(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("language_id"),
                                        new MultilanguageString(rs.getString("language_name_az"),
                                                rs.getString("language_name_en"),
                                                rs.getString("language_name_ru"))),
                                new DictionaryWrapper(rs.getInt("degree_id"),
                                        new MultilanguageString(rs.getString("degree_name_az"),
                                                rs.getString("degree_name_en"),
                                                rs.getString("degree_name_ru")))
                        ));

                    }

                }

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<PersonContact> getPersonContact(String token, int personId) {
        List<PersonContact> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatemnt = connection.prepareStatement("select * from cm_caremed.v_person_contact p  where  p.person_id = ?")) {
                preparedStatemnt.setInt(1, personId);
                try (ResultSet rs = preparedStatemnt.executeQuery()) {
                    while (rs.next()) {

                        list.add(new PersonContact(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("type_id"),
                                        new MultilanguageString(rs.getString("contact_name_az"),
                                                rs.getString("contact_name_en"),
                                                rs.getString("contact_name_ru"))),
                                rs.getString("contact")));

                    }

                }

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<PersonAddress> getPersonAddress(String token, int personId) {
        List<PersonAddress> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatemnt = connection.prepareStatement("select * from cm_caremed.v_person_address p where p.person_id = ?")) {
                preparedStatemnt.setInt(1, personId);
                try (ResultSet rs = preparedStatemnt.executeQuery()) {
                    while (rs.next()) {

                        list.add(new PersonAddress(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("address_id"),
                                        new MultilanguageString(rs.getString("address_name_az"),
                                                rs.getString("address_name_en"),
                                                rs.getString("address_name_ru"))),
                                new DictionaryWrapper(rs.getInt("type_id"),
                                        new MultilanguageString(rs.getString("type_name_az"),
                                                rs.getString("type_name_en"),
                                                rs.getString("type_name_ru"))),
                                rs.getString("address")));

                    }

                }

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<AcademicInfo> getPersonAcademicInfo(String token, int personId) {
        List<AcademicInfo> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_academic_info a  where a.person_id = ?")) {
                preparedStatement.setInt(1, personId);
                try (ResultSet rs = preparedStatement.executeQuery()) {

                    while (rs.next()) {
                        list.add(new AcademicInfo(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("type_id"),
                                        new MultilanguageString(rs.getString("type_name_az"),
                                                rs.getString("type_name_en"),
                                                rs.getString("type_name_ru"))),
                                new DictionaryWrapper(rs.getInt("academic_info_id"),
                                        new MultilanguageString(rs.getString("academic_name_az"),
                                                rs.getString("academic_name_en"),
                                                rs.getString("academic_name_ru"))),
                                rs.getString("start_date")
                        ));
                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<Education> getPersonEducation(String token, int personId) {
        List<Education> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_education e where e.person_id = ?")) {
                preparedStatement.setInt(1, personId);
                try (ResultSet rs = preparedStatement.executeQuery()) {

                    while (rs.next()) {
                        list.add(new Education(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("edu_level_id"),
                                        new MultilanguageString(rs.getString("edu_level_name_az"),
                                                rs.getString("edu_level_name_en"),
                                                rs.getString("edu_level_name_ru"))),
                                new DictionaryWrapper(rs.getInt("institution_type"),
                                        new MultilanguageString(rs.getString("institution_type_az"),
                                                rs.getString("institution_type_en"),
                                                rs.getString("institution_type_ru"))),
                                rs.getString("institution_name"),
                                rs.getString("institution_address"),
                                rs.getString("faculty_name"),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("diplom_number"),
                                rs.getString("note")));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<ElectronalOrganization> getPersonElectronalOrganization(String token, int personId) {
        List<ElectronalOrganization> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_electronal_organization e where e.person_id = ?")) {
                prepareStatement.setInt(1, personId);
                try (ResultSet rs = prepareStatement.executeQuery()) {
                    while (rs.next()) {

                        list.add(new ElectronalOrganization(rs.getInt("id"),
                                rs.getString("address"),
                                rs.getString("position_name"),
                                rs.getString("org_name"),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("note")));
                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<Employment> getEmploymentList(String token, int personId) {
        List<Employment> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_employment v where v.person_id = ?")) {
                prepareStatement.setInt(1, personId);
                try (ResultSet rs = prepareStatement.executeQuery()) {
                    while (rs.next()) {

                        list.add(new Employment(rs.getInt("id"),
                                rs.getString("company_name"),
                                rs.getString("position_name"),
                                rs.getString("company_address"),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("note")
                        ));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<MilitaryInfo> getMillitaryList(String token, int personId) {
        List<MilitaryInfo> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_millitary_info v where v.person_id = ?")) {
                prepareStatement.setInt(1, personId);
                try (ResultSet rs = prepareStatement.executeQuery()) {
                    while (rs.next()) {

                        list.add(new MilitaryInfo(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("millitary_name_id"),
                                        new MultilanguageString(rs.getString("millitary_name_az"),
                                                rs.getString("millitary_name_en"),
                                                rs.getString("millitary_name_ru"))),
                                new DictionaryWrapper(rs.getInt("staff_type"),
                                        new MultilanguageString(rs.getString("staff_type_name_az"),
                                                rs.getString("staff_type_name_en"),
                                                rs.getString("staff_type_name_ru"))),
                                new DictionaryWrapper(rs.getInt("army_type"),
                                        new MultilanguageString(rs.getString("army_type_name_az"),
                                                rs.getString("army_type_name_en"),
                                                rs.getString("army_type_name_ru"))),
                                rs.getString("note")
                        ));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<ResearchInfo> getResearchList(String token, int personId) {
        List<ResearchInfo> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_research_info r where r.person_id=?")) {
                prepareStatement.setInt(1, personId);
                try (ResultSet rs = prepareStatement.executeQuery()) {
                    while (rs.next()) {

                        list.add(new ResearchInfo(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("research_type_id"),
                                        new MultilanguageString(rs.getString("research_type_name_az"),
                                                rs.getString("research_type_name_en"),
                                                rs.getString("research_type_name_ru"))),
                                rs.getString("research_name"),
                                rs.getString("start_date"),
                                rs.getString("note")));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;

    }

    @Override
    public List<AbroadInfo> getAbroadInfo(String token, int personId) {
        List<AbroadInfo> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_abroad_info v where v.person_id = ?")) {
                prepareStatement.setInt(1, personId);
                try (ResultSet rs = prepareStatement.executeQuery()) {
                    while (rs.next()) {

                        list.add(new AbroadInfo(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("country_id"),
                                        new MultilanguageString(rs.getString("country_name_az"),
                                                rs.getString("country_name_en"),
                                                rs.getString("country_name_ru"))),
                                new DictionaryWrapper(rs.getInt("reason_id"),
                                        new MultilanguageString(rs.getString("reason_name_az"),
                                                rs.getString("reason_name_en"),
                                                rs.getString("reason_name_ru"))),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("note")));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;

    }

    @Override
    public List<AwardInfo> getAwardInfo(String token, int personId) {
        List<AwardInfo> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_award_info a where a.person_id=?")) {
                prepareStatement.setInt(1, personId);
                try (ResultSet rs = prepareStatement.executeQuery()) {
                    while (rs.next()) {

                        list.add(new AwardInfo(rs.getInt("id"),
                                rs.getString("start_date"),
                                rs.getString("award_name"),
                                new DictionaryWrapper(rs.getInt("type_id"),
                                        new MultilanguageString(rs.getString("type_name_az"),
                                                rs.getString("type_name_en"),
                                                rs.getString("type_name_ru"))),
                                rs.getString("note")));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;

    }

    @Override
    public List<PersonDocument> getPersonDocument(String token, int personId) {

        List<PersonDocument> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_person_document d where d.person_id=?")) {
                prepareStatement.setInt(1, personId);
                try (ResultSet rs = prepareStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new PersonDocument(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("type_id"),
                                        new MultilanguageString(rs.getString("type_name_az"),
                                                rs.getString("type_name_en"),
                                                rs.getString("type_name_ru"))),
                                rs.getString("doc_num"),
                                rs.getString("doc_serial"),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("note"),
                                this.getDocumentFiles(rs.getInt("id"))));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);

        }

        return list;
    }

    @Override
    public List<FileWrapper> getPersonFile(String token, int documentId) {
        System.out.println("sdsdss    ++++++ " + documentId);
        List<FileWrapper> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_person_file f  where f.id=?")) {
                prepareStatement.setInt(1, documentId);

                try (ResultSet rs = prepareStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new FileWrapper(rs.getInt("id"),
                                rs.getString("original_name"),
                                rs.getString("path")));
                    }

                }
            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<PersonRelationship> getPersonRealtionship(String token, int personId) {
        List<PersonRelationship> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_person_relationship r  where r.person_id=?")) {
                prepareStatement.setInt(1, personId);
                try (ResultSet rs = prepareStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new PersonRelationship(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("relationship_id"),
                                        new MultilanguageString(rs.getString("relationship_name_az"),
                                                rs.getString("relationship_name_en"),
                                                rs.getString("relationship_name_ru"))),
                                rs.getString("fullname"), rs.getString("birthdate"), rs.getString("contact_phone"), rs.getString("contact_home"), rs.getString("address"), rs.getString("note")));
                    }

                }

            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public FileWrapper getEmployeeImage(int id) {
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_person_photo v where v.person_id = ?")) {
                preparedStatement.setInt(1, id);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return new FileWrapper(resultSet.getInt("id"),
                                resultSet.getString("original_name"),
                                resultSet.getString("path"));
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public OperationResponse checkPincode(String pincode) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select count(*) count_employee from cm_common.persons p where lower(p.pincode)=lower(?) and p.active = 1")) {
                preparedStatement.setString(1, pincode);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        int count = resultSet.getInt("count_employee");

                        operationResponse.setData(count);
                        operationResponse.setCode(ResultCode.OK);
                        return operationResponse;
                    }

                }
            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public List<Vacation> getVacationList(String token, int perosonId) {
        List<Vacation> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_vacation_record  v where v.person_id =?")) {
                preparedStatement.setInt(1, perosonId);

                try (ResultSet rs = preparedStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new Vacation(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("vacation_type_id"),
                                        new MultilanguageString(rs.getString("vacation_type_name_az"),
                                                rs.getString("vacation_type_name_en"),
                                                rs.getString("vacation_type_name_ru"))),
                                rs.getString("date_period"),
                                rs.getInt("day_count"),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("order_no"),
                                rs.getString("next_vacation_date")));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public List<CaseRecord> getCaseList(String token, int personId) {
        List<CaseRecord> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from cm_caremed.v_case_record c  where c.person_id=? ")) {
                preparedStatement.setInt(1, personId);

                try (ResultSet rs = preparedStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new CaseRecord(rs.getInt("id"),
                                rs.getString("case_no"),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getInt("day_count")));

                    }

                }

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public OperationResponse editPesonContact(String token, int id, ContactForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {

            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.edit_person_contact (?,?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setInt(3, form.getTypeId());
                callableStatement.setString(4, form.getContact());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addDocument(DocumentForm form, String token, int personId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{? = call cm_caremed.add_document(?,?,?,?,?,?,?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setString(5, form.getNumber());
                callableStatement.setString(6, form.getSerial());
                callableStatement.setString(7, form.getStartDate());
                callableStatement.setString(8, form.getEndDate());
                callableStatement.executeUpdate();

                int docId = callableStatement.getInt(1);

                operationResponse.setData(docId);
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse addDocFile(DocumentForm form, String token) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.add_doc_file(?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());

                if (form.getUploads() == null) {

                    callableStatement.setNull(3, Types.NULL);
                } else {
                    callableStatement.setArray(3, connection.createArrayOf("text", Converter.toPlgFileWrapper(form.getUploads())));
                }

                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;

    }

    @Override
    public OperationResponse editPersonAddress(String token, AddressForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_person_address (?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setInt(3, form.getAddressId());
                callableStatement.setString(4, form.getAddress());
                callableStatement.setInt(5, form.getTypeId());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editPersonRelationship(String token, int id, PersonRelationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_person_relationship (?,?,?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setInt(3, form.getRelationshipId());
                callableStatement.setString(4, form.getFullName());
                callableStatement.setString(5, form.getBirthdate());
                callableStatement.setString(6, form.getContactPhone());
                callableStatement.setString(7, form.getContactHome());
                callableStatement.setString(8, form.getAddress());
                callableStatement.setString(9, form.getNote());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editPersonDocument(String token, int id, DocumentForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {

            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_person_document (?,?,?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setString(5, form.getNumber());
                callableStatement.setString(6, form.getSerial());
                callableStatement.setString(7, form.getStartDate());
                callableStatement.setString(8, form.getEndDate());
                callableStatement.setString(9, "");
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editAbroadInfo(String token, int id, AbroadForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_abroad_info (?,?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setInt(4, form.getCountryId());
                callableStatement.setInt(5, form.getReasonId());
                callableStatement.setString(6, form.getStartDate());
                callableStatement.setString(7, form.getEndDate());
                callableStatement.setString(8, form.getNote());
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse editAcademicInfo(String token, int id, AcademicInfoForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_academic_info(?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setInt(5, form.getAcademicInfoId());
                callableStatement.setString(6, form.getStartDate());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse editAward(String token, int id, AwardForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_award (?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setString(4, form.getStartDate());
                callableStatement.setString(5, form.getAwardName());
                callableStatement.setInt(6, form.getTypeId());
                callableStatement.setString(7, form.getNote());
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse removePersonContact(String token, int personId, int contactId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_person_contact(?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, contactId);
                callableStatement.setInt(3, personId);
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse addPersonContact(String token, int personId, ContactForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_person_contact (?, ?, ?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setString(5, form.getContact());
                callableStatement.executeUpdate();

                int contId = callableStatement.getInt(1);

                operationResponse.setData(contId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addAward(String token, int personId, AwardForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_award (?, ?, ?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setString(4, form.getStartDate());
                callableStatement.setString(5, form.getAwardName());
                callableStatement.setInt(6, form.getTypeId());
                callableStatement.setString(7, form.getNote());
                callableStatement.executeUpdate();

                int awardId = callableStatement.getInt(1);

                operationResponse.setData(awardId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addAbroadInfo(String token, int personId, AbroadForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_abroad_info (?, ?, ?, ?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getCountryId());
                callableStatement.setInt(5, form.getReasonId());
                callableStatement.setString(6, form.getStartDate());
                callableStatement.setString(7, form.getEndDate());
                callableStatement.setString(8, form.getNote());

                callableStatement.executeUpdate();

                int abroadId = callableStatement.getInt(1);

                operationResponse.setData(abroadId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addAcademicInfo(String token, int personId, AcademicInfoForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_academic_info (?, ?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setInt(5, form.getAcademicInfoId());
                callableStatement.setString(6, form.getStartDate());

                callableStatement.executeUpdate();

                int academicId = callableStatement.getInt(1);

                operationResponse.setData(academicId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addCaseRecord(String token, int personId, CaseRecordForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_case_record (?, ?, ?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setString(4, form.getCaseNo());
                callableStatement.setString(5, form.getStartDate());
                callableStatement.setString(6, form.getEndDate());
                callableStatement.setInt(7, form.getDayCount());

                callableStatement.executeUpdate();

                int caseRecordId = callableStatement.getInt(1);

                operationResponse.setData(caseRecordId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addEducation(String token, int personId, EducationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_education (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getEduLevelId());
                callableStatement.setString(5, form.getInstitutionName());
                callableStatement.setString(6, form.getInstitutionAddress());
                callableStatement.setInt(7, form.getInstitutionType());
                callableStatement.setString(8, form.getFacultyName());
                callableStatement.setString(9, form.getStartDate());
                callableStatement.setString(10, form.getEndDate());
                callableStatement.setString(11, form.getDiplomNumber());
                callableStatement.setString(12, form.getNote());

                callableStatement.executeUpdate();

                int educationId = callableStatement.getInt(1);

                operationResponse.setData(educationId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addElectronalOrgaznization(String token, int personId, ElectronalOrgForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {

            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_electronal_organization(? , ? , ? , ?, ? ,? , ? , ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setString(4, form.getAddress());
                callableStatement.setString(5, form.getPositionName());
                callableStatement.setString(6, form.getOrgName());
                callableStatement.setString(7, form.getStartDate());
                callableStatement.setString(8, form.getEndDate());
                callableStatement.setString(9, form.getNote());

                callableStatement.executeUpdate();

                int electronalId = callableStatement.getInt(1);

                operationResponse.setData(electronalId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addEmployement(String token, int personId, EmployementForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {

            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_employment (?, ?, ?, ?, ?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setString(4, form.getCompanyName());
                callableStatement.setString(5, form.getPositionName());
                callableStatement.setString(6, form.getCompanyAddress());
                callableStatement.setString(7, form.getStartDate());
                callableStatement.setString(8, form.getEndDate());
                callableStatement.setString(9, form.getNote());

                callableStatement.executeUpdate();

                int employementId = callableStatement.getInt(1);

                operationResponse.setData(employementId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addMemberShip(String token, int personId, MembershipForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_membership (? , ? , ? , ?, ? ,? ) }")) {
                callableStatement.registerOutParameter(1, Types.OTHER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setString(5, form.getPeriod());
                callableStatement.setString(6, form.getCardNum());
                callableStatement.setString(7, form.getNote());

                callableStatement.executeUpdate();

                int memberShipId = callableStatement.getInt(1);

                operationResponse.setData(memberShipId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addMillitaryInfo(String token, MilitaryInfoForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_millitary_info (? , ? , ? , ?, ? ,? ) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, form.getPersonId());
                callableStatement.setInt(4, form.getMillitaryNameId());
                callableStatement.setInt(5, form.getStaffType());
                callableStatement.setInt(6, form.getArmyType());
                callableStatement.setString(7, form.getNote());

                callableStatement.executeUpdate();

                int millitaryInfoId = callableStatement.getInt(1);

                operationResponse.setData(millitaryInfoId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPersonAddress(String token, int personId, AddressForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_person_address (?, ?, ?, ?, ?) }")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getAddressId());
                callableStatement.setString(5, form.getAddress());
                callableStatement.setInt(6, form.getTypeId());;

                callableStatement.executeUpdate();

                int addressId = callableStatement.getInt(1);

                operationResponse.setData(addressId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPersonLanguage(String token, int personId, PersonLanguageForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_person_language (?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getLanguageId());
                callableStatement.setInt(5, form.getDegreeId());

                callableStatement.executeUpdate();

                int languageId = callableStatement.getInt(1);

                operationResponse.setData(languageId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPersonRelationShip(String token, int personId, PersonRelationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_person_relationship (?, ?, ?, ?, ?, ?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getRelationshipId());
                callableStatement.setString(5, form.getFullName());
                callableStatement.setString(6, form.getBirthdate());
                callableStatement.setString(7, form.getContactPhone());
                callableStatement.setString(8, form.getContactHome());
                callableStatement.setString(9, form.getAddress());
                callableStatement.setString(10, form.getNote());

                callableStatement.executeUpdate();

                int relationShipId = callableStatement.getInt(1);

                operationResponse.setData(relationShipId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addPersonResearchInfo(String token, int personId, ResearchForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_research_info (?, ?, ?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4, form.getResearchType());
                callableStatement.setString(5, form.getResearchName());
                callableStatement.setString(6, form.getStartDate());

                callableStatement.setString(7, form.getNote());

                callableStatement.executeUpdate();

                int researchInfoId = callableStatement.getInt(1);

                operationResponse.setData(researchInfoId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addVacationRecord(String token, int personId, VacationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ ? = call cm_caremed.add_vacation_record (?, ?, ?, ?, ?, ?, ?, ?, ?)}")) {
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setString(2, token);
                callableStatement.setInt(3, personId);
                callableStatement.setInt(4,form.getTypeId());
                callableStatement.setString(5, form.getPeriod());
                callableStatement.setInt(6, form.getDayCount());
                callableStatement.setString(7, form.getStartDate());
                callableStatement.setString(8, form.getEndDate());
                callableStatement.setString(9, form.getOrgerNo());
                callableStatement.setString(10, form.getNextVacationDate());

                callableStatement.executeUpdate();

                int vacationId = callableStatement.getInt(1);

                operationResponse.setData(vacationId);
                operationResponse.setCode(ResultCode.OK);

            }
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }

    @Override
    public OperationResponse editCaseRecord(String token, int id, CaseRecordForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_case_record (?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setString(4, form.getCaseNo());
                callableStatement.setString(5, form.getStartDate());
                callableStatement.setString(6, form.getEndDate());
                callableStatement.setInt(7, form.getDayCount());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editEducation(String token, int id, EducationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_education (?,?,?,?,?,?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setInt(4, form.getEduLevelId());
                callableStatement.setString(5, form.getInstitutionName());
                callableStatement.setString(6, form.getInstitutionAddress());
                callableStatement.setInt(7, form.getInstitutionType());
                callableStatement.setString(8, form.getFacultyName());
                callableStatement.setString(9, form.getStartDate());
                callableStatement.setString(10, form.getEndDate());
                callableStatement.setString(11, form.getDiplomNumber());
                callableStatement.setString(12, form.getNote());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editElectronalOrganization(String token, ElectronalOrgForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_electronal_organization (?,?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setString(3, form.getAddress());
                callableStatement.setString(4, form.getPositionName());
                callableStatement.setString(5, form.getOrgName());
                callableStatement.setString(6, form.getStartDate());
                callableStatement.setString(7, form.getEndDate());
                callableStatement.setString(8, form.getNote());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editEmployement(String token, int personId, EmployementForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_employment(?,?,?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());
                callableStatement.setInt(3, personId);
                callableStatement.setString(4, form.getCompanyName());
                callableStatement.setString(5, form.getPositionName());
                callableStatement.setString(6, form.getCompanyAddress());
                callableStatement.setString(7, form.getStartDate());
                callableStatement.setString(8, form.getEndDate());
                callableStatement.setString(9, form.getNote());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editDocFile(String token, DocumentForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_file (?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, form.getId());

                callableStatement.setString(6, form.getStartDate());
                callableStatement.setString(7, form.getEndDate());
                callableStatement.setString(8, form.getNote());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editMembership(String token, MembershipForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_membership(?,?,?,?,?,?)}")) {
                callableStatement.setInt(1, form.getId());
                callableStatement.setString(2, token);
                callableStatement.setInt(3, form.getTypeId());
                callableStatement.setString(4, form.getPeriod());
                callableStatement.setString(5, form.getCardNum());
                callableStatement.setString(6, form.getNote());

                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editMillitaryInfo(String token, int id, MilitaryInfoForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_millitary_info (?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setInt(4, form.getMillitaryNameId());
                callableStatement.setInt(5, form.getStaffType());
                callableStatement.setInt(6, form.getArmyType());
                callableStatement.setString(7, form.getNote());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editResearchInfo(String token, int id, ResearchForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_research_info (?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setInt(4, form.getResearchType());
                callableStatement.setString(5, form.getResearchName());
                callableStatement.setString(6, form.getStartDate());
                callableStatement.setString(7, form.getNote());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editVacationRecord(String token, int id, VacationForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_vacation_record (?,?,?,?,?,?,?,?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, form.getId());
                callableStatement.setInt(4, form.getTypeId());
                callableStatement.setString(5, form.getPeriod());
                callableStatement.setInt(6, form.getDayCount());
                callableStatement.setString(7, form.getStartDate());
                callableStatement.setString(8, form.getEndDate());
                callableStatement.setString(9, form.getOrgerNo());
                callableStatement.setString(10, form.getNextVacationDate());
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeAbroadInfo(String token, int id, int abroadId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_abroad_info (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, abroadId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeAcademicInfo(String token, int id, int academicInfoId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_academic_info(?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, academicInfoId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeAward(String token, int id, int awardId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_award (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, awardId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeCaseRecord(String token, int id, int caseId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_case_record (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, caseId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeEducation(String token, int id, int educationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_education (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, educationId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeElectronalOrganization(String token, int id, int electoralId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_electronal_organization (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, electoralId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeEmployment(String token, int id, int employmentId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_employment (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, employmentId);
                callableStatement.setInt(3, id);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeMembership(String token, int id, int personId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_membership (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, personId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeMillitaryInfo(String token, int id, int militaryId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_millitary_info (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, militaryId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removePersonAddress(String token, int personId, int addressId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_person_address (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, addressId);
                callableStatement.setInt(3, personId);
                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removePersonDocument(String token, int id, int documentId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        List<FileWrapper> list = this.getPersonFile(token, documentId);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.remove_person_document (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, documentId);
                callableStatement.executeUpdate();

                operationResponse.setData(list);
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removePersonLanguage(String token, int id, int languageId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_person_language (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, languageId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removePersonRelationship(String token, int id, int relationshipId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_person_relationship (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, relationshipId);
                callableStatement.setInt(3, id);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeResearchInfo(String token, int id, int researchId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_research_info (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, id);
                callableStatement.setInt(3, researchId);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse removeVacationRecord(String token, int id, int vacationId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.remove_vacation_record (?,?,?) }")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, vacationId);
                callableStatement.setInt(3, id);
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse editEmployee(EmployeeForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call cm_caremed.edit_employee (?,?,?,?,?,?,?,?,?,?) }")) {
                callableStatement.setInt(1, form.getId());
                callableStatement.setString(2, form.getToken());
                callableStatement.setInt(3, form.getSpecId());
                callableStatement.setInt(4, form.getPositionId());
                callableStatement.setInt(5, form.getPositionCatId());
                callableStatement.setInt(6, form.getOrgId());

                callableStatement.setInt(7, form.getStatId());
                callableStatement.setInt(8, form.getPayId());
                callableStatement.setString(9, form.getStartDate());
                callableStatement.setString(10, form.getNote());

                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public List<FileWrapper> getDocumentFiles(int docId) {
        List<FileWrapper> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement prepareStatement = connection.prepareStatement("select * from cm_caremed.v_doc_files f  where f.doc_id = ?")) {
                prepareStatement.setInt(1, docId);

                try (ResultSet rs = prepareStatement.executeQuery()) {

                    while (rs.next()) {

                        list.add(new FileWrapper(rs.getInt("id"),
                                rs.getString("original_name"),
                                rs.getString("path")));
                    }

                }
            }

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return list;
    }

    @Override
    public FileWrapper getFileByFileId(int fileId) {

        try (Connection connection = dbConnect.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareCall("select * from cm_common.file where id = ?")) {
                preparedStatement.setInt(1, fileId);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return new FileWrapper(0, resultSet.getString("original_name"), resultSet.getString("path"));
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public OperationResponse removeDocFile(String token, int docId, int fileId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        FileWrapper fileWrapper = this.getFileByFileId(fileId);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.remove_doc_file(?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setInt(2, docId);
                callableStatement.setInt(3, fileId);
                callableStatement.executeUpdate();

                operationResponse.setData(fileWrapper);
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse removeEmployee(int personId, EmployeeHistoryForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{call cm_caremed.remove_employee(?,?,?,?,?)}")) {

                callableStatement.setString(1, form.getToken());
                callableStatement.setInt(2, personId);
                callableStatement.setInt(3, form.getReasonId());
                callableStatement.setString(4, form.getNote());
                callableStatement.setString(5, form.getEndDate());

                callableStatement.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @Override
    public OperationResponse addTest(test t) {
       OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
       
       try(Connection connection = dbConnect.getConnection()){
       int id =0;
           try(CallableStatement call = connection.prepareCall("{ call cm_common.test(?,?,?)}")){
       call.registerOutParameter(1, Types.INTEGER);
       call.setString(2, t.getName());
       call.setString(3, t.getLastName());
       call.setInt(4,t.getAge());
       call.execute();
       operationResponse.setData(call.getInt(1));
       
       operationResponse.setCode(ResultCode.OK);
           
           
       }
           
           
           
           
       }catch(Exception e){
           
           
           
       log.error(e.getMessage(),e);
       }
       
       
       
       return operationResponse;
    }

}
