/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class ElectronalOrganization {
    private int id;
    private String address;
    private String positionName;
    private String orgName;
    private String startDate;
    private String endDate;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserId;
    private String note;

    public ElectronalOrganization() {
    }

    public ElectronalOrganization(int id, String address, String positionName, String orgName, String startDate, String endDate, String note) {
        this.id = id;
        this.address = address;
        this.positionName = positionName;
        this.orgName = orgName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ElectronalOrganization{" + "id=" + id + ", address=" + address + ", positionName=" + positionName + ", orgName=" + orgName + ", startDate=" + startDate + ", endDate=" + endDate + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + ", note=" + note + '}';
    }
    
}
