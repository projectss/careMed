/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class Education {
    private int id;
    private DictionaryWrapper eduLevelId;
    private DictionaryWrapper institutionType;
    private String institutionName;
    private String institutionAddress;
    private String facultyName;
    private String startDate;
    private String endDate;
    private String diplomName;
    private String note;
    private String createDate;
    private int createUserId;
    private String updateDate;
    private int updateUserId;

    public Education() {
    }

    public Education(int id, DictionaryWrapper eduLevelId, DictionaryWrapper institutionType, String institutionName, String institutionAddress, String facultyName, String startDate, String endDate, String diplomName, String note) {
        this.id = id;
        this.eduLevelId = eduLevelId;
        this.institutionType = institutionType;
        this.institutionName = institutionName;
        this.institutionAddress = institutionAddress;
        this.facultyName = facultyName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.diplomName = diplomName;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getEduLevelId() {
        return eduLevelId;
    }

    public void setEduLevelId(DictionaryWrapper eduLevelId) {
        this.eduLevelId = eduLevelId;
    }

    public DictionaryWrapper getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(DictionaryWrapper institutionType) {
        this.institutionType = institutionType;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDiplomName() {
        return diplomName;
    }

    public void setDiplomName(String diplomName) {
        this.diplomName = diplomName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getInstitutionAddress() {
        return institutionAddress;
    }

    public void setInstitutionAddress(String institutionAddress) {
        this.institutionAddress = institutionAddress;
    }

    @Override
    public String toString() {
        return "Education{" + "id=" + id + ", eduLevelId=" + eduLevelId + ", institutionType=" + institutionType + ", institutionName=" + institutionName + ", institutionAddress=" + institutionAddress + ", facultyName=" + facultyName + ", startDate=" + startDate + ", endDate=" + endDate + ", diplomName=" + diplomName + ", note=" + note + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }
}
