/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class AwardInfo {
  private int id;
  private String startDate;
  private String awardName;
  private DictionaryWrapper typeId;
  private String note;
  private String createDate;
  private int createUserId;
  private String updateDate;
  private int updateUserId;

    public AwardInfo() {
    }

    public AwardInfo(int id, String startDate, String awardName, DictionaryWrapper typeId, String note) {
        this.id = id;
        this.startDate = startDate;
        this.awardName = awardName;
        this.typeId = typeId;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getAwardName() {
        return awardName;
    }

    public void setAwardName(String awardName) {
        this.awardName = awardName;
    }

    public DictionaryWrapper getTypeId() {
        return typeId;
    }

    public void setTypeId(DictionaryWrapper typeId) {
        this.typeId = typeId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Override
    public String toString() {
        return "AwardInfo{" + "id=" + id + ", startDate=" + startDate + ", awardName=" + awardName + ", typeId=" + typeId + ", note=" + note + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }    
}
