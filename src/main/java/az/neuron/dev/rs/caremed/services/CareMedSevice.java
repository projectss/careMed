/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.services;

import az.neuron.dev.rs.caremed.dao.EmployeeDao;
import az.neuron.dev.rs.caremed.dao.PatientDao;
import az.neuron.dev.rs.caremed.dao.StructureDao;
import az.neuron.dev.rs.caremed.domain.AbroadInfo;
import az.neuron.dev.rs.caremed.domain.AcademicInfo;
import az.neuron.dev.rs.caremed.domain.AwardInfo;
import az.neuron.dev.rs.caremed.domain.CaseRecord;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.Education;
import az.neuron.dev.rs.caremed.domain.ElectronalOrganization;
import az.neuron.dev.rs.caremed.domain.Employee;
import az.neuron.dev.rs.caremed.domain.EmployeeList;
import az.neuron.dev.rs.caremed.domain.Employment;
import az.neuron.dev.rs.caremed.domain.FileWrapper;
import az.neuron.dev.rs.caremed.domain.MilitaryInfo;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.PersonAddress;
import az.neuron.dev.rs.caremed.domain.PersonContact;
import az.neuron.dev.rs.caremed.domain.PersonDocument;
import az.neuron.dev.rs.caremed.domain.PersonLanguage;
import az.neuron.dev.rs.caremed.domain.PersonRelationship;
import az.neuron.dev.rs.caremed.domain.ResearchInfo;
import az.neuron.dev.rs.caremed.domain.User;
import az.neuron.dev.rs.caremed.domain.Vacation;
import az.neuron.dev.rs.caremed.form.AbroadForm;
import az.neuron.dev.rs.caremed.form.AcademicInfoForm;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.AwardForm;
import az.neuron.dev.rs.caremed.form.CaseRecordForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.DocumentForm;
import az.neuron.dev.rs.caremed.form.EducationForm;
import az.neuron.dev.rs.caremed.form.ElectronalOrgForm;
import az.neuron.dev.rs.caremed.form.EmployeeForm;
import az.neuron.dev.rs.caremed.form.EmployeeHistoryForm;
import az.neuron.dev.rs.caremed.form.EmployementForm;
import az.neuron.dev.rs.caremed.form.MembershipForm;
import az.neuron.dev.rs.caremed.form.MilitaryInfoForm;
import az.neuron.dev.rs.caremed.form.PFile;
import az.neuron.dev.rs.caremed.form.PatientForm;
import az.neuron.dev.rs.caremed.form.PersonForm;
import az.neuron.dev.rs.caremed.form.PersonLanguageForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import az.neuron.dev.rs.caremed.form.ResearchForm;
import az.neuron.dev.rs.caremed.form.VacationForm;
import az.neuron.dev.rs.caremed.search.EmployeeSearchForm;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import az.neuron.dev.rs.caremed.dao.IOperationDao;

/**
 *
 * @author Bayram
 */
@Service
public class CareMedSevice implements ICareMedService {

    @Autowired
    private StructureDao structureDao;

    @Autowired
    private EmployeeDao employeeDao;
    
    @Autowired
    private PatientDao patientDao;
    

    @Override
    public User checkToken(String token) {
        return this.structureDao.checkToken(token);
    }

    @Override
    public OperationResponse addEmployee(EmployeeForm form, String imageFullpath, String imageOriginalName) {
        return this.employeeDao.addEmployee(form, imageFullpath, imageOriginalName);
    }

    @Override
    public OperationResponse addEmployeeFile(String token, int id, int docId, PFile file) {
        return this.employeeDao.addEmployeeFile(token, id, docId, file);
    }

    @Override
    public DataTable getEmployeeList(EmployeeSearchForm form) {
        return this.employeeDao.getEmployeeList(form);
    }

    @Override
    public Employee getEmployeeDetails(String token, int personId) {
        return this.employeeDao.getEmployeeDetails(token, personId);
    }

    @Override
    public Employee getEmployeePersonalInfo(String token, int personId) {
        return this.employeeDao.getEmployeePersonalInfo(token, personId);
    }


    @Override
    public FileWrapper getEmployeeImage(int id) {
        return this.employeeDao.getEmployeeImage(id);
    }

    @Override
    public OperationResponse checkPincode(String pincode) {
       return this.employeeDao.checkPincode(pincode);
    }

    @Override
    public OperationResponse editPesonContact(String token, int id, ContactForm form) {
       return this.employeeDao.editPesonContact(token, id, form);
    }

    @Override
    public OperationResponse editPersonAddress(String token, AddressForm form) {
       return this.employeeDao.editPersonAddress(token, form);
    }

    @Override
    public OperationResponse editPersonRelationship(String token, int id, PersonRelationForm form) {
       return this.employeeDao.editPersonRelationship(token, id, form);
    }

    @Override
    public OperationResponse addPersonContact(String token, int personId, ContactForm form) {
       return this.employeeDao.addPersonContact(token, personId, form);
    }

    @Override
    public OperationResponse removePersonContact(String token, int id, int personId) {
        return this.employeeDao.removePersonContact(token, id, personId);
    }

    @Override
    public OperationResponse addPersonAddress(String token, int id, AddressForm form) {
        return this.employeeDao.addPersonAddress(token, id, form);
    }

    @Override
    public OperationResponse removePersonAddress(String token, int id, int addressId) {
        return this.employeeDao.removePersonAddress(token, id, addressId);
    }
    
    @Override
    public List<PersonAddress> getPersonAddress(String token, int personId) {
        return this.employeeDao.getPersonAddress(token, personId);
    }

    @Override
    public OperationResponse editPersonalInfo(String token, PersonForm form, String imageFullpath, String imageOriginalName) {
        return this.employeeDao.editPersons(token, form, imageFullpath, imageOriginalName);
    }
    
    @Override
    public OperationResponse editMembershipInfo(String token, MembershipForm form){
        return this.employeeDao.editMembership(token, form);
    }

    @Override
    public OperationResponse editEmployee(EmployeeForm form) {
       return this.employeeDao.editEmployee(form);
    }
    
    @Override
    public List<PersonContact> getPersonContact(String token, int personId) {
        return this.employeeDao.getPersonContact(token, personId);
    }

    @Override
    public OperationResponse addPersonRelationShip(String token, int personId, PersonRelationForm form) {
        return this.employeeDao.addPersonRelationShip(token, personId, form);
    }
    
    @Override
    public List<PersonRelationship> getPersonRealtionship(String token, int personId) {
        return this.employeeDao.getPersonRealtionship(token, personId);
    }
    
    @Override
    public OperationResponse removePersonRelationship(String token, int id, int relationshipId) {
        return this.employeeDao.removePersonRelationship(token, id, relationshipId);
    }

    @Override
    public OperationResponse addEmployement(String token, int personId, EmployementForm form) {
        return this.employeeDao.addEmployement(token, personId, form);
    }

    @Override
    public OperationResponse editEmployement(String token, int personId, EmployementForm form) {
        return this.employeeDao.editEmployement(token, personId, form);
    }

    @Override
    public OperationResponse removeEmployment(String token, int id, int employmentId) {
        return this.employeeDao.removeEmployment(token, id, employmentId);
    }

    @Override
    public List<Employment> getEmploymentList(String token, int personId) {
        return this.employeeDao.getEmploymentList(token, personId);
    }

    @Override
    public OperationResponse addAbroadInfo(String token, int personId, AbroadForm form) {
        return this.employeeDao.addAbroadInfo(token, personId, form);
    }

    @Override
    public OperationResponse editAbroadInfo(String token, int id, AbroadForm form) {
        return this.employeeDao.editAbroadInfo(token, id, form);
    }

    @Override
    public OperationResponse removeAbroadInfo(String token, int id, int abroadId) {
        return this.employeeDao.removeAbroadInfo(token, id, abroadId);
    }

    @Override
    public List<AbroadInfo> getAbroadInfo(String token, int personId) {
        return this.employeeDao.getAbroadInfo(token, personId);
    }

    @Override
    public OperationResponse addVacationRecord(String token, int personId, VacationForm form) {
        return this.employeeDao.addVacationRecord(token, personId, form);
    }

    @Override
    public OperationResponse editVacationRecord(String token, int id, VacationForm form) {
        return this.employeeDao.editVacationRecord(token, id, form);
    }

    @Override
    public OperationResponse removeVacationRecord(String token, int id, int vacationId) {
        return this.employeeDao.removeVacationRecord(token, id, vacationId);
    }

    @Override
    public List<Vacation> getVacationList(String token, int perosonId) {
        return this.employeeDao.getVacationList(token, perosonId);
    }

    @Override
    public OperationResponse addAward(String token, int personId, AwardForm form) {
        return this.employeeDao.addAward(token, personId, form);
    }

    @Override
    public OperationResponse editAward(String token, int id, AwardForm form) {
        return this.employeeDao.editAward(token, id, form);
    }

    @Override
    public OperationResponse removeAward(String token, int id, int awardId) {
        return this.employeeDao.removeAward(token, id, awardId);
    }

    @Override
    public List<AwardInfo> getAwardInfo(String token, int personId) {
        return this.employeeDao.getAwardInfo(token, personId);
    }
    
    @Override
    public OperationResponse addElectronalOrgaznization(String token, ElectronalOrgForm form) {
        return this.employeeDao.addElectronalOrgaznization(token, form.getPersonId(), form);
    }

    @Override
    public List<ElectronalOrganization> getElectronalOrgList(String token, int personId) {
        return this.employeeDao.getPersonElectronalOrganization(token, personId);
    }

    @Override
    public OperationResponse addEducation(String token, int personId, EducationForm form) {
        return this.employeeDao.addEducation(token, personId, form);
    }

    @Override
    public OperationResponse editEducation(String token, int id, EducationForm form) {
        return this.employeeDao.editEducation(token, id, form);
    }

    @Override
    public OperationResponse removeEducation(String token, int id, int educationId) {
        return this.employeeDao.removeEducation(token, id, educationId);
    }

    @Override
    public List<Education> getPersonEducation(String token, int personId) {
        return this.employeeDao.getPersonEducation(token, personId);
    }

    @Override
    public OperationResponse addPersonLanguage(String token, int personId, PersonLanguageForm form) {
        return this.employeeDao.addPersonLanguage(token, personId, form);
    }

    @Override
    public OperationResponse editPersonLanguage(String token, int id, PersonLanguageForm language) {
        return this.employeeDao.editPersonLanguage(token, id, language);
    }

    @Override
    public OperationResponse removePersonLanguage(String token, int id, int languageId) {
        return this.employeeDao.removePersonLanguage(token, id, languageId);
    }

    @Override
    public List<PersonLanguage> getPersonLanguage(String token, int personId) {
        return this.employeeDao.getPersonLanguage(token, personId);
    }

    @Override
    public OperationResponse addAcademicInfo(String token, int personId, AcademicInfoForm form) {
        return this.employeeDao.addAcademicInfo(token, personId, form);
    }

    @Override
    public OperationResponse editAcademicInfo(String token, int id, AcademicInfoForm form) {
        return this.employeeDao.editAcademicInfo(token, id, form);
    }

    @Override
    public OperationResponse removeAcademicInfo(String token, int id, int academicId) {
        return this.employeeDao.removeAcademicInfo(token, id, academicId);
    }

    @Override
    public List<AcademicInfo> getPersonAcademicInfo(String token, int personId) {
        return this.employeeDao.getPersonAcademicInfo(token, personId);
    }

    @Override
    public OperationResponse addPersonResearchInfo(String token, int personId, ResearchForm form) {
        return this.employeeDao.addPersonResearchInfo(token, personId, form);
    }

    @Override
    public OperationResponse editResearchInfo(String token, int id, ResearchForm form) {
        return this.employeeDao.editResearchInfo(token, id, form);
    }

    @Override
    public OperationResponse removeResearchInfo(String token, int id, int researchId) {
        return this.employeeDao.removeResearchInfo(token, id, researchId);
    }

    @Override
    public List<ResearchInfo> getResearchList(String token, int personId) {
        return this.employeeDao.getResearchList(token, personId);
    }

    @Override
    public OperationResponse editElectronalOrganization(String token, ElectronalOrgForm form) {
        return this.employeeDao.editElectronalOrganization(token, form);
    }
    
    @Override
    public OperationResponse removeElectronalOrganization(String token, int id, int electoralId) {
        return this.employeeDao.removeElectronalOrganization(token, id, electoralId);
    }


    @Override
    public OperationResponse addMilitary(String token, MilitaryInfoForm form) {
        return this.employeeDao.addMillitaryInfo(token, form);
    }

    @Override
    public List<MilitaryInfo> getMilitaryList(String token, int personId) {
        return this.employeeDao.getMillitaryList(token, personId);
    }
    
////-------------------------------------------------------------------------------------------------------------------
//    @Override
//    public OperationResponse addPatient(String token, PatientForm form, String imageFullpath, String imageOriginalName) {
//        return this.patientDao.addPatient(token, form, imageFullpath, imageOriginalName);
//    }

    @Override
    public OperationResponse removeMillitaryInfo(String token, int id, int militaryId) {
        return this.employeeDao.removeMillitaryInfo(token, id, militaryId);
    }

    @Override
    public OperationResponse editMillitaryInfo(String token, int id, MilitaryInfoForm form) {
        return this.employeeDao.editMillitaryInfo(token, id, form);
    }

    @Override
    public OperationResponse addDocument(DocumentForm form, String token, int personId) {
        return this.employeeDao.addDocument(form, token, personId);
    }
    
    @Override
    public OperationResponse editPersonDocument(String token, int id, DocumentForm form) {
        return this.employeeDao.editPersonDocument(token, id, form);
    }

    @Override
    public OperationResponse removePersonDocument(String token, int id, int documentId) {
        return this.employeeDao.removePersonDocument(token, id, documentId);
    }

    @Override
    public List<PersonDocument> getPersonDocument(String token, int personId) {
        return this.employeeDao.getPersonDocument(token, personId);
    }
    
    @Override
    public OperationResponse addDocFile(DocumentForm form, String token) {
        return this.employeeDao.addDocFile(form, token);
    }
    
    @Override
    public FileWrapper getFileByFileId(int fileId) {
        return this.employeeDao.getFileByFileId(fileId);
    }
    
    @Override
    public OperationResponse removeDocFile(String token, int docId, int fileId) {
        return this.employeeDao.removeDocFile(token, docId, fileId);
    }

    @Override
    public OperationResponse removeEmployee(int personId, EmployeeHistoryForm form) {
      return this.employeeDao.removeEmployee(personId, form);
    }

    @Override
    public OperationResponse addCaseRecord(String token, int personId, CaseRecordForm form) {
        return this.employeeDao.addCaseRecord(token, personId, form);
    }

    @Override
    public OperationResponse editCaseRecord(String token, int id, CaseRecordForm form) {
       return this.employeeDao.editCaseRecord(token, id, form);
    }

    @Override
    public OperationResponse removeCaseRecord(String token, int id, int caseId) {
        return this.employeeDao.removeCaseRecord(token, id, caseId);
    }

    @Override
    public List<CaseRecord> getCaseList(String token, int personId) {
    return this.employeeDao.getCaseList(token, personId);
    }

   
}
