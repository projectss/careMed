/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 *
 * @author Bayram
 */

@Data
@AllArgsConstructor
@ToString
public class PatienForwardByDoctor {
    private int id;
    private DictionaryWrapper orgId;
    private DictionaryWrapper services;
    private DictionaryWrapper serviceType;
    private DictionaryWrapper queueType;
    private DictionaryWrapper paymentType;
    
    
    
}
