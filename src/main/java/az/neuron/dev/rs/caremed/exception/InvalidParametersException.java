/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.exception;

/**
 *
 * @author Bayram
 */
public class InvalidParametersException extends CareMedException {

    public InvalidParametersException(String message) {
        super(message);
    }
}
