/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.validator;

import az.neuron.dev.rs.caremed.enums.Constants;
import az.neuron.dev.rs.caremed.enums.Regex;
import az.neuron.dev.rs.caremed.form.*;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Bayram
 */
public class EmployeeFormValidator implements Validator {

    private static String type;

    public EmployeeFormValidator(String type) {
        this.type = type;

    }

    @Override
    public boolean supports(Class<?> type) {
        return EmployeeForm.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        EmployeeForm employeeForm = (EmployeeForm) o;

            if (Constants.VALIDATOR_ADD.equals(type)) {
            if (employeeForm.getFistName() == null || employeeForm.getFistName().isEmpty()) {
                errors.reject("invalid employee firstname");

            }
            if (employeeForm.getLastName() == null || employeeForm.getLastName().isEmpty()) {

                errors.reject("invalid employee lastname");
            }
            if (employeeForm.getMiddleName() == null || employeeForm.getMiddleName().isEmpty()) {
                errors.reject("invalid employee middlename");

            }
            if (employeeForm.getGenderId() <= 0) {

                errors.reject("invalid employee gender id");

            }
            if (employeeForm.getBirthdate() == null || employeeForm.getBirthdate().trim().isEmpty() /*|| !employeeForm.getBirthdate().matches(Regex.DATE)*/ ) {
                errors.reject("Invalid birthdate");
            }

            if (employeeForm.getCitizenshipId() <= 0) {
                errors.reject("Invalid citizenship id");

            }

            if (employeeForm.getSvSeriya() <= 0) {

                errors.reject("Invalid sv_seriya ");
            }

            if (employeeForm.getSeriya() == null || employeeForm.getSeriya().isEmpty()) {

                errors.reject("Invalid seriya");
            }

            if (employeeForm.getSupplyOrg() <= 0) {

                errors.reject("Invalid supply organization name");
            }

            if (employeeForm.getMaritalStatusId() <= 0) {
                errors.reject("Invalid marital status id");
            }

            if (employeeForm.getChildCount() == null || employeeForm.getChildCount() < 0) {

                errors.reject("Invalid child count");
            }
            if (employeeForm.getMillitaryServiceId() <= 0) {
                errors.reject("Invalid millitary service id");
            }

            if (employeeForm.getNationalityId() < 0) {
                errors.reject("Invalid nationality id");
            }

            if (employeeForm.getSocialStatusId() < 0) {

                errors.reject("Invalid social status id");
            }

            if (employeeForm.getBloodGroupId() <= 0) {

                errors.reject("Invalid blood id");
            }

       // ------------------- address validation ---------------------------
            if (employeeForm.getAddresses().length > 0) {
                for (AddressForm form : employeeForm.getAddresses()) {

                    if (form.getAddressId() <= 0) {
                        errors.reject("Invalid address id");
                    }
                    if (form.getTypeId() <= 0) {
                        errors.reject("Invalid type id");
                    }
                    if (form.getTypeId() != 1000026 && form.getAddress().trim().isEmpty()) {

                        errors.reject("Invalid birth place");
                    }

                }
            }

       // --------------------------------------------------------------------
       // ----------- contact validation ----------------------------------------
            if (employeeForm.getContacts().length > 0) {

                for (ContactForm form : employeeForm.getContacts()) {

                    if (form.getContact() == null || form.getContact().isEmpty()) {

                        errors.reject("invalid employee contact");
                    }

                    if (form.getTypeId() <= 0) {
                        errors.reject("invalid contact type id");
                    }

                }

            }

       //-------------------------------------------------------------------     
       //----------------- document validation -------------------------------------
            if (employeeForm.getDocuments().length > 0) {

                for (DocumentForm form : employeeForm.getDocuments()) {
                    if (form.getTypeId() <= 0) {

                        errors.reject("invalid document type id");
                    }

                    if (form.getSerial() == null || form.getSerial().isEmpty()) {
                        errors.reject("invalid document seria ");
                    }

                    if (form.getNumber() == null || form.getNumber().isEmpty()) {

                        errors.reject("invalid document number");
                    }

                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {

                        errors.reject("invalid start date");
                    }

                }

            }

       // -------------------------------------------------------------------------------------------  
            // ----------RelationShip validation --------------------------------------------------
            if (employeeForm.getRelations().length > 0) {
                for (PersonRelationForm form : employeeForm.getRelations()) {

                    if (form.getRelationshipId() <= 0) {
                        errors.reject("invalid realtionship id");

                    }
                    if (form.getFullName() == null || form.getFullName().isEmpty()) {
                        errors.reject("invalid realtionship fullname ");

                    }

                }

            }

      //------------------- Employeee validation ----------------------------------------
            if (employeeForm.getSpecId() <= 0) {

                errors.reject("invalid spec id");

            }

            if (employeeForm.getPositionCatId() <= 0) {

                errors.reject("invalid position category id");
            }

            if (employeeForm.getPositionId() <= 0) {

                errors.reject("invalid position id");
            }

            if (employeeForm.getOrgId() <= 0) {
                errors.reject("invalid org id");
            }
            if (employeeForm.getStatId() <= 0) {

                errors.reject("invalid stat id");
            }
            if (employeeForm.getPayId() <= 0) {
                errors.reject("invalid pay id");
            }

            if (employeeForm.getStartDate() == null || employeeForm.getStartDate().isEmpty()) {

                errors.reject("invalid start date");
            }

     // -----------------------------------------------------------------------------------
            // --------------Employement validation --------------------------------------------------
            if (employeeForm.getEmployement().length > 0) {

                for (EmployementForm form : employeeForm.getEmployement()) {

                    if (form.getCompanyName() == null || form.getCompanyName().isEmpty()) {

                        errors.reject("invalid company name");
                    }

                    if (form.getPositionName() == null || form.getPositionName().isEmpty()) {

                        errors.reject("invalid company position ");
                    }

                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {

                        errors.reject("invalid start date");
                    }

                    if (form.getEndDate() == null || form.getEndDate().isEmpty()) {

                        errors.reject("invalid end date");
                    }

                }

            }

    //-------------------------------------------------------------------------------------
    // -----------Abroad Info validation --------------------------------------------
            if (employeeForm.getAbroad().length > 0) {
                for (AbroadForm form : employeeForm.getAbroad()) {
                    if (form.getCountryId() <= 0) {
                        errors.reject("invalid contry id");

                    }

                    if (form.getReasonId() <= 0) {
                        errors.reject("invalid reason id");
                    }

                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {

                        errors.reject("invalid start date ");
                    }

                    if (form.getEndDate() == null || form.getEndDate().isEmpty()) {

                        errors.reject("invalid end date");
                    }

                }
            }
     // -------------------------------------------------------------------------

    // --------- Award validation ------------------------------------------------
            if (employeeForm.getAward().length > 0) {

                for (AwardForm form : employeeForm.getAward()) {
                    if (form.getAwardName() == null || form.getAwardName().isEmpty()) {
                        errors.reject("invalid award name");

                    }
                    if (form.getTypeId() <= 0) {
                        errors.reject("invalid award type id");
                    }

                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {

                        errors.reject("invalid start date");
                    }

                }
            }

     // ------------------------------------------------------------------------
            // -----------Vacation  validation ----------------------------------
            if (employeeForm.getVacationForm().length > 0) {

                for (VacationForm form : employeeForm.getVacationForm()) {

                    if (form.getTypeId() <= 0) {

                        errors.reject("invalid type id");
                    }

                    if (form.getPeriod() == null || form.getPeriod().isEmpty()) {

                        errors.reject("invalid period");
                    }

                    if (form.getDayCount() <= 0) {

                        errors.reject("invalid day count");
                    }

                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {
                        errors.reject("invalid start date");
                    }

                    if (form.getEndDate() == null || form.getEndDate().isEmpty()) {
                        errors.reject("invalid end date");
                    }

                }

            }
    // ---------------------------------------------------------------------------------

    // -------------Case record validation -----------------------------------------
            if (employeeForm.getCaseRecordForms().length > 0) {

                for (CaseRecordForm form : employeeForm.getCaseRecordForms()) {

                    if (form.getCaseNo() == null || form.getCaseNo().isEmpty()) {

                        errors.reject("invalid case record no");
                    }

                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {

                        errors.reject("invalid start date");
                    }

                    if (form.getEndDate() == null || form.getEndDate().isEmpty()) {

                        errors.reject("invalid end date");
                    }

                    if (form.getDayCount() <= 0) {

                        errors.reject("invalid day count");
                    }

                }

            }

      // ------------------------------------------------------------------------------------
            // ----------------Education validation -------------------------------------------
            if (employeeForm.getEducation().length > 0) {
                for (EducationForm form : employeeForm.getEducation()) {

                    if (form.getEduLevelId() <= 0) {

                        errors.reject("invalid edu level id");
                    }

                    if (form.getInstitutionType() <= 0) {
                        errors.reject("invalid institution type");
                    }
                    if (form.getInstitutionName() == null || form.getInstitutionName().isEmpty()) {

                        errors.reject("invalid institution name");
                    }

                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {
                        errors.reject("invalid start date");
                    }

                    if (form.getEndDate() == null || form.getEndDate().isEmpty()) {
                        errors.reject("invalid end date");
                    }

                    if (form.getDiplomNumber() == null || form.getDiplomNumber().isEmpty()) {

                        errors.reject("invalid diplom number");
                    }

                }
            }
     // -------------------------------------------------------------------------------

            // -----------Language validation -------------------------------------------------
            if (employeeForm.getLanguageForms().length > 0) {
                for (PersonLanguageForm form : employeeForm.getLanguageForms()) {
                    if (form.getLanguageId() <= 0) {

                        errors.reject("invalid language id");
                    }

                    if (form.getDegreeId() <= 0) {
                        errors.reject("invalid degree id");
                    }

                }

            }

    // --------------------------------------------------------------------------------
            // --------- Academi ifno validation -----------------------------------------------
            if (employeeForm.getAcademicInfo().length > 0) {

                for (AcademicInfoForm form : employeeForm.getAcademicInfo()) {

                    if (form.getTypeId() <= 0) {
                        errors.reject("invalid type id");

                    }

                    if (form.getAcademicInfoId() <= 0) {
                        errors.reject("invalid academic info id");
                    }

                }

            }

   //----------------------------------------------------------------------------------       
            // -----------------Research validation ----------------------------------------
            if (employeeForm.getResearch().length > 0) {

                for (ResearchForm form : employeeForm.getResearch()) {

                    if (form.getResearchType() <= 0) {
                        errors.reject("invalid research type");
                    }
                    if (form.getResearchName() == null || form.getResearchName().isEmpty()) {

                        errors.reject("invalid research name");
                    }
                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {
                        errors.reject("invalid start date");
                    }

                }

            }
   //------------------------------------------------------------------------------  

            // ----------- Millitary  validation ------------------------------------------
            if (employeeForm.getMillitatyInfo().length > 0) {

                for (MilitaryInfoForm form : employeeForm.getMillitatyInfo()) {

                    if (form.getArmyType() <= 0) {
                        errors.reject("invalid army type");
                    }
                    if (form.getStaffType() <= 0) {
                        errors.reject("invalid staff type");
                    }
                    if (form.getMillitaryNameId() <= 0) {
                        errors.reject("invalid millitary name id");
                    }

                }
            }
   // -----------------------------------------------------------------------------       

            //----------Electronal orgranization validation --------------------------------
            if (employeeForm.getElectronalOrg().length > 0) {
                for (ElectronalOrgForm form : employeeForm.getElectronalOrg()) {

                    if (form.getAddress() == null || form.getAddress().isEmpty()) {

                        errors.reject("invalid address");
                    }

                    if (form.getOrgName() == null || form.getOrgName().isEmpty()) {
                        errors.reject("invalid org name ");
                    }

                    if (form.getPositionName() == null || form.getPositionName().isEmpty()) {
                        errors.reject("position name");
                    }

                    if (form.getStartDate() == null || form.getStartDate().isEmpty()) {
                        errors.reject("invalid start date");
                    }
                    if (form.getEndDate() == null || form.getEndDate().isEmpty()) {
                        errors.reject("invalid end date");
                    }
                }

            }

   //------------------------------------------------------------------------------         
        }

    }

}
