/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class ResearchForm {
    private int id;
    private int researchType;
    private String researchName;
    private String startDate;
    private String note;
    
    ResearchForm(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getResearchType() {
        return researchType;
    }

    public void setResearchType(int researchType) {
        this.researchType = researchType;
    }

    public String getResearchName() {
        return researchName;
    }

    public void setResearchName(String researchName) {
        this.researchName = researchName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @Override
    public String toString() {
        return "ResearchForm{" + "id=" + id + ", researchType=" + researchType + ", researchName=" + researchName + ", startDate=" + startDate + ", note=" + note + '}';
    }
}
