/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.validator;

import az.neuron.dev.rs.caremed.enums.Constants;
import az.neuron.dev.rs.caremed.form.ServicesForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Bayram
 */
public class ServicesFormValidator implements Validator{
    private static String type;

    public ServicesFormValidator(String type) {
        this.type = type;
    }
    
    
    @Override
    public boolean supports(Class<?> type) {
        return ServicesForm.class.isAssignableFrom(type);
    }
    

    @Override
    public void validate(Object o, Errors errors) {
       ServicesForm serviceForm = (ServicesForm)o;
       
       if (Constants.VALIDATOR_ADD.equals(type) || Constants.VALIDATOR_EDIT.equals(type)){
           if (serviceForm.getServiceTypeId() <= 0){
               errors.reject("empty service type id");
           }
//           if (serviceForm.getTypeId() <= 0){
//               errors.reject("empty type id");
//           }
//           if (serviceForm.getOrgId() <= 0){
//               errors.reject("empty org id");
//           }
           if (serviceForm.getCost() <= 0){
               errors.reject("empty cost");
           }
           if (serviceForm.getCurrencyId() <= 0){
               errors.reject("empty currency id");
           }
           if (serviceForm.getName() == null || serviceForm.getName().isEmpty()){
               errors.reject("empty name");
           }
           if (serviceForm.getDuration() == null || serviceForm.getDuration().isEmpty()){
               errors.reject("empty duration");
           }
           if(serviceForm.getServiceCode() == null || serviceForm.getServiceCode().isEmpty()){
               errors.reject("empty service code");
           }
           
       }
    }
}
