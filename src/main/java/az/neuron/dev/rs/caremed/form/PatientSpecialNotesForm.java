/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

import lombok.Data;

/**
 *
 * @author Admin
 */
public @Data
class PatientSpecialNotesForm {

    private int id;
    private int patientId;
    private int typeId;
    private String note;
    private String orginalName;
    private String path;
    private int size;
    private String contentType;

}
