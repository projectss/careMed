/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class EmployeeForm extends PersonForm {
   
    private int electoralOrgName;
    private String electoralPeriod;
    private String electoralCardNum;
    private int specId;
    private int positionId;
    private int positionCatId;
    private int orgId;
    private int statId;
    private int payId;
    private String startDate;
    private String note;
    private Integer imageId;
    private PersonLanguageForm[] languageForms;
    private EmployementForm[] employement;
    private AbroadForm[] abroad;
    private AcademicInfoForm[] academicInfo;
    private AwardForm[] award;
    private EducationForm[] education;
    private ElectronalOrgForm[] electronalOrg;
    private MilitaryInfoForm[] millitatyInfo;
    private ResearchForm[] research;
    private VacationForm[] vacationForm;
    private CaseRecordForm[] caseRecordForms;

    public EmployeeForm() {
    }

    public int getElectoralOrgName() {
        return electoralOrgName;
    }

    public void setElectoralOrgName(int electoralOrgName) {
        this.electoralOrgName = electoralOrgName;
    }

  

    public String getElectoralPeriod() {
        return electoralPeriod;
    }

    public void setElectoralPeriod(String electoralPeriod) {
        this.electoralPeriod = electoralPeriod;
    }

    public String getElectoralCardNum() {
        return electoralCardNum;
    }

    public void setElectoralCardNum(String electoralCardNum) {
        this.electoralCardNum = electoralCardNum;
    }

    public int getSpecId() {
        return specId;
    }

    public void setSpecId(int specId) {
        this.specId = specId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public int getPositionCatId() {
        return positionCatId;
    }

    public void setPositionCatId(int positionCatId) {
        this.positionCatId = positionCatId;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getStatId() {
        return statId;
    }

    public void setStatId(int statId) {
        this.statId = statId;
    }

    public int getPayId() {
        return payId;
    }

    public void setPayId(int payId) {
        this.payId = payId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public PersonLanguageForm[] getLanguageForms() {
        return languageForms;
    }

    public void setLanguageForms(PersonLanguageForm[] languageForms) {
        this.languageForms = languageForms;
    }

    public EmployementForm[] getEmployement() {
        return employement;
    }

    public void setEmployement(EmployementForm[] employement) {
        this.employement = employement;
    }

    public AbroadForm[] getAbroad() {
        return abroad;
    }

    public void setAbroad(AbroadForm[] abroad) {
        this.abroad = abroad;
    }

    public AcademicInfoForm[] getAcademicInfo() {
        return academicInfo;
    }

    public void setAcademicInfo(AcademicInfoForm[] academicInfo) {
        this.academicInfo = academicInfo;
    }

    public AwardForm[] getAward() {
        return award;
    }

    public void setAward(AwardForm[] award) {
        this.award = award;
    }

    public EducationForm[] getEducation() {
        return education;
    }

    public void setEducation(EducationForm[] education) {
        this.education = education;
    }

    public ElectronalOrgForm[] getElectronalOrg() {
        return electronalOrg;
    }

    public void setElectronalOrg(ElectronalOrgForm[] electronalOrg) {
        this.electronalOrg = electronalOrg;
    }

    public MilitaryInfoForm[] getMillitatyInfo() {
        return millitatyInfo;
    }

    public void setMillitatyInfo(MilitaryInfoForm[] millitatyInfo) {
        this.millitatyInfo = millitatyInfo;
    }

    public ResearchForm[] getResearch() {
        return research;
    }

    public void setResearch(ResearchForm[] research) {
        this.research = research;
    }

    public VacationForm[] getVacationForm() {
        return vacationForm;
    }

    public void setVacationForm(VacationForm[] vacationForm) {
        this.vacationForm = vacationForm;
    }

    public CaseRecordForm[] getCaseRecordForms() {
        return caseRecordForms;
    }

    public void setCaseRecordForms(CaseRecordForm[] caseRecordForms) {
        this.caseRecordForms = caseRecordForms;
    }

    @Override
    public String toString() {
        return "EmployeeForm{" + "electoralOrgName=" + electoralOrgName + ", electoralPeriod=" + electoralPeriod + ", electoralCardNum=" + electoralCardNum + ", specId=" + specId + ", positionId=" + positionId + ", positionCatId=" + positionCatId + ", orgId=" + orgId + ", statId=" + statId + ", payId=" + payId + ", startDate=" + startDate + ", note=" + note + ", imageId=" + imageId + ", languageForms=" + languageForms + ", employement=" + employement + ", abroad=" + abroad + ", academicInfo=" + academicInfo + ", award=" + award + ", education=" + education + ", electronalOrg=" + electronalOrg + ", millitatyInfo=" + millitatyInfo + ", research=" + research + ", vacationForm=" + vacationForm + ", caseRecordForms=" + caseRecordForms + '}';
    }

   

}
