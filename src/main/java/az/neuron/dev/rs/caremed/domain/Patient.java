/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

import java.util.List;
import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Bayram
 */
@Data
@ToString
public class Patient extends Person {

    private int number;
    private int patientId;
    private String patientDocNum;
    private String registrationDate;
    private String patientNote;
    private DictionaryWrapper companyNameId;
    private DictionaryWrapper positionId;
    private DictionaryWrapper typeId;
    private DictionaryWrapper status;
    private DictionaryWrapper service;
    private int serviseOperationId;

    public Patient() {
    }

    public Patient(int patientId, int number, String patientDocNum, String registrationDate, DictionaryWrapper companyNameId, DictionaryWrapper positionId, DictionaryWrapper typeId, int id, String firstname, String lastname, String middlename, DictionaryWrapper genderId, String birthdate) {
        super(id, firstname, lastname, middlename, genderId, birthdate);
        this.patientId = patientId;
        this.number = number;
        this.patientDocNum = patientDocNum;
        this.registrationDate = registrationDate;
        this.companyNameId = companyNameId;
        this.positionId = positionId;
        this.typeId = typeId;
    }

    public Patient(String patientDocNum, String registrationDate, String patientNote, DictionaryWrapper companyNameId, DictionaryWrapper positionId, DictionaryWrapper typeId, int id, String firstname, String lastname, String middlename, DictionaryWrapper citizenshipId, DictionaryWrapper maritalStatusId, DictionaryWrapper millitaryServiceId, DictionaryWrapper nationalityId, DictionaryWrapper socialStatusId, String birthdate, String pincode, DictionaryWrapper svSeriya, String seriya, DictionaryWrapper bloodGroupId, DictionaryWrapper supplyOrgId, String svEndDate, int height, DictionaryWrapper genderId, int photoFileId, int childCount, List<PersonAddress> addreses, List<PersonContact> contacts, List<PersonDocument> documents, List<PersonRelationship> relationships) {
        super(id, firstname, lastname, middlename, citizenshipId, maritalStatusId, millitaryServiceId, nationalityId, socialStatusId, birthdate, pincode, svSeriya, seriya, bloodGroupId, supplyOrgId, svEndDate, height, genderId, photoFileId, childCount, addreses, contacts, documents, relationships);
        this.patientDocNum = patientDocNum;
        this.registrationDate = registrationDate;
        this.patientNote = patientNote;
        this.companyNameId = companyNameId;
        this.positionId = positionId;
        this.typeId = typeId;
    }

    public Patient(int serviseOperationId,  int number, String patientDocNum, String registrationDate, DictionaryWrapper typeId, DictionaryWrapper status, int id, String firstname, String lastname, String middlename, DictionaryWrapper genderId, String birthdate, DictionaryWrapper service) {
        super(id, firstname, lastname, middlename, genderId, birthdate);
        this.serviseOperationId = serviseOperationId;
        this.number = number;
        this.patientDocNum = patientDocNum;
        this.registrationDate = registrationDate;
        this.typeId = typeId;
        this.status = status;
        this.service = service;
        
    }    

   
    public Patient(int patientId, int serviseOperationId, int number, String patientDocNum, String registrationDate, DictionaryWrapper typeId, DictionaryWrapper status, int id, String firstname, String lastname, String middlename, DictionaryWrapper genderId, String birthdate, DictionaryWrapper service) {
        super(id, firstname, lastname, middlename, genderId, birthdate);
        this.patientId = patientId;
        this.serviseOperationId = serviseOperationId;
        this.number = number;
        this.patientDocNum = patientDocNum;
        this.registrationDate = registrationDate;
        this.typeId = typeId;
        this.status = status;
        this.service = service;
        
    }    
}
