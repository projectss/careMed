/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.form;

/**
 *
 * @author Bayram
 */
public class PatientHamfullStuffForm extends BaseForm {

    private int id;
    private HamfullStuffForm[] hamfullStuffForms;

    public PatientHamfullStuffForm() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HamfullStuffForm[] getHamfullStuffForms() {
        return hamfullStuffForms;
    }

    public void setHamfullStuffForms(HamfullStuffForm[] hamfullStuffForms) {
        this.hamfullStuffForms = hamfullStuffForms;
    }

    @Override
    public String toString() {
        return "PatientHamfullStuffForm{" + "id=" + id + ", hamfullStuffForms=" + hamfullStuffForms + '}';
    }

}
