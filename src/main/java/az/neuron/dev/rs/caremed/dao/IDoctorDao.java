/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.dao;

import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.PatienForwardByDoctor;
import az.neuron.dev.rs.caremed.domain.Patient;
import az.neuron.dev.rs.caremed.form.CheckPasientForm;
import az.neuron.dev.rs.caremed.form.OperationForm;
import az.neuron.dev.rs.caremed.search.PatientSearchByDoctor;
import java.util.List;

/**
 *
 * @author Bayram
 */
public interface IDoctorDao {

    public DataTable getPatientListByDoctor(String token, PatientSearchByDoctor searchByDoctor);

    public OperationResponse addCheckUpPatient(String token, CheckPasientForm form);

    public OperationResponse addSubServicesByDoctor(String token, OperationForm operationForm);
    
    public List<PatienForwardByDoctor> getPatientForwardByDoctor(String token, int id);
    
}
