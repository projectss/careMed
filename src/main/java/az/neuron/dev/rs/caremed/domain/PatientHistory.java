/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class PatientHistory {

    private int number;
    private int patientId;
    private String patientFirstName;
    private String patientLastName;
    private String patientMiddleName;
    private String patientBirtdate;
    private DictionaryWrapper doctorId;
    private String doctorFirstName;
    private String doctorLastName;
    private String doctortMiddleName;
    private DictionaryWrapper doctorOrgId;
    private DictionaryWrapper doctorPositionId;
    private DictionaryWrapper patientServiceId;
    private DictionaryWrapper patientStatusId;
    private String startDate;
    private String endDate;
    private DictionaryWrapper patientQueueId;
    private DictionaryWrapper patientPaymentId;
    private String price;
    private String note;
    private int medicalCommitionId;

    public PatientHistory() {
    }

    public PatientHistory(int number, int patientId, String patientFirstName, String patientLastName, String patientMiddleName, String patientBirtdate, DictionaryWrapper doctorId, String doctorFirstName, String doctorLastName, String doctortMiddleName, DictionaryWrapper doctorOrgId, DictionaryWrapper doctorPositionId, DictionaryWrapper patientServiceId, DictionaryWrapper patientStatusId, String startDate, String endDate, DictionaryWrapper patientQueueId, DictionaryWrapper patientPaymentId, String price, String note, int medicalCommitionId) {
        this.number = number;
        this.patientId = patientId;
        this.patientFirstName = patientFirstName;
        this.patientLastName = patientLastName;
        this.patientMiddleName = patientMiddleName;
        this.patientBirtdate = patientBirtdate;
        this.doctorId = doctorId;
        this.doctorFirstName = doctorFirstName;
        this.doctorLastName = doctorLastName;
        this.doctortMiddleName = doctortMiddleName;
        this.doctorOrgId = doctorOrgId;
        this.doctorPositionId = doctorPositionId;
        this.patientServiceId = patientServiceId;
        this.patientStatusId = patientStatusId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.patientQueueId = patientQueueId;
        this.patientPaymentId = patientPaymentId;
        this.price = price;
        this.note = note;
        this.medicalCommitionId = medicalCommitionId;
    }

    
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientMiddleName() {
        return patientMiddleName;
    }

    public void setPatientMiddleName(String patientMiddleName) {
        this.patientMiddleName = patientMiddleName;
    }

    public String getPatientBirtdate() {
        return patientBirtdate;
    }

    public void setPatientBirtdate(String patientBirtdate) {
        this.patientBirtdate = patientBirtdate;
    }

    public DictionaryWrapper getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(DictionaryWrapper doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getDoctortMiddleName() {
        return doctortMiddleName;
    }

    public void setDoctortMiddleName(String doctortMiddleName) {
        this.doctortMiddleName = doctortMiddleName;
    }

    public DictionaryWrapper getDoctorOrgId() {
        return doctorOrgId;
    }

    public void setDoctorOrgId(DictionaryWrapper doctorOrgId) {
        this.doctorOrgId = doctorOrgId;
    }

    public DictionaryWrapper getDoctorPositionId() {
        return doctorPositionId;
    }

    public void setDoctorPositionId(DictionaryWrapper doctorPositionId) {
        this.doctorPositionId = doctorPositionId;
    }

    public DictionaryWrapper getPatientServiceId() {
        return patientServiceId;
    }

    public void setPatientServiceId(DictionaryWrapper patientServiceId) {
        this.patientServiceId = patientServiceId;
    }

    public DictionaryWrapper getPatientStatusId() {
        return patientStatusId;
    }

    public void setPatientStatusId(DictionaryWrapper patientStatusId) {
        this.patientStatusId = patientStatusId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public DictionaryWrapper getPatientQueueId() {
        return patientQueueId;
    }

    public void setPatientQueueId(DictionaryWrapper patientQueueId) {
        this.patientQueueId = patientQueueId;
    }

    public DictionaryWrapper getPatientPaymentId() {
        return patientPaymentId;
    }

    public void setPatientPaymentId(DictionaryWrapper patientPaymentId) {
        this.patientPaymentId = patientPaymentId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getMedicalCommitionId() {
        return medicalCommitionId;
    }

    public void setMedicalCommitionId(int medicalCommitionId) {
        this.medicalCommitionId = medicalCommitionId;
    }

    @Override
    public String toString() {
        return "PatientHistory{" + "number=" + number + ", patientId=" + patientId + ", patientFirstName=" + patientFirstName + ", patientLastName=" + patientLastName + ", patientMiddleName=" + patientMiddleName + ", patientBirtdate=" + patientBirtdate + ", doctorId=" + doctorId + ", doctorFirstName=" + doctorFirstName + ", doctorLastName=" + doctorLastName + ", doctortMiddleName=" + doctortMiddleName + ", doctorOrgId=" + doctorOrgId + ", doctorPositionId=" + doctorPositionId + ", patientServiceId=" + patientServiceId + ", patientStatusId=" + patientStatusId + ", startDate=" + startDate + ", endDate=" + endDate + ", patientQueueId=" + patientQueueId + ", patientPaymentId=" + patientPaymentId + ", price=" + price + ", note=" + note + ", medicalCommitionId=" + medicalCommitionId + '}';
    }

}
