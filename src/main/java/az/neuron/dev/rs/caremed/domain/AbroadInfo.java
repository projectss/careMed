/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class AbroadInfo {
   private int id;
   private DictionaryWrapper countryId;
   private DictionaryWrapper reasonId;
   private String startDate;
   private String endDate;
   private String note;
   private String createDate;
   private int createUserId;
   private String updateDate;
   private int updateUserId;

    public AbroadInfo() {
    }

    public AbroadInfo(int id, DictionaryWrapper countryId, DictionaryWrapper reasonId, String startDate, String endDate, String note) {
        this.id = id;
        this.countryId = countryId;
        this.reasonId = reasonId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getCountryId() {
        return countryId;
    }

    public void setCountryId(DictionaryWrapper countryId) {
        this.countryId = countryId;
    }

    public DictionaryWrapper getReasonId() {
        return reasonId;
    }

    public void setReasonId(DictionaryWrapper reasonId) {
        this.reasonId = reasonId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(int updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Override
    public String toString() {
        return "AbroadInfo{" + "id=" + id + ", countryId=" + countryId + ", reasonId=" + reasonId + ", startDate=" + startDate + ", endDate=" + endDate + ", note=" + note + ", createDate=" + createDate + ", createUserId=" + createUserId + ", updateDate=" + updateDate + ", updateUserId=" + updateUserId + '}';
    }
   
}
