/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.controller;

import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.User;
import az.neuron.dev.rs.caremed.enums.ResultCode;
import az.neuron.dev.rs.caremed.form.BaseForm;
import az.neuron.dev.rs.caremed.form.EmpStatusForm;
import az.neuron.dev.rs.caremed.form.EmployeeStatusForm;
import az.neuron.dev.rs.caremed.form.OperationForm;
import az.neuron.dev.rs.caremed.search.PatientCommitionHistory;
import az.neuron.dev.rs.caremed.search.PatientSearchForm;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Bayram
 */
@RestController
@RequestMapping(value = "/operation", produces = MediaType.APPLICATION_JSON_VALUE)

public class OperationController extends SkeletonController {

    private static final Logger log = Logger.getLogger(OperationController.class);

    @GetMapping(value = "/availableEmployeeList")
    protected OperationResponse getAvailableEmployeeList(EmployeeStatusForm form, BaseForm baseForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, baseForm.getToken());

            operationResponse.setData(operation.getEmployeeStatus(baseForm.getToken(), form));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @PostMapping(value = "/changeStatus")
    protected OperationResponse changeEmployeeStatus(BaseForm baseForm,
            EmpStatusForm empStatusForm) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse.setData(operation.changeEmployeeStatus(baseForm.getToken(), empStatusForm));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @PostMapping(value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse addPatientServiceOperation(BaseForm baseForm,
            @RequestPart(name = "form", required = false) OperationForm form) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse = operation.addPatientServiceOperation(baseForm.getToken(), form);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @PostMapping(value = "/getCommition")
    protected OperationResponse getPatientForCommition(BaseForm form, PatientSearchForm searchForm, BindingResult result) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, form.getToken());
            System.out.println("length " + searchForm.getLength() + "stat " + searchForm.getStart() + " order by " + searchForm.getOrderType() + "dsds");
            operationResponse.setData(operation.getPatientForCommition(form.getToken(), searchForm));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @PostMapping(value = "/{id:\\d+}/addMedicalCommition", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse addPatientMedicalCommition(BaseForm baseForm,
            @PathVariable("id") int id,
            @RequestPart(name = "form", required = false) OperationForm form
    ) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, baseForm.getToken());
            operationResponse = operation.addPatientMedicalCommition(baseForm.getToken(), id, form);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @PostMapping(value = "/{id:\\d+}/getPatientCommitionHistory")
    protected OperationResponse getPatientCommitionHistory(BaseForm form, @PathVariable("id") int id, PatientCommitionHistory commitionHistory, BindingResult result) {

        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {

            User user = checkToken(operationResponse, form.getToken());

            operationResponse.setData(operation.getCommitionHistory(form.getToken(), id, commitionHistory));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @GetMapping(value = "/{id:\\d+}/getServiceListForCommition")
    protected OperationResponse getServiceListFormCommition(BaseForm form, @PathVariable("id") int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(operationResponse, form.getToken());

            operationResponse.setData(operation.getServicesForMedicalCommition(form.getToken(), id));
            operationResponse.setCode(ResultCode.OK);
        } catch (Exception e) {

            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

}
