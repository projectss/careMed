/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 *
 * @author Bayram
 */
@Data
@ToString
@AllArgsConstructor
public class PatientProphylaxlsesVaccination {

    private int id;
    private int patientId;
    private DictionaryWrapper vaccinationId;
    private String dateOfVaccination;
    private String againstWhichInfaction;
    private String doze;
    private String numberOfSeries;
    private String result;
    private String note;
}
