/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.validator;

import az.neuron.dev.rs.caremed.enums.Constants;
import az.neuron.dev.rs.caremed.form.ContactForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Bayram
 */
public class ContactFormValidator implements Validator{
    private static String type;

    public ContactFormValidator(String type){
    this.type=type;
    }
    
    @Override
    public boolean supports(Class<?> type) {
      return ContactForm.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
       ContactForm form =(ContactForm)o;
       
         if (Constants.VALIDATOR_EDIT.equals(type) || Constants.VALIDATOR_ADD.equals(type)) {
            if ( form.getTypeId() <= 0) {
                errors.reject("Invalid contact type");
            }

            if (form.getContact() == null || form.getContact().trim().isEmpty()) {
                errors.reject("Invalid contact");
            }
            if (Constants.VALIDATOR_EDIT.equals(type)) {
                if ( form.getId() <= 0) {
                    errors.reject("Invalid Contact Id");
                }
            }
        }
        if (Constants.VALIDATOR_REMOVE.equals(type)) {
            if ( form.getId() <= 0) {
                errors.reject("Invalid Contact Id");
            }
        }
    }
    
}
