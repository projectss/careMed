/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.services;

import az.neuron.dev.rs.caremed.dao.DoctorDao;
import az.neuron.dev.rs.caremed.domain.DataTable;
import az.neuron.dev.rs.caremed.domain.OperationResponse;
import az.neuron.dev.rs.caremed.domain.PatienForwardByDoctor;
import az.neuron.dev.rs.caremed.domain.Patient;
import az.neuron.dev.rs.caremed.form.CheckPasientForm;
import az.neuron.dev.rs.caremed.form.OperationForm;
import az.neuron.dev.rs.caremed.search.PatientSearchByDoctor;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Bayram
 */
@Service
public class DoctorService implements IDoctorService {

    @Autowired
    private DoctorDao doctorDao;

    @Override
    public DataTable getPatientListByDoctor(String token, PatientSearchByDoctor searchByDoctor) {
        return this.doctorDao.getPatientListByDoctor(token, searchByDoctor);
    }

    public OperationResponse addCheckUpPatient(String token, CheckPasientForm form) {
        return this.doctorDao.addCheckUpPatient(token, form);
    }

    @Override
    public OperationResponse addSubServicesByDoctor(String token, OperationForm operationForm) {
        return this.doctorDao.addSubServicesByDoctor(token, operationForm);
    }

    @Override
    public List<PatienForwardByDoctor> getPatientForwardByDoctor(String token, int id) {
        return this.doctorDao.getPatientForwardByDoctor(token, id);
    }
}
