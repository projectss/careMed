/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.search;

/**
 *
 * @author Bayram
 */
public class ConsuptionSearchForm {
      
    private String orderColumn = "id";
    private String orderType = "asc";
    private Integer start = 0;
    private Integer length = 10;
    private String code;
    private Integer type;
    private Integer unit;

    public ConsuptionSearchForm() {
    }

    
    
    public String getOrderColumn() {
        return orderColumn;
    }

    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "ConsuptionSearchForm{" + "orderColumn=" + orderColumn + ", orderType=" + orderType + ", start=" + start + ", length=" + length + ", code=" + code + ", type=" + type + ", unit=" + unit + '}';
    }
    
    
    
    
    
    
}
