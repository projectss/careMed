/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

import java.util.List;

/**
 *
 * @author Bayram
 */
public class Person {
    private int id;
    private String fullname;
    private String firstname;
    private String lastname;
    private String middlename;
    private DictionaryWrapper citizenshipId;
    private DictionaryWrapper maritalStatusId;
    private DictionaryWrapper millitaryServiceId;
    private DictionaryWrapper nationalityId;
    private DictionaryWrapper socialStatusId;
    private String birthdate;
    private String pincode;
    private DictionaryWrapper svSeriya;
    private String seriya;
    private DictionaryWrapper bloodGroupId;
    private DictionaryWrapper supplyOrgId;
    private String svEndDate;
    private int height;
    private DictionaryWrapper genderId;
    private int photoFileId;
    private int childCount;
    private List <PersonAddress> addreses;
    private List <PersonContact> contacts;
    private List <PersonDocument> documents;
    private List <PersonRelationship> relationships;
    private List <PersonLanguage> languages;

    public Person() {
    }

    public Person(int id, String firstname, String lastname, String middlename, DictionaryWrapper genderId) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.genderId = genderId;
    }
     public Person(int id, String firstname, String lastname, String middlename, DictionaryWrapper genderId, String birthdate) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.genderId = genderId;
        this.birthdate = birthdate;
    }
      public Person( String firstname, String lastname, String middlename, DictionaryWrapper genderId) {

        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.genderId = genderId;
    }

    public Person(int id, String firstname, String lastname, String middlename, DictionaryWrapper maritalStatusId, DictionaryWrapper nationalityId, String birthdate, DictionaryWrapper svSeriya, String seriya,DictionaryWrapper supplyOrgId) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.maritalStatusId = maritalStatusId;
  
        this.nationalityId = nationalityId;
        this.birthdate = birthdate;
        this.svSeriya = svSeriya;
        this.seriya = seriya;
        this.supplyOrgId=supplyOrgId;
    }

    
    
    public Person(int id, String firstname, String lastname, String middlename, DictionaryWrapper citizenshipId, DictionaryWrapper maritalStatusId, DictionaryWrapper millitaryServiceId, DictionaryWrapper nationalityId, DictionaryWrapper socialStatusId, String birthdate, String pincode, DictionaryWrapper svSeriya, String seriya, DictionaryWrapper bloodGroupId, DictionaryWrapper supplyOrgId, String svEndDate, int height, DictionaryWrapper genderId, int photoFileId, int childCount, List<PersonAddress> addreses, List<PersonContact> contacts, List<PersonDocument> documents, List<PersonRelationship> relationships, List<PersonLanguage> languages) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.citizenshipId = citizenshipId;
        this.maritalStatusId = maritalStatusId;
        this.millitaryServiceId = millitaryServiceId;
        this.nationalityId = nationalityId;
        this.socialStatusId = socialStatusId;
        this.birthdate = birthdate;
        this.pincode = pincode;
        this.svSeriya = svSeriya;
        this.seriya = seriya;
        this.bloodGroupId = bloodGroupId;
        this.supplyOrgId = supplyOrgId;
        this.svEndDate = svEndDate;
        this.height = height;
        this.genderId = genderId;
        this.photoFileId = photoFileId;
        this.childCount = childCount;
        this.addreses = addreses;
        this.contacts = contacts;
        this.documents = documents;
        this.relationships = relationships;
        this.languages = languages;
    }

    public Person(int id, String firstname, String lastname, String middlename, DictionaryWrapper citizenshipId, DictionaryWrapper maritalStatusId, DictionaryWrapper millitaryServiceId, DictionaryWrapper nationalityId, DictionaryWrapper socialStatusId, String birthdate, String pincode, DictionaryWrapper svSeriya, String seriya, DictionaryWrapper bloodGroupId, DictionaryWrapper supplyOrgId, String svEndDate, int height, DictionaryWrapper genderId, int photoFileId, int childCount, List<PersonAddress> addreses, List<PersonContact> contacts, List<PersonDocument> documents, List<PersonRelationship> relationships) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.citizenshipId = citizenshipId;
        this.maritalStatusId = maritalStatusId;
        this.millitaryServiceId = millitaryServiceId;
        this.nationalityId = nationalityId;
        this.socialStatusId = socialStatusId;
        this.birthdate = birthdate;
        this.pincode = pincode;
        this.svSeriya = svSeriya;
        this.seriya = seriya;
        this.bloodGroupId = bloodGroupId;
        this.supplyOrgId = supplyOrgId;
        this.svEndDate = svEndDate;
        this.height = height;
        this.genderId = genderId;
        this.photoFileId = photoFileId;
        this.childCount = childCount;
        this.addreses = addreses;
        this.contacts = contacts;
        this.documents = documents;
        this.relationships = relationships;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public DictionaryWrapper getCitizenshipId() {
        return citizenshipId;
    }

    public void setCitizenshipId(DictionaryWrapper citizenshipId) {
        this.citizenshipId = citizenshipId;
    }

    public DictionaryWrapper getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setMaritalStatusId(DictionaryWrapper maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }

    public DictionaryWrapper getMillitaryServiceId() {
        return millitaryServiceId;
    }

    public void setMillitaryServiceId(DictionaryWrapper millitaryServiceId) {
        this.millitaryServiceId = millitaryServiceId;
    }

    public DictionaryWrapper getNationalityId() {
        return nationalityId;
    }

    public void setNationalityId(DictionaryWrapper nationalityId) {
        this.nationalityId = nationalityId;
    }

    public DictionaryWrapper getSocialStatusId() {
        return socialStatusId;
    }

    public void setSocialStatusId(DictionaryWrapper socialStatusId) {
        this.socialStatusId = socialStatusId;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public DictionaryWrapper getSvSeriya() {
        return svSeriya;
    }

    public void setSvSeriya(DictionaryWrapper svSeriya) {
        this.svSeriya = svSeriya;
    }

    public String getSeriya() {
        return seriya;
    }

    public void setSeriya(String seriya) {
        this.seriya = seriya;
    }

    public DictionaryWrapper getBloodGroupId() {
        return bloodGroupId;
    }

    public void setBloodGroupId(DictionaryWrapper bloodGroupId) {
        this.bloodGroupId = bloodGroupId;
    }

    public DictionaryWrapper getSupplyOrgId() {
        return supplyOrgId;
    }

    public void setSupplyOrgId(DictionaryWrapper supplyOrgId) {
        this.supplyOrgId = supplyOrgId;
    }

    public String getSvEndDate() {
        return svEndDate;
    }

    public void setSvEndDate(String svEndDate) {
        this.svEndDate = svEndDate;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public DictionaryWrapper getGenderId() {
        return genderId;
    }

    public void setGenderId(DictionaryWrapper genderId) {
        this.genderId = genderId;
    }

    public int getPhotoFileId() {
        return photoFileId;
    }

    public void setPhotoFileId(int photoFileId) {
        this.photoFileId = photoFileId;
    }

    public int getChildCount() {
        return childCount;
    }

    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    public List <PersonAddress> getAddreses() {
        return addreses;
    }

    public void setAddreses(List <PersonAddress> addreses) {
        this.addreses = addreses;
    }

    public List <PersonContact> getContacts() {
        return contacts;
    }

    public void setContacts(List <PersonContact> contacts) {
        this.contacts = contacts;
    }

    public List <PersonDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List <PersonDocument> documents) {
        this.documents = documents;
    }

    public List <PersonRelationship> getRelationships() {
        return relationships;
    }

    public void setRelationships(List <PersonRelationship> relationships) {
        this.relationships = relationships;
    }

    public List <PersonLanguage> getLanguages() {
        return languages;
    }

    public void setLanguages(List <PersonLanguage> languages) {
        this.languages = languages;
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", fullname=" + fullname + ", firstname=" + firstname + ", lastname=" + lastname + ", middlename=" + middlename + ", citizenshipId=" + citizenshipId + ", maritalStatusId=" + maritalStatusId + ", millitaryServiceId=" + millitaryServiceId + ", nationalityId=" + nationalityId + ", socialStatusId=" + socialStatusId + ", birthdate=" + birthdate + ", pincode=" + pincode + ", svSeriya=" + svSeriya + ", seriya=" + seriya + ", bloodGroupId=" + bloodGroupId + ", supplyOrgId=" + supplyOrgId + ", svEndDate=" + svEndDate + ", height=" + height + ", genderId=" + genderId + ", photoFileId=" + photoFileId + ", childCount=" + childCount + ", addreses=" + addreses + ", contacts=" + contacts + ", documents=" + documents + ", relationships=" + relationships + ", languages=" + languages + '}';
    }
}
