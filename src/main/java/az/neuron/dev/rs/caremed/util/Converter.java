/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.util;

import az.neuron.dev.rs.caremed.domain.FileWrapper;
import az.neuron.dev.rs.caremed.form.AbroadForm;
import az.neuron.dev.rs.caremed.form.AcademicInfoForm;
import az.neuron.dev.rs.caremed.form.AddressForm;
import az.neuron.dev.rs.caremed.form.AwardForm;
import az.neuron.dev.rs.caremed.form.CaseRecordForm;
import az.neuron.dev.rs.caremed.form.CheckUpForm;
import az.neuron.dev.rs.caremed.form.ContactForm;
import az.neuron.dev.rs.caremed.form.EducationForm;
import az.neuron.dev.rs.caremed.form.ElectronalOrgForm;
import az.neuron.dev.rs.caremed.form.EmployementForm;
import az.neuron.dev.rs.caremed.form.FileWrapperForm;
import az.neuron.dev.rs.caremed.form.HamfullStuffForm;
import az.neuron.dev.rs.caremed.form.MilitaryInfoForm;
import az.neuron.dev.rs.caremed.form.PatientHamfullStuffForm;
import az.neuron.dev.rs.caremed.form.PersonLanguageForm;
import az.neuron.dev.rs.caremed.form.PersonRelationForm;
import az.neuron.dev.rs.caremed.form.ResearchForm;
import az.neuron.dev.rs.caremed.form.ServiceOperationForm;
import az.neuron.dev.rs.caremed.form.VacationForm;

/**
 *
 * @author Bayram
 */
public class Converter {

    public static final Object[][] toPlpgContactObject(ContactForm[] contactForm) {

        Object[][] objects = new Object[contactForm.length][];

        for (int i = 0; i < contactForm.length; i++) {

            objects[i] = new Object[2];
            objects[i][0] = contactForm[i].getTypeId();
            objects[i][1] = contactForm[i].getContact();

        }

        return objects;
    }

    public static final Object[][] toPlpgAddressObject(AddressForm[] addressForms) {

        Object[][] objects = new Object[addressForms.length][];

        for (int i = 0; i < addressForms.length; i++) {
            objects[i] = new Object[3];
            objects[i][0] = addressForms[i].getTypeId();
            objects[i][1] = addressForms[i].getAddressId();
            objects[i][2] = addressForms[i].getAddress();

        }

        return objects;
    }

    public static final Object[][] toPlpgAbroadObject(AbroadForm[] abroadForm) {

        Object[][] objects = new Object[abroadForm.length][];
        for (int i = 0; i < abroadForm.length; i++) {
            objects[i] = new Object[5];
            objects[i][0] = abroadForm[i].getCountryId();
            objects[i][1] = abroadForm[i].getReasonId();
            objects[i][2] = abroadForm[i].getStartDate();
            objects[i][3] = abroadForm[i].getEndDate();
            objects[i][4] = abroadForm[i].getNote();

        }

        return objects;
    }

    public static final Object[][] toPlpgAcademicInfoObject(AcademicInfoForm[] academicInfoForm) {

        Object[][] objects = new Object[academicInfoForm.length][];
        for (int i = 0; i < academicInfoForm.length; i++) {
            objects[i] = new Object[3];
            objects[i][0] = academicInfoForm[i].getTypeId();
            objects[i][1] = academicInfoForm[i].getAcademicInfoId();
            objects[i][2] = academicInfoForm[i].getStartDate();

        }

        return objects;
    }

    public static final Object[][] toPlpgResearch(ResearchForm[] researchForm) {

        Object[][] objects = new Object[researchForm.length][];

        for (int i = 0; i < researchForm.length; i++) {
            objects[i] = new Object[4];
            objects[i][0] = researchForm[i].getResearchName();
            objects[i][1] = researchForm[i].getResearchType();
            objects[i][2] = researchForm[i].getStartDate();
            objects[i][3] = researchForm[i].getNote();

        }
        return objects;
    }

    public static final Object[][] toPlpgMilitaryInfo(MilitaryInfoForm[] millitaryinfoForm) {

        Object[][] objects = new Object[millitaryinfoForm.length][];

        for (int i = 0; i < millitaryinfoForm.length; i++) {
            objects[i] = new Object[4];
            objects[i][0] = millitaryinfoForm[i].getMillitaryNameId();
            objects[i][1] = millitaryinfoForm[i].getStaffType();
            objects[i][2] = millitaryinfoForm[i].getArmyType();
            objects[i][3] = millitaryinfoForm[i].getNote();
        }

        return objects;
    }

    public static final Object[][] toPlpgEmployement(EmployementForm[] employementForm) {
        Object[][] objects = new Object[employementForm.length][];

        for (int i = 0; i < employementForm.length; i++) {
            objects[i] = new Object[6];
            objects[i][0] = employementForm[i].getCompanyName();
            objects[i][1] = employementForm[i].getPositionName();
            objects[i][2] = employementForm[i].getCompanyAddress();
            objects[i][3] = employementForm[i].getStartDate();
            objects[i][4] = employementForm[i].getEndDate();
            objects[i][5] = employementForm[i].getNote();

        }

        return objects;
    }

    public static final Object[][] toPlpgElectoralOrg(ElectronalOrgForm[] electronalOrgForm) {
        Object[][] objects = new Object[electronalOrgForm.length][];

        for (int i = 0; i < electronalOrgForm.length; i++) {
            objects[i] = new Object[6];
            objects[i][0] = electronalOrgForm[i].getOrgName();
            objects[i][1] = electronalOrgForm[i].getAddress();
            objects[i][2] = electronalOrgForm[i].getPositionName();
            objects[i][3] = electronalOrgForm[i].getStartDate();
            objects[i][4] = electronalOrgForm[i].getEndDate();
            objects[i][5] = electronalOrgForm[i].getNote();
        }

        return objects;
    }

    public static final Object[][] toPlpgAward(AwardForm[] awardForm) {
        Object[][] objects = new Object[awardForm.length][];

        for (int i = 0; i < awardForm.length; i++) {
            objects[i] = new Object[4];
            objects[i][0] = awardForm[i].getStartDate();
            objects[i][1] = awardForm[i].getAwardName();
            objects[i][2] = awardForm[i].getTypeId();
            objects[i][3] = awardForm[i].getNote();
        }

        return objects;
    }

    public static final Object[][] toPlpgPersonLanguage(PersonLanguageForm[] languageForms) {
        Object[][] objects = new Object[languageForms.length][];

        for (int i = 0; i < languageForms.length; i++) {
            objects[i] = new Object[2];
            objects[i][0] = languageForms[i].getLanguageId();
            objects[i][1] = languageForms[i].getDegreeId();

        }

        return objects;
    }

    public static final Object[][] toPlpgRelationship(PersonRelationForm[] personRelationForm) {
        Object[][] objects = new Object[personRelationForm.length][];

        for (int i = 0; i < personRelationForm.length; i++) {
            objects[i] = new Object[7];
            objects[i][0] = personRelationForm[i].getRelationshipId();
            objects[i][1] = personRelationForm[i].getFullName();
            objects[i][2] = personRelationForm[i].getBirthdate();
            objects[i][3] = personRelationForm[i].getContactPhone();
            objects[i][4] = personRelationForm[i].getContactHome();
            objects[i][5] = personRelationForm[i].getAddress();
            objects[i][6] = personRelationForm[i].getNote();
        }

        return objects;
    }

    public static final Object[][] toPlpgServiceOperation(ServiceOperationForm[] serviceOperationForms) {
        Object[][] objects = new Object[serviceOperationForms.length][];

        for (int i = 0; i < serviceOperationForms.length; i++) {
            objects[i] = new Object[11];

            objects[i][0] = serviceOperationForms[i].getPatientId();
            objects[i][1] = serviceOperationForms[i].getEmployeeId();
            objects[i][2] = serviceOperationForms[i].getServiceId();
            objects[i][3] = serviceOperationForms[i].getStartDate();
            objects[i][4] = serviceOperationForms[i].getEndDate();
            objects[i][5] = serviceOperationForms[i].getStatusTypeId();
            objects[i][6] = serviceOperationForms[i].getQueueTypeId();
            objects[i][7] = serviceOperationForms[i].getMedicalCommitionId();
            objects[i][8] = serviceOperationForms[i].getPaymentStatusId();
            objects[i][9] = serviceOperationForms[i].getPrice();
            objects[i][10] = serviceOperationForms[i].getNote();

        }

        return objects;
    }

    public static final Object[][] toPlgEducation(EducationForm[] educationForms) {
        Object[][] objects = new Object[educationForms.length][];
        for (int i = 0; i < educationForms.length; i++) {
            objects[i] = new Object[9];
            objects[i][0] = educationForms[i].getEduLevelId();
            objects[i][1] = educationForms[i].getInstitutionName();
            objects[i][2] = educationForms[i].getInstitutionAddress();
            objects[i][3] = educationForms[i].getInstitutionType();
            objects[i][4] = educationForms[i].getFacultyName();
            objects[i][5] = educationForms[i].getStartDate();
            objects[i][6] = educationForms[i].getEndDate();
            objects[i][7] = educationForms[i].getDiplomNumber();
            objects[i][8] = educationForms[i].getNote();
        }

        return objects;
    }

    public static final Object[][] toPlgCaseRecord(CaseRecordForm[] caseRecordForms) {
        Object[][] objects = new Object[caseRecordForms.length][];
        for (int i = 0; i < caseRecordForms.length; i++) {
            objects[i] = new Object[4];
            objects[i][0] = caseRecordForms[i].getCaseNo();
            objects[i][1] = caseRecordForms[i].getStartDate();
            objects[i][2] = caseRecordForms[i].getEndDate();
            objects[i][3] = caseRecordForms[i].getDayCount();
        }

        return objects;
    }

    public static final Object[][] toPlgVacation(VacationForm[] vacationForms) {
        Object[][] objects = new Object[vacationForms.length][];
        for (int i = 0; i < vacationForms.length; i++) {
            objects[i] = new Object[9];
            objects[i][0] = vacationForms[i].getTypeId();
            objects[i][1] = vacationForms[i].getDayCount();
            objects[i][2] = vacationForms[i].getPeriod();
            objects[i][3] = vacationForms[i].getStartDate();
            objects[i][4] = vacationForms[i].getEndDate();
            objects[i][5] = vacationForms[i].getOrgerNo();
            objects[i][6] = vacationForms[i].getNextVacationDate();
        }

        return objects;
    }

    public static final Object[][] toPlgFileWrapper(FileWrapperForm[] fileWrappers) {
        Object[][] objects = new Object[fileWrappers.length][];
        for (int i = 0; i < fileWrappers.length; i++) {
            objects[i] = new Object[4];
            objects[i][0] = fileWrappers[i].getOriginalName();
            objects[i][1] = fileWrappers[i].getPath();
            objects[i][2] = fileWrappers[i].getFile().getSize();
            objects[i][3] = fileWrappers[i].getFile().getContentType();
        }

        return objects;
    }

    public static final Object[][] toPlpgCheckUp(CheckUpForm[] checkUpForm) {

        Object[][] objects = new Object[checkUpForm.length][];

        for (int i = 0; i < checkUpForm.length; i++) {
            objects[i] = new Object[2];
            objects[i][0] = checkUpForm[i].getId();
            objects[i][1] = checkUpForm[i].getValue();
        }

        return objects;
    }

    public static final Object[][] toPlpgHarmfullStuff(HamfullStuffForm[] form) {
        Object[][] objectses = new Object[form.length][];

        for (int i = 0; i < form.length; i++) {
            objectses[i] = new Object[3];

            objectses[i][0] = form[i].getPatientId();
            objectses[i][1] = form[i].getHarmfullStuffId();
            objectses[i][2] = form[i].getDate();
        }

        return objectses;
    }

}
