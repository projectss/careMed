/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.caremed.domain;

/**
 *
 * @author Bayram
 */
public class Document {
    private int id;
    private DictionaryWrapper type;
    private String docSeriya;
    private String docNumber;
    private String startDate;
    private String endDate;

    public Document() {
    }

    public Document(int id, DictionaryWrapper type, String docSeriya, String docNumber, String startDate, String endDate) {
        this.id = id;
        this.type = type;
        this.docSeriya = docSeriya;
        this.docNumber = docNumber;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DictionaryWrapper getType() {
        return type;
    }

    public void setType(DictionaryWrapper type) {
        this.type = type;
    }

    public String getDocSeriya() {
        return docSeriya;
    }

    public void setDocSeriya(String docSeriya) {
        this.docSeriya = docSeriya;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Document{" + "id=" + id + ", type=" + type + ", docSeriya=" + docSeriya + ", docNumber=" + docNumber + ", startDate=" + startDate + ", endDate=" + endDate + '}';
    }
}
